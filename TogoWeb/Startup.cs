﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(TogoWeb.Startup))]
namespace TogoWeb
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
