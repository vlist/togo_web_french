﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.IO;

namespace TogoWeb
{
    public partial class DataPage : System.Web.UI.Page
    {
        TraceSource trace;
        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);
            
            trace.TraceEvent(TraceEventType.Verbose, 1, DateTime.Now.ToString() + ": In Main.aspx");
            trace.Flush();
            if (!IsPostBack)
            {
                Session.Add("idView", 0);

                Load_Station_GridView();
            }
        }
        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }

        private void Load_Station_GridView()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var sta = from s in context.Stations
                              select s;

                    DataTable staTable = new DataTable();

                    staTable.Columns.Add("ID", typeof(string));
                    staTable.Columns.Add("Name", typeof(string));
                    staTable.Columns.Add("xCoordinate", typeof(double));
                    staTable.Columns.Add("yCoordinate", typeof(double));

                    if (sta.Count() > 0)
                    {
                        foreach (Station ss in sta)
                        {
                            string NameUse = ss.Name;
                            if (NameUse.Trim() == "Nangbeto Dam") NameUse = "Barrage Nangbeto";
                            staTable.Rows.Add(ss.id, NameUse, ss.xCoordinate, ss.yCoordinate);
                        }
                    }
                    Stations_GridView.DataSource = staTable;
                    Stations_GridView.DataBind();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void Details_Button_Command(object sender, CommandEventArgs e)
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int rowIndex = Convert.ToInt32(e.CommandArgument);
                    GridViewRow rowtest = Stations_GridView.Rows[rowIndex];
                    int selectedSta = Convert.ToInt32(rowtest.Cells[1].Text);
                    string staName = Convert.ToString(rowtest.Cells[2].Text);
                    if (staName.Trim() == "Nangbeto Dam") staName = "Barrage Nangbeto";

                    TextBox1_StationID.Text = Convert.ToString(selectedSta);
                    TextBox1_StationID0.Text = Convert.ToString(selectedSta);
                    TextBox1_Sta.Text = staName;
                    TextBox1_Sta0.Text = staName;
                    Populate_Variable_Drop(context);
                }
            }

            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
            AddValue_Button.Visible = false;
            Export_Button.Visible = false;
            Export_Button0.Visible = false;
            LoadTextboxSettings();
            DateRange_Reset();
            DateRange_Reset0();

            bool editable = new bool();
            int idUser = Convert.ToInt32(Session["idUser"]);
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var use = from u in context.Users
                              where u.id == idUser
                              select u;
                    
                    if (use.Count() > 0)
                    {
                        editable = use.FirstOrDefault().Permission;
                        if (editable == true)
                        {
                            MultiView1.ActiveViewIndex = 1;
                            AddValue_Button.Visible = true;
                        }
                        else
                        {
                            MultiView1.ActiveViewIndex = 1;
                            AddValue_Button.Visible = true;
                            //any user can upload data
                        }
                    }
                    else
                    {
                        editable = false;
                        MultiView1.ActiveViewIndex = 3;
                        // if no user login, then go to generic data page
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        private void Populate_Variable_Drop(Togo_Test_dataEntities context)
        {
            int selectedSta = Convert.ToInt32(TextBox1_StationID.Text);

            var varXSta = from vs in context.VariableXStations
                           where vs.idStation == selectedSta
                           select vs;

            DataTable varTable = new DataTable();
            varTable.Columns.Add("Id", typeof(int));
            varTable.Columns.Add("Name", typeof(string));

            foreach(VariableXStation i in varXSta)
            {
                var varUse = from v in context.Variables
                             where v.id == i.idVariable
                             select v;

                if (varUse.Count()>0)
                {
                    Variable thisVar = varUse.First();
                    string VarNameUse = thisVar.Name;
                    
                    if (thisVar.Name.Trim() == "Inflow")
                    {
                        VarNameUse = "Apport";
                    }
                    if (thisVar.Name.Trim() == "Outflow")
                    {
                        VarNameUse = "Débit en aval";
                    }
                    if (thisVar.Name.Trim() == "Water Level")
                    {
                        VarNameUse = "Niveau du lac";
                    }
                    varTable.Rows.Add(thisVar.id, VarNameUse);
                }
            }
            DropDownList1_Var.DataSource = varTable;
            DropDownList1_Var.DataTextField = "Name";
            DropDownList1_Var.DataValueField = "Id";
            DropDownList1_Var.DataBind();
            DropDownList1_Var.Items.Insert(0, ".::Sélectionnez variable::.");

            DropDownList1_Var0.DataSource = varTable;
            DropDownList1_Var0.DataTextField = "Name";
            DropDownList1_Var0.DataValueField = "Id";
            DropDownList1_Var0.DataBind();
            DropDownList1_Var0.Items.Insert(0, ".::Sélectionnez variable::.");
        }
        protected void Cancel_Button_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
            LoadTextboxSettings();
            DateRange_Reset();
            DateRange_Reset0();
        }
        protected void Cancel_Add_Button_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 1;
        }

        protected void AddValue_Button_Click(object sender, EventArgs e)
        {
            if (DropDownList1_Var.SelectedIndex < 1)
            {
                Label_Variable_Error.Visible = true;
                return;
            }
            else
            {
                Label_Variable_Error.Visible = false;
            }
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int selectedVar = Convert.ToInt32(DropDownList1_Var.SelectedValue);

                    var query = (from u in context.Variables
                                    where u.id == selectedVar
                                    select u).FirstOrDefault();

                    Label_Units.Text = query.Unit;
                    if (query.Unit.Trim() == "m MSL") Label_Units.Text = "m.s.m";
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            LoadTextboxSettings();

            TextBox1_Sta_Add.Text = TextBox1_Sta.Text;
            TextBox1_Var_Add.Text = DropDownList1_Var.SelectedItem.Text;

            MultiView1.ActiveViewIndex = 2;
           
        }
        protected void DropDown_Var_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateRange_Reset();

            if (DropDownList1_Var.SelectedIndex < 1)
            {
                Label_Variable_Error.Visible = true;
            }
            else
            {
                Label_Variable_Error.Visible = false;
            }

        }
        protected void DropDown_Var0_SelectedIndexChanged(object sender, EventArgs e)
        {
            DateRange_Reset0();
            if (DropDownList1_Var0.SelectedIndex < 1)
            {
                Label_Variable_Error0.Visible = true;
            }
            else
            {
                Label_Variable_Error0.Visible = false;
            }
        }
        private void Load_Data_GridView()   // for admin users
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int selectedVar = Convert.ToInt32(DropDownList1_Var.SelectedValue);
                    int selectedSta = Convert.ToInt32(TextBox1_StationID.Text);

                    DateTime start_range = Convert.ToDateTime(TextBox_DataRange_Start.Text);
                    DateTime end_range = Convert.ToDateTime(TextBox_DataRange_End.Text);

                    start_range = start_range.AddDays(-1);
                    end_range = end_range.AddDays(1);

                    var var = (from v in context.Variables
                               where v.id == selectedVar
                               select v).FirstOrDefault();

                    var varXSta = (from vs in context.VariableXStations
                                  where vs.idStation == selectedSta & vs.idVariable == selectedVar 
                                  select vs).FirstOrDefault();

                    var queryAll = from d in context.DataValues
                                   where d.idVariableXStation == varXSta.id && d.DateTime > start_range && d.DateTime < end_range
                                   orderby d.DateTime descending
                                   select d;

                    DataTable dataGrid = new DataTable();
                    dataGrid.Columns.Add("ID", typeof(string));
                    dataGrid.Columns.Add("TimeDate", typeof(string));
                    dataGrid.Columns.Add("Value", typeof(double));
                    dataGrid.Columns.Add("Units", typeof(string));

                    if (queryAll.Count() > 0)
                    {
                        Export_Button.Visible = true;
                        Label_NoData.Visible = false;

                        string unitUse = var.Unit;
                        if (unitUse.Trim() == "m MSL") unitUse = "m.s.m";

                        foreach (DataValue dd in queryAll)
                        {
                            dataGrid.Rows.Add(dd.id, dd.DateTime.ToString("dd MMM yyyy"), dd.Value, unitUse);
                        }
                    }
                    else
                    {
                        Export_Button.Visible = false;
                        Label_NoData.Visible = true;
                    }

                    Data_GridView.DataSource = dataGrid;
                    Data_GridView.DataBind();

                    for (int i = 0; i < Data_GridView.Rows.Count; i++)
                    {
                        GridViewRow row = Data_GridView.Rows[i];
                        ((TextBox)row.FindControl("TextBox_ID")).Enabled = true;
                        ((TextBox)row.FindControl("TextBox_ID")).Text = dataGrid.Rows[i]["ID"].ToString();
                        ((TextBox)row.FindControl("TextBox_ID")).Enabled = false;
                        ((TextBox)row.FindControl("TextBox_Value")).Text = dataGrid.Rows[i]["Value"].ToString();
                        ((TextBox)row.FindControl("TextBox_Value")).Enabled = false;
                    }

                    Data_GridView.Visible = true;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        private void Load_Data_GridView0()  // for general users
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int selectedVar = Convert.ToInt32(DropDownList1_Var0.SelectedValue);
                    int selectedSta = Convert.ToInt32(TextBox1_StationID0.Text);

                    DateTime start_range = Convert.ToDateTime(TextBox_DataRange_Start0.Text);
                    DateTime end_range = Convert.ToDateTime(TextBox_DataRange_End0.Text);

                    start_range = start_range.AddDays(-1);
                    end_range = end_range.AddDays(1);

                    var var = (from v in context.Variables
                               where v.id == selectedVar
                               select v).FirstOrDefault();

                    var varXSta = (from vs in context.VariableXStations
                                   where vs.idStation == selectedSta & vs.idVariable == selectedVar 
                                   select vs).FirstOrDefault();

                    var queryAll = from d in context.DataValues
                                   where d.idVariableXStation == varXSta.id && d.DateTime > start_range && d.DateTime < end_range
                                   orderby d.DateTime descending
                                   select d;

                    DataTable dataGrid = new DataTable();
                    dataGrid.Columns.Add("ID", typeof(string));
                    dataGrid.Columns.Add("TimeDate", typeof(string));
                    dataGrid.Columns.Add("Value", typeof(double));
                    dataGrid.Columns.Add("Units", typeof(string));

                    if (queryAll.Count() > 0)
                    {
                        Export_Button.Visible = true;
                        Label_NoData.Visible = false;

                        string unitUse = var.Unit;
                        if (unitUse.Trim() == "m MSL") unitUse = "m.s.m";

                        foreach (DataValue dd in queryAll)
                        {
                            dataGrid.Rows.Add(dd.id, dd.DateTime.ToString("dd MMM yyyy"), dd.Value, unitUse);
                        }
                    }
                    else
                    {
                        Label_NoData0.Visible = true;
                        Export_Button0.Visible = false;
                    }

                    Data_GridView0.DataSource = dataGrid;
                    Data_GridView0.DataBind();

                    Data_GridView0.Visible = true;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void Button_GridView_Edit_Click(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = Data_GridView.Rows[idRow];
            ((TextBox)row.FindControl("Textbox_Value")).Enabled = true;
            ((Button)row.FindControl("Button_GridView_Edit")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Save")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Cancel")).Visible = true;
            Export_Button.Enabled = false;
        }

        protected void Button_GridView_Save_Click(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = Data_GridView.Rows[idRow];
            //check value filled in
            if (((TextBox)row.FindControl("Textbox_Value")).Text == "")
            {
                Label_ValueError.Visible = true;
                return;
            }
            Label_ValueError.Visible = false;
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int id = Convert.ToInt32(((TextBox)row.FindControl("TextBox_ID")).Text);

                    var query = from c in context.DataValues
                                where c.id == id
                                select c;
                    DataValue dat = query.First();
                    try
                    {
                        double ValueTest = Convert.ToDouble(((TextBox)row.FindControl("Textbox_Value")).Text);
                        Label_ValueError.Visible = false;
                    }
                    catch
                    {
                        Label_ValueError.Visible = true;
                        return;
                    }
                    dat.Value = Convert.ToDouble(((TextBox)row.FindControl("Textbox_Value")).Text);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            ((TextBox)row.FindControl("Textbox_Value")).Enabled = false;
            ((Button)row.FindControl("Button_GridView_Edit")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Save")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Cancel")).Visible = false;
            Export_Button.Enabled = true;
        }
        protected void Button_GridView_Cancel_Click(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = Data_GridView.Rows[idRow];
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int id = Convert.ToInt32(((TextBox)row.FindControl("TextBox_ID")).Text);
                    var query = from c in context.DataValues
                                where c.id == id
                                select c;
                    DataValue dat = query.First();
                    ((TextBox)row.FindControl("Textbox_Value")).Text = dat.Value.ToString();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            ((TextBox)row.FindControl("Textbox_Value")).Enabled = false;
            ((Button)row.FindControl("Button_GridView_Edit")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Save")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Cancel")).Visible = false;
            Export_Button.Enabled = true;
        }

        protected void LoadTextboxSettings()
        {
            Label4.Visible = true;
            Value_TextBox.Visible = true;
            Value_TextBox.Text = "";
            Label_Units.Visible = true;
            Hour_TextBox.Visible = true;
            Hour_TextBox.Text = "6";    //default to 6:00 am
            Minute_TextBox.Visible = true;
            Minute_TextBox.Text = "0";  //default to 6:00 am
            Label8.Visible = true;
            Label9.Visible = true;
            Save_Button.Visible = true;
            Value_Check_Label.Visible = false;
            Date_Check_Label.Visible = false;
            Time_Check_Label.Visible = false;
            Calendar_Add.SelectedDate = DateTime.Today;
            Label_Variable_Error.Visible = false;
            Label_DateRange_Error.Visible = false;
            Label_Variable_Error0.Visible = false;
            Label_DateRange_Error0.Visible = false;
        }
        protected void Save_Button_Click(object sender, EventArgs e)
        {
            int idUser = Convert.ToInt32(Session["idUser"]);
            try
            {
                int ck = 0;
                Value_Check_Label.Visible = false;
                Date_Check_Label.Visible = false;
                Time_Check_Label.Visible = false; 

                if (Value_TextBox.Text == "")
                {
                    Value_Check_Label.Visible = true;
                    ck = 1;
                }
                try
                {
                    double ValueAdd = Convert.ToDouble(Value_TextBox.Text);   
                }
                catch
                {
                    Value_Check_Label.Visible = true;
                    ck = 1;
                }
                if (Calendar_Add.SelectedDate == null)
                {
                    Date_Check_Label.Visible = true;
                    ck = 1;
                }
                if (Hour_TextBox.Text == "")
                {
                    Time_Check_Label.Visible = true;
                    ck = 1;
                }
                if (Minute_TextBox.Text == "")
                {
                    Time_Check_Label.Visible = true;
                    ck = 1;
                }
                try
                {
                    int DateAdd4 = Convert.ToInt32(Hour_TextBox.Text);
                    int DateAdd5 = Convert.ToInt32(Minute_TextBox.Text);
                }
                catch
                {
                    Time_Check_Label.Visible = true; // run length - integer
                    ck = 1;
                }

                if (ck == 1) return;

                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    DataValue data_new = new DataValue();                    

                    var query = from d in context.DataValues
                                orderby d.id descending
                                select d;

                    if (query.Count() > 0)
                    {
                        data_new.id = query.First().id + 1;
                    }
                    else
                    {
                        data_new.id = 1;
                    }

                    int selectedSta = Convert.ToInt32(TextBox1_StationID.Text);
                    int selectedVar = Convert.ToInt32(DropDownList1_Var.SelectedValue);

                    var varXSta = (from vs in context.VariableXStations
                                  where vs.idStation == selectedSta & vs.idVariable == selectedVar
                                  select vs).FirstOrDefault();

                    data_new.Value = Convert.ToDouble(Value_TextBox.Text);
                    data_new.idVariableXStation = varXSta.id;

                    int Month = Calendar_Add.SelectedDate.Month;
                    int Day = Calendar_Add.SelectedDate.Day;
                    int Year = Calendar_Add.SelectedDate.Year;
                    DateTime TimeDateAdd = new DateTime(Year, Month, Day, Convert.ToInt32(Hour_TextBox.Text), Convert.ToInt32(Minute_TextBox.Text), 0);
                    data_new.DateTime = TimeDateAdd;
                    data_new.idUsers = idUser;

                    context.DataValues.Add(data_new);
                    context.SaveChanges();
                }
                LoadTextboxSettings();
                Load_Data_GridView();
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
            MultiView1.ActiveViewIndex = 1;
        }
        protected void DataRange_Start_Button_Click(object sender, EventArgs e)
        {
            Calendar1.Visible = true;
        }
        protected void DataRange_End_Button_Click(object sender, EventArgs e)
        {
            Calendar2.Visible = true;
        }
        
        protected void Calendar1_SelectionChanged(object sender, EventArgs e)
        {
            TextBox_DataRange_Start.Text = Calendar1.SelectedDate.Date.ToString("dd MMM yyyy");
            TextBox_DataRange_Start.Visible = true;
            DataRange_Start_Button.Visible = false;
            Calendar1.Visible = false;
        }

        protected void Calendar2_SelectionChanged(object sender, EventArgs e)
        {
            TextBox_DataRange_End.Text = Calendar2.SelectedDate.Date.ToString("dd MMM yyyy");
            TextBox_DataRange_End.Visible = true;
            DataRange_End_Button.Visible = false;
            Calendar2.Visible = false;
        }
        protected void DataRange_Start_Button0_Click(object sender, EventArgs e)
        {
            Calendar10.Visible = true;
        }
        protected void DataRange_End_Button0_Click(object sender, EventArgs e)
        {
            Calendar20.Visible = true;
        }

        protected void Calendar10_SelectionChanged(object sender, EventArgs e)
        {
            TextBox_DataRange_Start0.Text = Calendar10.SelectedDate.Date.ToString("dd MMM yyyy");
            TextBox_DataRange_Start0.Visible = true;
            DataRange_Start_Button0.Visible = false;
            Calendar10.Visible = false;
        }

        protected void Calendar20_SelectionChanged(object sender, EventArgs e)
        {
            TextBox_DataRange_End0.Text = Calendar20.SelectedDate.Date.ToString("dd MMM yyyy");
            TextBox_DataRange_End0.Visible = true;
            DataRange_End_Button0.Visible = false;
            Calendar20.Visible = false;
        }

        protected void DateRange_Apply_Button_Click(object sender, EventArgs e)
        {
            Label_DateRange_Error.Visible = false;
            Label_Variable_Error.Visible = false;
            int ck = 0;

            if (DropDownList1_Var.SelectedIndex < 1)
            {
                Label_Variable_Error.Visible = true;
                ck = 1;
            }
            if (TextBox_DataRange_Start.Text == "")
            {
                Label_DateRange_Error.Visible = true;
                ck = 1;
            }
            if (TextBox_DataRange_End.Text == "")
            {
                Label_DateRange_Error.Visible = true;
                ck = 1;
            }
            if (ck == 1) return;
            DateTime start_test = Convert.ToDateTime(TextBox_DataRange_Start.Text);
            DateTime end_test = Convert.ToDateTime(TextBox_DataRange_End.Text);
            if (end_test < start_test)
            {
                Label_DateRange_Error.Visible = true;
                ck = 1;
            }
            if (ck == 1) return;

            Load_Data_GridView();
            AddValue_Button.Visible = true;
        }
        protected void DateRange_Apply_Button0_Click(object sender, EventArgs e)
        {
            Label_DateRange_Error0.Visible = false;
            Label_Variable_Error0.Visible = false;
            int ck = 0;

            if (DropDownList1_Var0.SelectedIndex < 1)
            {
                Label_Variable_Error0.Visible = true;
                ck = 1;
            }
            if (TextBox_DataRange_Start0.Text == "")
            {
                Label_DateRange_Error0.Visible = true;
                ck = 1;
            }
            if (TextBox_DataRange_End0.Text == "")
            {
                Label_DateRange_Error0.Visible = true;
                ck = 1;
            }
            if (ck == 1) return;
            DateTime start_test = Convert.ToDateTime(TextBox_DataRange_Start0.Text);
            DateTime end_test = Convert.ToDateTime(TextBox_DataRange_End0.Text);
            if (end_test < start_test)
            {
                Label_DateRange_Error0.Visible = true;
                ck = 1;
            }
            if (ck == 1) return;

            Load_Data_GridView0();
        }

        protected void DateRange_Reset_Button_Click(object sender, EventArgs e)
        {
            DateRange_Reset();
        }
        protected void DateRange_Reset()
        {
            DataRange_Start_Button.Visible = true;
            DataRange_End_Button.Visible = true;
            TextBox_DataRange_Start.Text = "";
            TextBox_DataRange_Start.Visible = false;
            TextBox_DataRange_End.Text = "";
            TextBox_DataRange_End.Visible = false;
            Data_GridView.Visible = false;
            Calendar1.SelectedDates.Clear();
            Calendar1.VisibleDate = DateTime.Today;
            Calendar2.SelectedDates.Clear();
            Calendar2.VisibleDate = DateTime.Today;
            Export_Button.Visible = false;
            Label_ExportError.Visible = false;
            Label_NoData.Visible = false;
            Label_DateRange_Error.Visible = false;
        }
        protected void DateRange_Reset_Button0_Click(object sender, EventArgs e)
        {
            DateRange_Reset0();
        }
        protected void DateRange_Reset0()
        {
            DataRange_Start_Button0.Visible = true;
            DataRange_End_Button0.Visible = true;
            TextBox_DataRange_Start0.Text = "";
            TextBox_DataRange_Start0.Visible = false;
            TextBox_DataRange_End0.Text = "";
            TextBox_DataRange_End0.Visible = false;
            Data_GridView0.Visible = false;
            Calendar10.SelectedDates.Clear();
            Calendar10.VisibleDate = DateTime.Today;
            Calendar20.SelectedDates.Clear();
            Calendar20.VisibleDate = DateTime.Today;
            Export_Button0.Visible = false;
            Label_ExportError0.Visible = false;
            Label_NoData0.Visible = false;
            Label_DateRange_Error0.Visible = false;
        }

        protected void Export_Button_Click(object sender, EventArgs e)
        {
            try
            {
                Label_ExportError.Visible = false;
                string units_text;
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int selectedVar = Convert.ToInt32(DropDownList1_Var.SelectedValue);

                    var query = (from u in context.Variables
                                 where u.id == selectedVar
                                 select u).FirstOrDefault();
                    units_text = query.Unit;
                    
                }
                
                string sta_text = TextBox1_Sta.Text.Trim();
                string var_text = DropDownList1_Var.SelectedItem.Text.Trim();
                
                System.IO.FileInfo Dfile = new System.IO.FileInfo(Server.MapPath("\\temp\\Togo_" + sta_text + "_" + var_text + ".csv"));
                string LocalFileName = Dfile.FullName;
                using (FileStream f = File.Open(LocalFileName, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (StreamWriter file = new StreamWriter(f))
                    {
                        string headerline = "Station,Variable,DateTime,Value,Units";
                        file.WriteLine(headerline);
                        for (int i=0; i<Data_GridView.Rows.Count; i++)
                        {
                            GridViewRow rowtest = Data_GridView.Rows[i];
                            string time = rowtest.Cells[1].Text;
                            string value = ((TextBox)rowtest.FindControl("TextBox_Value")).Text;

                            string linetext = sta_text +"," + var_text +"," + time + "," + value + "," + units_text.Trim();
                            file.WriteLine(linetext);
                        }
                    }
                }

                Response.ClearContent();
                Response.Clear();
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", LocalFileName));
                Response.WriteFile(LocalFileName);
                Response.End();
                System.IO.File.Delete(Page.MapPath(LocalFileName));

            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                Label_ExportError.Visible = true;
            }
        }
        protected void Export_Button0_Click(object sender, EventArgs e)
        {
            try
            {
                Label_ExportError0.Visible = false;
                string units_text;
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int selectedVar = Convert.ToInt32(DropDownList1_Var0.SelectedValue);

                    var query = (from u in context.Variables
                                 where u.id == selectedVar
                                 select u).FirstOrDefault();
                    units_text = query.Unit;

                }

                string sta_text = TextBox1_Sta0.Text.Trim();
                string var_text = DropDownList1_Var0.SelectedItem.Text.Trim();
                System.IO.FileInfo Dfile = new System.IO.FileInfo(Server.MapPath("\\temp\\Togo_" + sta_text + "_" + var_text + ".csv"));

                string LocalFileName = Dfile.FullName;
                using (FileStream f = File.Open(LocalFileName, FileMode.Create, FileAccess.Write, FileShare.None))
                {
                    using (StreamWriter file = new StreamWriter(f))
                    {
                        string headerline = "Station,Variable,DateTime,Value,Units";
                        file.WriteLine(headerline);
                        for (int i = 0; i < Data_GridView0.Rows.Count; i++)
                        {
                            GridViewRow rowtest = Data_GridView0.Rows[i];
                            string time = rowtest.Cells[1].Text;
                            string value = rowtest.Cells[2].Text;

                            string linetext = sta_text + "," + var_text + "," + time + "," + value + "," + units_text.Trim();
                            file.WriteLine(linetext);
                        }
                    }
                }

                Response.ClearContent();
                Response.Clear();
                Response.AddHeader("content-disposition", string.Format("attachment; filename={0}", LocalFileName));
                Response.WriteFile(LocalFileName);
                Response.End();
                System.IO.File.Delete(Page.MapPath(LocalFileName));

            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                Label_ExportError0.Visible = true;
                throw ex;
            }
        }
    }
}