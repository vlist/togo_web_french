﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddData.aspx.cs" Inherits="TogoWeb.AddData" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 35px;
        }
        .auto-style3 {
            height: 29px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">
                                        <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Ajouter une valeur de données" Font-Underline="True"></asp:Label>
                                    &nbsp;&nbsp;
                                    </td>
            </tr>
            <tr>
                <td>
                                        <asp:Panel ID="Panel_Add_Data" runat="server">
                                            <table class="auto-style1">
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station de données: "></asp:Label>
                                                        &nbsp;<asp:DropDownList ID="DropDownList_Station" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_Add_Station_SelectedIndexChanged" Font-Names="Calibri" Font-Size="Medium">
                                                        </asp:DropDownList>
                                                        &nbsp;<asp:Label ID="Label_Check_Sta" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="S'il vous plaît sélectionner la station de données" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label19" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                                                        &nbsp;<asp:DropDownList ID="DropDownList_Variable" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DropDownList_Add_Variable_SelectedIndexChanged" Font-Names="Calibri" Font-Size="Medium">
                                                        </asp:DropDownList>
                                                        &nbsp;<asp:Label ID="Label_Check_Var" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="S'il vous plaît sélectionner la variable" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="Label4" runat="server" Font-Names="Calibri" Text="Valeur: " Font-Size="Medium"></asp:Label>
                                                        <asp:TextBox ID="Value_TextBox" runat="server" Font-Names="Calibri"></asp:TextBox>
                                                        <asp:Label ID="Label_Units" runat="server" Font-Names="Calibri" Text="(units)" Visible="False"></asp:Label>
                                                        &nbsp;<asp:Label ID="Value_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Calendar ID="Calendar_Add" runat="server" Font-Names="Calibri" Font-Size="Medium"></asp:Calendar>
                                                        <asp:Label ID="Date_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="S'il vous plaît sélectionner la date" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style3">
                                                        <asp:Label ID="Label8" runat="server" Font-Names="Calibri" Text="Heure: " Font-Size="Medium"></asp:Label>
                                                        &nbsp;<asp:TextBox ID="Hour_TextBox" runat="server" Font-Names="Calibri"></asp:TextBox>
                                                        &nbsp;<asp:Label ID="Label9" runat="server" Font-Names="Calibri" Text="Minute: " Font-Size="Medium"></asp:Label>
                                                        <asp:TextBox ID="Minute_TextBox" runat="server" Font-Names="Calibri" style="margin-top: 0px"></asp:TextBox>
                                                        <asp:Label ID="Time_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="S'il vous plaît entrer une heure" Visible="False"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td class="auto-style3">
                                                        <asp:Button ID="Save_Button" runat="server" OnClick="Button_Save_Click" Text="Sauvegarder" Visible="False" Font-Names="Calibri" Font-Size="Medium" />
                                                        &nbsp;<asp:Button ID="Button_Reset" runat="server" OnClick="Button_Reset_Click" Text="Annuler" Visible="False" Font-Names="Calibri" Font-Size="Medium" />
                                                    </td>
                                                </tr>
                                            </table>
                                            &nbsp;&nbsp;</asp:Panel>
                                    </td>
            </tr>
            </table>
    
    </div>
    </form>
</body>
</html>
