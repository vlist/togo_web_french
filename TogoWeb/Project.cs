//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TogoWeb
{
    using System;
    using System.Collections.Generic;
    
    public partial class Project
    {
        public Project()
        {
            this.ModelRuns = new HashSet<ModelRun>();
            this.VariableXStations = new HashSet<VariableXStation>();
        }
    
        public int id { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<ModelRun> ModelRuns { get; set; }
        public virtual ICollection<VariableXStation> VariableXStations { get; set; }
    }
}
