﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Main.aspx.cs" Inherits="TogoWeb.Main" EnableEventValidation="false"%>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="header">
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <table class="nav-justified">
                            <tr>
                                <td>
            <asp:Label ID="Label1" runat="server" Text="FUNES" Font-Bold="True" Font-Names="Calibri" Font-Size="XX-Large" ForeColor="#003399"></asp:Label>
            &nbsp;
                        <asp:Label ID="Label2" runat="server" Font-Italic="True" ForeColor="Red" Text="Prototype: à valider"></asp:Label>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Button_Login" runat="server" OnClick="Button_Login_Click" Text="Login" Width="50px" />
                    &nbsp;<asp:Label ID="Label_Login" runat="server" Font-Names="Calibri" Text="Label" Visible="False"></asp:Label>
                        &nbsp;<asp:Button ID="Button_LogOut" runat="server" OnClick="Button_LogOut_Click" Text="Log Out" Visible="False" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                </table>
        </div>
        <asp:Menu ID="MainMenu" runat="server" EnableViewState="false"  Orientation="Horizontal"  OnMenuItemClick="MainMenu_MenuItemClick" Width="100%" BackColor="#B5C7DE" Font-Size="Large" ForeColor="#284E98" StaticSubMenuIndent="20px" Font-Names="Calibri" Height="90px" >
            <Items>
                <asp:MenuItem Text="Cartographie" Value="Map"></asp:MenuItem>
                <asp:MenuItem Text="Simulations" Value="Simulations"></asp:MenuItem>
                <asp:MenuItem Text="Données hydro" Value="DataPage">
                    <asp:MenuItem Text="Afficher" Value ="DataPage"></asp:MenuItem>
                    <asp:MenuItem Text="Ajouter Précipitations" Value ="AddDataRain"></asp:MenuItem>
                    <asp:MenuItem Text="Ajouter au Nangbéto" Value ="AddDataDam"></asp:MenuItem>
                </asp:MenuItem>
                <asp:MenuItem Text="Données Croix Rouge" Value="RedCross">
                    <asp:MenuItem Text="Impactes Observés" Value="Log"></asp:MenuItem>
                    <asp:MenuItem Text="Chaine d'alerte" Value="Contact"></asp:MenuItem>
                    <asp:MenuItem Text="Niveaux d'alerte" Value="Alerts"></asp:MenuItem>
                </asp:MenuItem>
                <asp:MenuItem Text="Admin" Value="Admin">
                    <asp:MenuItem Text="Users" Value="Users"></asp:MenuItem>
                </asp:MenuItem>
                <asp:MenuItem Text="À propos" Value="About"></asp:MenuItem>
                <asp:MenuItem Text="Aider" Value="Help"></asp:MenuItem>
            </Items>
            <StaticMenuItemStyle HorizontalPadding="20px" ItemSpacing="20px" />
        </asp:Menu>
    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="3" >
            <asp:View ID="View1" runat="server">
                <iframe id ="frame1"  src="Simulations.aspx" style="width:100%; height: 100em"></iframe>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <iframe id ="frame2" src="DataPage.aspx" style="width:100%; height: 100em"></iframe>
            </asp:View>
            <asp:View ID="View3" runat="server">
                        <meta name="viewport" content="width=device-width, initial-scale=1"/>
                        <link rel="stylesheet" type="text/css" href="Styles/chosen.css" />
                        <link rel="stylesheet" type="text/css" href="Styles/bootstrap.css" />
                        <div class="container">
                        <div class="row">
                            <div class="col-sm-6 col-md-4 col-md-offset-4">
                                &nbsp;<h1 class="text-center login-title">Code for Resilience (CfR) Togo User Login</h1>
                                <div class="account-wall">
                                    <form class="form-signin">
                                        <asp:Textbox runat="server" ID="UserTextbox" CssClass="form-control" type="text" placeholder="User" required autofocus></asp:Textbox>
                                        <asp:TextBox runat="server" ID="passwordTextbox" CssClass = "form-control" type="password" placeholder="Password" required></asp:TextBox>
                                        <asp:Label runat="server" ID="loginErrorLabel" CssClass="form-group has-error" 
                                            for="passwordTextbox" ForeColor="Red" Visible="False">Username or Password is not valid</asp:Label>
                                        <asp:Button runat="server" ID="loginButton" 
                                            CssClass="btn btn-lg btn-primary btn-block" type="submit" formmethod="get" 
                                            Text="Enter" onclick="loginButton_Click"/>
                                        <asp:CheckBox runat="server" ID="rememberCheckbox" 
                                            CssClass="checkbox-inline" Text="Remember me" />
                                        <span class="clearfix"></span>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
            </asp:View>
        <asp:View ID="View4" runat="server">
            <iframe id ="frame4" src="Map.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        <asp:View ID="View5" runat="server">
            <iframe id ="frame5" src="Users.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        <asp:View ID="View6" runat="server">
            <iframe id ="frame6" src="Alerts.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        <asp:View ID="View7" runat="server">
            <iframe id ="frame7" src="About.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        <asp:View ID="View8" runat="server">
            <iframe id ="frame8" src="TogoContact.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        <asp:View ID="View9" runat="server">
            <iframe id ="frame9" src="Log.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        <asp:View ID="View10" runat="server">
            <iframe id ="frame10" src="AddDataRain.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        <asp:View ID="View11" runat="server">
            <iframe id ="frame11" src="Help.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        <asp:View ID="View12" runat="server">
            <iframe id ="frame12" src="AddDataDam.aspx" style="width:100%; height: 100em"></iframe>
        </asp:View>
        </asp:MultiView>
    </div>
        <p>
            <asp:Label ID="Label12" runat="server" Text="Avec l’appui de GFDRR/CfR" Font-Names="Calibri" Font-Size="Small" ForeColor="#003399" Font-Italic="True"></asp:Label>
        </p>
    </form>
</body>
</html>
