//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace TogoWeb
{
    using System;
    using System.Collections.Generic;
    
    public partial class DataValue
    {
        public int id { get; set; }
        public int idVariableXStation { get; set; }
        public double Value { get; set; }
        public System.DateTime DateTime { get; set; }
        public int idUsers { get; set; }
    
        public virtual User User { get; set; }
        public virtual VariableXStation VariableXStation { get; set; }
    }
}
