﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Alerts.aspx.cs" Inherits="TogoWeb.Alerts" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 35px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
            <asp:View ID="View1" runat="server">
                <table class="auto-style1">
                    <tr>
                        <td>
                            <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Gestion des alertes pour les scénarios"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridView_Meta" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:BoundField HeaderText="ID" DataField="ID" />
                                    <asp:BoundField HeaderText="Nom" DataField="Name" />
                                    <asp:BoundField HeaderText="Risque" DataField="Risk" >
                                    <ItemStyle Width="100px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Description" DataField="Description" >
                                    <ItemStyle Width="500px" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Alerte">
                                        <ItemTemplate>
                                            <asp:Button ID="Button_Alert_Details" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_Alert_Details_Click" Text="Détails" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </asp:View>
            <asp:View ID="View2" runat="server">
                <table class="auto-style1">
                    <tr>
                        <td class="auto-style2">
                            <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Gestion des alertes " Font-Underline="True"></asp:Label>
                            &nbsp;&nbsp;<asp:Label ID="Label4" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Large" Text="Scénario:"></asp:Label>
                            &nbsp;<asp:Label ID="Label_Alert_MetaRisk" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Large" Text="(Risk)"></asp:Label>
                            &nbsp;&nbsp;<asp:Button ID="Button_Alert_OK" runat="server" OnClick="Button_Alert_OK_Click" Text="Ok" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox ID="TextBox_Alert_MetaID" runat="server" Visible="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Font-Underline="False" Text="Contact"></asp:Label>
                            &nbsp;&nbsp;<asp:Button ID="Button_Alert_Add" runat="server" OnClick="Button_Alert_Add_Click" Text="Ajouter contact existant" Width="160px" />
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label6" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Nom de contact:" Visible="False"></asp:Label>
                            &nbsp;<asp:DropDownList ID="DropDownList_AlertAdd_Contact" runat="server" OnSelectedIndexChanged="DropDownList_AlertAdd_Contact_SelectedIndexChanged" Visible="False">
                            </asp:DropDownList>
                            &nbsp;<asp:Label ID="Label7" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Action: " Visible="False"></asp:Label>
                            <asp:DropDownList ID="DropDownList_AlertAdd_Action" runat="server" OnSelectedIndexChanged="DropDownList_AlertAdd_Action_SelectedIndexChanged" Visible="False">
                            </asp:DropDownList>
                            &nbsp;&nbsp;<asp:Button ID="Button_AlertAdd_Save" runat="server" OnClick="Button_AlertAdd_Save_Click" Text="Sauvegarder" Visible="False" />
                            &nbsp;<asp:Button ID="Button_AlertAdd_Cancel" runat="server" OnClick="Button_AlertAdd_Cancel_Click" Text="Annuler" Visible="False" />
                            &nbsp;<asp:Button ID="Button_Alert_AddNew" runat="server" OnClick="Button_Alert_AddNew_Click" Text="Ajouter nouveau contact" Visible="False" Width="159px" />
                            <asp:Label ID="Label_Error_Contact" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="S'il vous plaît sélectionner le contact" Visible="False"></asp:Label>
                            &nbsp;<asp:Label ID="Label_Error_Action" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="S'il vous plaît sélectionnez l'action" Visible="False"></asp:Label>
                            &nbsp;<asp:Label ID="Label_Error_Exists" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Une alerte est déjà configuré pour ce contact et l'action" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:GridView ID="GridView_Alerts" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None" style="margin-right: 1px">
                                <AlternatingRowStyle BackColor="White" />
                                <Columns>
                                    <asp:TemplateField HeaderText="ActionID" Visible="False">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox_Alert_ActionID" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ContactID" Visible="False">
                                        <ItemTemplate>
                                            <asp:TextBox ID="TextBox_Alert_ContactID" runat="server"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField HeaderText="Prénom" DataField="ContactFirstName" />
                                    <asp:BoundField HeaderText="Nom" DataField="ContactLastName" />
                                    <asp:BoundField DataField="ActionType" HeaderText="Type d’action" >
                                    <ControlStyle Width="400px" />
                                    </asp:BoundField>
                                    <asp:BoundField HeaderText="Email" DataField="ContactEmail" />
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Button ID="Button_Alert_Remove" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_Alert_Remove_Click" Text="Effacer" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                                <EditRowStyle BackColor="#2461BF" />
                                <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                <RowStyle BackColor="#EFF3FB" />
                                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                <SortedDescendingHeaderStyle BackColor="#4870BE" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td class="auto-style2">
                            <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Font-Underline="False" Text="Message"></asp:Label>
                            &nbsp;<asp:Button ID="Button_Message_Edit" runat="server" OnClick="Button_Message_Edit_Click" Text="Editer" />
                            &nbsp;<asp:Button ID="Button_Message_Save" runat="server" OnClick="Button_Message_Save_Click" Text="Sauvegarder" Visible="False" />
                            &nbsp;<asp:Button ID="Button_Message_Cancel" runat="server" OnClick="Button_Message_Cancel_Click" Text="Annuler" Visible="False" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label10" runat="server" Font-Names="Calibri" Text="Entête:"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBox_Message_Header" runat="server" Enabled="False" Font-Names="Calibri" Height="90px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                            &nbsp;<asp:Label ID="Label_Error_Message_Header" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="S'il vous plaît entrer l'entête du message d'alerte" Visible="False"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label11" runat="server" Font-Names="Calibri" Text="Information sur la prévision:"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBox_Message_Main" runat="server" Enabled="False" Font-Names="Calibri" Height="90px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Label12" runat="server" Font-Names="Calibri" Text="Bas de page:"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:TextBox ID="TextBox_Message_Footer" runat="server" Enabled="False" Font-Names="Calibri" Height="90px" TextMode="MultiLine" Width="500px"></asp:TextBox>
                            &nbsp;<asp:Label ID="Label_Error_Message_Footer" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="S'il vous plaît entrer dans la fin du message d'alerte" Visible="False"></asp:Label>
                        </td>
                    </tr>
                </table>
            </asp:View>
        </asp:MultiView>
    </form>
</body>
</html>
