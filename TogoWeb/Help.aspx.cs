﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.Net.Mail;
using System.Net;

namespace TogoWeb
{
    public partial class Help : System.Web.UI.Page
    {
        static TraceSource trace;

        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");
        }

        protected void Button_Help_Send_Click(object sender, EventArgs e)
        {
            //Check if there is text in the textboxes
            Label_Help_Error_Name.Visible = false;
            Label_Help_Error_Email.Visible = false;
            Label_Help_Error_Subject.Visible = false;
            Label_Help_Error_Message.Visible = false;

            int ck = 0;
            if (TextBox_Help_Name.Text == "")
            {
                Label_Help_Error_Name.Visible = true;
                ck = 1;
            }
            if (TextBox_Help_Email.Text == "")
            {
                Label_Help_Error_Email.Visible = true;
                ck = 1;
            }
            if (TextBox_Help_Subject.Text == "")
            {
                Label_Help_Error_Subject.Visible = true;
                ck = 1;
            }
            if (TextBox_Help_Message.Text == "")
            {
                Label_Help_Error_Message.Visible = true;
                ck = 1;
            }
            if (ck == 1)
            {
                return;
            }

            string addressTO = "";
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {

                    var query = from c in context.Users
                                where c.Permission_TechSupport == true
                                select c;

                    if (query.Count() > 0)
                    {
                        foreach (User cc in query)
                        {
                            if (addressTO == "")
                            {
                                addressTO = cc.Email;
                            }
                            else
                            {
                                addressTO = addressTO + "," + cc.Email;
                            }
                        }

                        bool success = SendEmail(TextBox_Help_Email.Text, TextBox_Help_Subject.Text, TextBox_Help_Message.Text, TextBox_Help_Name.Text, addressTO);

                        if (success == true)
                        {
                            Panel_Help_Email.Visible = false;
                            Label_Help_Sent.Visible = true;
                            Button_Help_Sent_OK.Visible = true;
                        }
                        if (success == false)
                        {
                            Panel_Help_Email.Visible = false;
                            Label_Help_Sent_Error.Visible = true;
                            Label_Help_Sent_ErrorContact.Visible = true;
                            Button_Help_Sent_OK.Visible = true;
                        }
                    }
                    else
                    {
                        Panel_Help_Email.Visible = false;
                        Label_Help_Sent_Error.Visible = true;
                        Label_Help_Sent_ErrorContact.Visible = true;
                        Button_Help_Sent_OK.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Reset_Email_Form()
        {
            Label_Help_Error_Name.Visible = false;
            Label_Help_Error_Email.Visible = false;
            Label_Help_Error_Subject.Visible = false;
            Label_Help_Error_Message.Visible = false;
            TextBox_Help_Name.Text = "";
            TextBox_Help_Email.Text = "";
            TextBox_Help_Subject.Text = "";
            TextBox_Help_Message.Text = "";

            Label_Help_Sent.Visible = false;
            Label_Help_Sent_Error.Visible = false;
            Label_Help_Sent_ErrorContact.Visible = false;
            Button_Help_Sent_OK.Visible = false;
            Panel_Help_Email.Visible = true;
        }

        public static bool SendEmail(string addressFROM, string subject, string msgBody, string name, string addressTO)
        {
            msgBody = "Ce message a été envoyé par le systême de FUNES parce qu'un usager cherche de conseil technique.  Vous avez reçu ce message parce que vous êtes identifié comme faisant partie du personnel de soutien technique:\n\n" + msgBody + "\n\nMessage de: " + name + " " + addressFROM;
            
            bool success = false;

            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("vlist@envmodeling.com", "c0rTad0s"),
                EnableSsl = true
            };

            MailMessage message = new MailMessage();
            message.From = new MailAddress("vlist@envmodeling.com");  //HARDWIRE: for demonstration only - replace with another email ("soporte@gmail.com") // new MailAddress(addressFROM); 

            try
            {
                message.To.Add(addressTO);  // send to togo support staff
                message.Subject = subject;
                message.Body = msgBody;
            }
            catch
            {
                success = false;
                return success;
            }

            message.IsBodyHtml = false;
            message.Priority = MailPriority.Normal;

            try
            {
                client.Send(message);
                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                trace.TraceEvent(TraceEventType.Error, 1, "Error sending email to support " + addressTO);
                trace.Flush();
            }
            
            return success;
        }

        protected void Button_Help_Sent_OK_Click(object sender, EventArgs e)
        {
            Reset_Email_Form();
            Panel_Help_Email.Visible = true;
        }
    }
}