﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.IO;

namespace TogoWeb
{
    public partial class Users : System.Web.UI.Page
    {
        TraceSource trace;
        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var queryLogin = (from u in context.Users
                                      where u.id == idUser
                                      select u).FirstOrDefault();

                    if (queryLogin.Permission == false) // if no admin priv then can only access own
                    {
                        Button_User_Add.Visible = false;
                    }
                    else
                    {
                        Button_User_Add.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            trace.TraceEvent(TraceEventType.Verbose, 1, DateTime.Now.ToString() + ": In Users.aspx");
            trace.Flush();
            if (!IsPostBack)
            {
                Session.Add("idView", 0);

                Load_GridView_Users();
            }
        }
        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }
        private void Load_GridView_Users()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int idUser = Convert.ToInt32(Session["idUser"]);
                    var queryLogin = (from u in context.Users
                                      where u.id == idUser
                                      select u).FirstOrDefault();

                    DataTable userGrid = new DataTable();

                    if (queryLogin.Permission == false) // if no admin priv then can only access own
                    {
                        var queryAll = from u in context.Users
                                       where u.id == idUser
                                       select new
                                       {
                                           ID = u.id,
                                           Username = u.Username,
                                           Password = u.Password,
                                           FirstName = u.FirstName,
                                           LastName = u.LastName,
                                           Permission = u.Permission,
                                           PermissionOpt = u.Permission_opt,
                                           PermissionRedCross = u.Permission_RedCross,
                                           TechSupport = u.Permission_TechSupport,
                                           Email = u.Email
                                       };
                        userGrid = queryAll.ToDataTable();
                    }
                    else
                    {
                        var queryAll = from u in context.Users
                                       select new
                                       {
                                           ID = u.id,
                                           Username = u.Username,
                                           Password = u.Password,
                                           FirstName = u.FirstName,
                                           LastName = u.LastName,
                                           Permission = u.Permission,
                                           PermissionOpt = u.Permission_opt,
                                           PermissionRedCross = u.Permission_RedCross,
                                           TechSupport = u.Permission_TechSupport,
                                           Email = u.Email
                                       };
                        userGrid = queryAll.ToDataTable();
                    }
                    GridView_Users.DataSource = userGrid;
                    GridView_Users.DataBind();

                    for (int i = 0; i < GridView_Users.Rows.Count; i++)
                    {
                        GridViewRow row = GridView_Users.Rows[i];
                        ((TextBox)row.FindControl("TextBox_ID")).Enabled = true;
                        ((TextBox)row.FindControl("TextBox_ID")).Text = userGrid.Rows[i]["ID"].ToString();
                        ((TextBox)row.FindControl("TextBox_ID")).Enabled = false;

                        string perm = userGrid.Rows[i]["Permission"].ToString();
                        if (perm == "True")
                        {
                            ((CheckBox)row.FindControl("CheckBox_Permission")).Checked = true;
                        }
                        else
                        {
                            ((CheckBox)row.FindControl("CheckBox_Permission")).Checked = false;
                        }

                        ((CheckBox)row.FindControl("CheckBox_Permission")).Enabled = false;

                        string permOpt = userGrid.Rows[i]["PermissionOpt"].ToString();
                        if (permOpt == "True")
                        {
                            ((CheckBox)row.FindControl("CheckBox_PermissionOpt")).Checked = true;
                        }
                        else
                        {
                            ((CheckBox)row.FindControl("CheckBox_PermissionOpt")).Checked = false;
                        }
                        ((CheckBox)row.FindControl("CheckBox_PermissionOpt")).Enabled = false;

                        string permRedCross = userGrid.Rows[i]["PermissionRedCross"].ToString();
                        if (permRedCross == "True")
                        {
                            ((CheckBox)row.FindControl("CheckBox_PermissionRedCross")).Checked = true;
                        }
                        else
                        {
                            ((CheckBox)row.FindControl("CheckBox_PermissionRedCross")).Checked = false;
                        }
                        ((CheckBox)row.FindControl("CheckBox_PermissionRedCross")).Enabled = false;

                        string techSupport = userGrid.Rows[i]["TechSupport"].ToString();
                        if (techSupport == "True")
                        {
                            ((CheckBox)row.FindControl("CheckBox_TechSupport")).Checked = true;
                        }
                        else
                        {
                            ((CheckBox)row.FindControl("CheckBox_TechSupport")).Checked = false;
                        }
                        ((CheckBox)row.FindControl("CheckBox_TechSupport")).Enabled = false;

                        if (queryLogin.Permission == false)
                        {
                            ((Button)row.FindControl("Button_GridView_Remove")).Visible = false;
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Button_GridView_Edit_Click(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_Users.Rows[idRow];

            ((TextBox)row.FindControl("TextBox_ID")).Enabled = true;
            int id = Convert.ToInt32(((TextBox)row.FindControl("TextBox_ID")).Text);
            ((TextBox)row.FindControl("TextBox_ID")).Enabled = false;

            TextBox_AddUser_ID.Text = id.ToString();

            Reset_AddUser();

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = (from u in context.Users
                                 where u.id == id
                                 select u).FirstOrDefault();

                    TextBox_AddUser_Username.Text = query.Username;
                    TextBox_AddUser_Password.Text = query.Password;
                    TextBox_AddUser_FirstName.Text = query.FirstName;
                    TextBox_AddUser_LastName.Text = query.LastName;
                    TextBox_AddUser_Email.Text = query.Email;
                    CheckBox_AddUser_Permission.Checked = query.Permission;
                    CheckBox_AddUser_PermissionOpt.Checked = query.Permission_opt;
                    CheckBox_AddUser_PermissionRedCross.Checked = query.Permission_RedCross;
                    CheckBox_AddUser_TechnicalSupport.Checked = query.Permission_TechSupport;

                    int idUser = Convert.ToInt32(Session["idUser"]);
                    var queryLogin = (from u in context.Users
                                      where u.id == idUser
                                      select u).FirstOrDefault();

                    if (queryLogin.Permission == false) // if not an admin, then can't edit their username 
                    {
                        TextBox_AddUser_Username.Enabled = false;
                        CheckBox_AddUser_Permission.Enabled = false;
                        CheckBox_AddUser_PermissionOpt.Enabled = false;
                        CheckBox_AddUser_PermissionRedCross.Enabled = false;
                        CheckBox_AddUser_TechnicalSupport.Enabled = false;
                        Label_Warning_NoAdminPriv.Visible = true;
                    }
                    else
                    {
                        TextBox_AddUser_Username.Enabled = true;
                        CheckBox_AddUser_Permission.Enabled = true;
                        CheckBox_AddUser_PermissionOpt.Enabled = true;
                        CheckBox_AddUser_PermissionRedCross.Enabled = true;
                        CheckBox_AddUser_TechnicalSupport.Enabled = true;
                        Label_Warning_NoAdminPriv.Visible = false;
                    }
                    MultiView1.ActiveViewIndex = 1;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Button_User_Add_Click(object sender, EventArgs e)
        {
            int UserId = -1;
            TextBox_AddUser_ID.Text = UserId.ToString();

            MultiView1.ActiveViewIndex = 1;
            Reset_AddUser();
        }

        protected void Button_User_Save_Click(object sender, EventArgs e)
        {
            int ck = 0;
            if (TextBox_AddUser_Username.Text == "")
            {
                Label_Error_AddUser_Username.Visible = true;
                ck = 1;
            }
            if (TextBox_AddUser_Password.Text == "")
            {
                Label_Error_AddUser_Password.Visible = true;
                ck = 1;
            }
            if (TextBox_AddUser_FirstName.Text == "")
            {
                Label_Error_AddUser_FirstName.Visible = true;
                ck = 1;
            }
            if (TextBox_AddUser_LastName.Text == "")
            {
                Label_Error_AddUser_LastName.Visible = true;
                ck = 1;
            }
            if (TextBox_AddUser_Email.Text == "")
            {
                Label_Error_AddUser_Email.Visible = true;
                ck = 1;
            }
            if (ck == 1)
            {
                return;
            }
            Label_Error_AddUser_Username.Visible = false;
            Label_Error_AddUser_Password.Visible = false;
            Label_Error_AddUser_FirstName.Visible = false;
            Label_Error_AddUser_LastName.Visible = false;
            Label_Error_AddUser_Email.Visible = false;

            int user_dam_opt = 0;
            int user_id_save = 0;

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int UserIdUse = Convert.ToInt32(TextBox_AddUser_ID.Text);
                    if (UserIdUse < 1)  // if new user
                    {
                        var query = from c in context.Users
                                    orderby c.id descending
                                    select c;

                        User User_Add = new User();
                        if (query.Count() > 0)
                        {
                            User_Add.id = query.First().id + 1;
                        }
                        else
                        {
                            User_Add.id = 1;
                        }
                        user_id_save = User_Add.id;

                        User_Add.Username = TextBox_AddUser_Username.Text;
                        User_Add.Password = TextBox_AddUser_Password.Text;
                        User_Add.FirstName = TextBox_AddUser_FirstName.Text;
                        User_Add.LastName = TextBox_AddUser_LastName.Text;
                        User_Add.Permission = CheckBox_AddUser_Permission.Checked;
                        User_Add.Permission_opt = CheckBox_AddUser_PermissionOpt.Checked;
                        User_Add.Permission_RedCross = CheckBox_AddUser_PermissionRedCross.Checked;
                        User_Add.Permission_TechSupport = CheckBox_AddUser_TechnicalSupport.Checked;
                        User_Add.Email = TextBox_AddUser_Email.Text;

                        context.Users.Add(User_Add);
                        context.SaveChanges();

                        // TODO if user has Nangbeto privileges, then add as a contact for automated run email
                        if (User_Add.Permission_opt == true) user_dam_opt = 1;
                    }
                    else  // if existing user
                    {
                        var queryUser = (from c in context.Users
                                    where c.id == UserIdUse
                                    select c).First();

                        if (queryUser.Permission_opt == false & CheckBox_AddUser_PermissionOpt.Checked == true)
                        {
                            // add to metaXAction
                            user_dam_opt = 1;
                        }
                        if (queryUser.Permission_opt == true & CheckBox_AddUser_PermissionOpt.Checked == false)
                        {
                            //remove
                            user_dam_opt = 2;
                        }

                        queryUser.Username = TextBox_AddUser_Username.Text;
                        queryUser.Password = TextBox_AddUser_Password.Text;
                        queryUser.FirstName = TextBox_AddUser_FirstName.Text;
                        queryUser.LastName = TextBox_AddUser_LastName.Text;
                        queryUser.Permission = CheckBox_AddUser_Permission.Checked;
                        queryUser.Permission_opt = CheckBox_AddUser_PermissionOpt.Checked;
                        queryUser.Permission_RedCross = CheckBox_AddUser_PermissionRedCross.Checked;
                        queryUser.Permission_TechSupport = CheckBox_AddUser_TechnicalSupport.Checked;
                        queryUser.Email = TextBox_AddUser_Email.Text;

                        context.SaveChanges();

                        user_id_save = UserIdUse;

                    }
                }
                if (user_dam_opt == 1)
                {
                    // add if not already tehre
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        var querymXa = from q in context.MetaXActions
                                       where q.idUsers == user_id_save
                                       select q;

                        if (querymXa.Count() < 1)
                        {
                                var queryMxA_add = from q in context.MetaXActions
                                                   orderby q.id descending
                                                   select q;

                                MetaXAction mXa_add = new MetaXAction();
                                int mXa_id_use = 0;
                                if (queryMxA_add.Count() > 0)
                                {
                                    mXa_id_use = queryMxA_add.First().id + 1;
                                }
                                else
                                {
                                    mXa_id_use = 1;
                                }

                                var queryMeta = from m in context.Metascenarios
                                                select m;

                                mXa_add.idAction = 1;   // hardwire to 1 for email
                                mXa_add.idUsers = user_id_save;

                                foreach (Metascenario mm in queryMeta)
                                {
                                    mXa_add.id = mXa_id_use;
                                    mXa_add.idMetascenarios = mm.id;

                                    using (Togo_Test_dataEntities context_add = new Togo_Test_dataEntities())
                                    {
                                        context_add.MetaXActions.Add(mXa_add);
                                        context_add.SaveChanges();
                                    }
                                    //context.MetaXActions.Add(mXa_add);
                                    mXa_id_use = mXa_id_use + 1;
                                }
                        }
                        else
                        {
                            // do nothing? or check that all 5 are there?
                        }
                    }
                }
                if (user_dam_opt == 2)
                {
                    //remove
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        var querymXa = from q in context.MetaXActions
                                       where q.idUsers == user_id_save
                                       select q;

                        foreach (MetaXAction ma in querymXa)
                        {
                            context.MetaXActions.Remove(ma);
                        }
                        context.SaveChanges();
                    }
                }

            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            Load_GridView_Users();
            MultiView1.ActiveViewIndex = 0;
            Reset_AddUser();
        }

        protected void Button_User_Cancel_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
            Reset_AddUser();
        }

        protected void Reset_AddUser()
        {
            TextBox_AddUser_Username.Text = "";
            TextBox_AddUser_Password.Text = "";
            TextBox_AddUser_FirstName.Text = "";
            TextBox_AddUser_LastName.Text = "";
            TextBox_AddUser_Email.Text = "";
            CheckBox_AddUser_Permission.Checked = false;
            CheckBox_AddUser_PermissionOpt.Checked = false;
            CheckBox_AddUser_PermissionRedCross.Checked = false;
            CheckBox_AddUser_TechnicalSupport.Checked = false;

            Label_Error_AddUser_Username.Visible = false;
            Label_Error_AddUser_Password.Visible = false;
            Label_Error_AddUser_FirstName.Visible = false;
            Label_Error_AddUser_LastName.Visible = false;
            Label_Error_AddUser_Email.Visible = false;
        }

        protected void Button_GridView_Remove_Click(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_Users.Rows[idRow];

            ((TextBox)row.FindControl("TextBox_ID")).Enabled = true;
            int id = Convert.ToInt32(((TextBox)row.FindControl("TextBox_ID")).Text);
            ((TextBox)row.FindControl("TextBox_ID")).Enabled = false;

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query_mXa = from m in context.MetaXActions
                                     where m.idUsers == id
                                     select m;

                    foreach (MetaXAction mm in query_mXa)
                    {
                        context.MetaXActions.Remove(mm);
                    }
                    
                    var query = (from u in context.Users
                                 where u.id == id
                                 select u).FirstOrDefault();

                    context.Users.Remove(query);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
            Load_GridView_Users();
        }
    }
}