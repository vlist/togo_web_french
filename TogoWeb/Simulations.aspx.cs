﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.Net.Mail;
using System.Net;

namespace TogoWeb
{
    public partial class Simulations : System.Web.UI.Page
    {
        static TraceSource trace;

        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);

            if (!IsPostBack)
            {
                Session.Add("idView", 0);
                Load_Header_Text();
                Load_Simulation_GridView();

                Button_Review.Visible = false;

                bool damOperator = new bool();

                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {

                        var use = from u in context.Users
                                  where u.id == idUser
                                  select u;

                        if (use.Count() > 0)
                        {
                            damOperator = use.FirstOrDefault().Permission_opt;
                            if (damOperator == true)
                            {
                                Button_Review.Visible = true;
                            }
                            else
                            {
                                Button_Review.Visible = false;
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
        }

        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }

        protected void Load_Header_Text()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var mod = (from m in context.ModelRuns
                              orderby m.RunStartTime descending
                              select m).FirstOrDefault();

                    SelectedSim_TextBox_Header.Text = Convert.ToString(mod.id);

                    DateTime StartTime = Convert.ToDateTime(mod.StartTime);
                    DateTime EndTime = Convert.ToDateTime(mod.EndTime);
                    StartTime_TextHeader.Text = StartTime.ToString("dd MMM yyyy");
                    EndTime_TextHeader.Text = EndTime.ToString("dd MMM yyyy");

                    if (mod.idScenariosXMeta > 0)
                    {
                        SelectedSim_TextBox_Header.Text = Convert.ToString(mod.id);

                        Label_NoSimulations_Header.Visible = false;

                        var query = from s in context.ScenariosXMetas
                                    where s.idScenarios == mod.idScenariosXMeta
                                    select s;

                        ImageReset();
                        ImageResetOpt();

                        if (query.Count() > 0)
                        {
                            var metaEng = (from n in context.Metascenarios
                                        where n.id == query.FirstOrDefault().idMetascenario
                                        select n).FirstOrDefault();

                            var meta = (from no in context.MetascenariosFrenches
                                        where no.id == metaEng.idMetascenariosFrench
                                        select no).FirstOrDefault();

                            Risk_Text_Header.Text = meta.Risk;

                            Image_Null_Header.Visible = false;

                            if (meta.Name == "A")
                            {
                                Image_A_Header.Visible = true;
                            }
                            if (meta.Name == "B")
                            {
                                Image_B_Header.Visible = true;
                            }
                            if (meta.Name == "C")
                            {
                                Image_C_Header.Visible = true;
                            }
                            if (meta.Name == "D")
                            {
                                Image_D_Header.Visible = true;
                            }
                            if (meta.Name == "E")
                            {
                                Image_E_Header.Visible = true;
                            }

                            var queryOpt = from sO in context.ScenariosXMetas
                                           where sO.idScenarios == mod.idScenariosXMetaOpt
                                           select sO;

                            if (queryOpt.Count() > 0)
                            {
                                var metaOptEng = (from n in context.Metascenarios
                                               where n.id == queryOpt.FirstOrDefault().idMetascenario
                                               select n).FirstOrDefault();

                                var metaOpt = (from no in context.MetascenariosFrenches
                                               where no.id == metaOptEng.idMetascenariosFrench
                                               select no).FirstOrDefault();

                                Risk_Calc_Text_Header.Text = metaOpt.Risk;

                                Image_Null_Calc_Header.Visible = false;

                                if (metaOpt.Name == "A")
                                {
                                    Image_A_Calc_Header.Visible = true;
                                }
                                if (metaOpt.Name == "B")
                                {
                                    Image_B_Calc_Header.Visible = true;
                                }
                                if (metaOpt.Name == "C")
                                {
                                    Image_C_Calc_Header.Visible = true;
                                }
                                if (metaOpt.Name == "D")
                                {
                                    Image_D_Calc_Header.Visible = true;
                                }
                                if (metaOpt.Name == "E")
                                {
                                    Image_E_Calc_Header.Visible = true;
                                }
                            }
                            else
                            {
                                Image_Null_Calc_Header.Visible = true;
                                Risk_Calc_Text_Header.Text = "en attente de validation par operateur de barrage";
                            }
                        }
                        else
                        {
                            Image_Null_Header.Visible = true;
                        }

                    }
                    else
                    {
                        Label_NoSimulations_Header.Visible = true;

                        SelectedSim_TextBox_Header.Text = "";

                        Risk_Text_Header.Text = "Non disponible";
                        Risk_Calc_Text_Header.Text = "Non disponible";

                        if (mod.Status.ToString() == "Ongoing")
                        {
                            Risk_Text_Header.Text = "Calculateur... réessayez dans quelques minutes";
                            Risk_Calc_Text_Header.Text = "Validation par l'opérateur du barrage sera nécessaire";
                        }

                        ImageReset();

                        Image_Null_Header.Visible = true;
                        Image_Null_Calc_Header.Visible = true;

                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void Load_Simulation_GridView()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var mod = from m in context.ModelRuns
                              orderby m.RunStartTime descending
                              select m;

                    DataTable modTable = new DataTable();
                    modTable.Columns.Add("Id", typeof(string));
                    modTable.Columns.Add("StartTime", typeof(string));
                    modTable.Columns.Add("EndTime", typeof(string));
                    modTable.Columns.Add("Status", typeof(string));
                    modTable.Columns.Add("RunStartTime", typeof(string));
                    modTable.Columns.Add("RiskLetter", typeof(string));
                    modTable.Columns.Add("RiskDescription", typeof(string));
                    modTable.Columns.Add("RiskLetterOpt", typeof(string));
                    modTable.Columns.Add("RiskDescriptionOpt", typeof(string));

                    if (mod.Count() > 0)
                    {
                        foreach (ModelRun mm in mod)
                        {
                            DateTime RunStart = Convert.ToDateTime(mm.RunStartTime);

                            var query = from r in context.ScenariosXMetas
                                        where r.idScenarios == mm.idScenariosXMeta
                                        select r;

                            if (query.Count() > 0)
                            {

                                var metaEng = (from n in context.Metascenarios
                                            where n.id == query.FirstOrDefault().idMetascenario
                                            select n).FirstOrDefault();

                                var meta = (from no in context.MetascenariosFrenches
                                               where no.id == metaEng.idMetascenariosFrench
                                               select no).FirstOrDefault();

                                if (mm.idScenariosXMetaOpt > 0)
                                {
                                    var queryOpt = from rr in context.ScenariosXMetas
                                                   where rr.idScenarios == mm.idScenariosXMetaOpt
                                                   select rr;

                                    if (queryOpt.Count() > 0)
                                    {
                                        var metaOptEng = (from nn in context.Metascenarios
                                                       where nn.id == queryOpt.FirstOrDefault().idMetascenario
                                                       select nn).FirstOrDefault();

                                        var metaOpt = (from nno in context.MetascenariosFrenches
                                                       where nno.id == metaOptEng.idMetascenariosFrench
                                                       select nno).FirstOrDefault();
                                        
                                        modTable.Rows.Add(mm.id, mm.StartTime.ToString("dd MMM yyyy"), mm.EndTime.ToString("dd MMM yyyy"), mm.Status, RunStart.ToString("dd MMM yyyy HH:mm"), meta.Name, meta.Risk, metaOpt.Name, metaOpt.Risk);
                                    }
                                }
                                else
                                {
                                    modTable.Rows.Add(mm.id, mm.StartTime.ToString("dd MMM yyyy"), mm.EndTime.ToString("dd MMM yyyy"), mm.Status, RunStart.ToString("dd MMM yyyy HH:mm"), meta.Name, meta.Risk, "N/A", "N/A");
                                }
                            }
                            else
                            {
                                modTable.Rows.Add(mm.id, mm.StartTime.ToString("dd MMM yyyy"), mm.EndTime.ToString("dd MMM yyyy"), mm.Status, RunStart.ToString("dd MMM yyyy HH:mm"), mm.Status, mm.Status, "N/A", "N/A");
                            }
                        }
                        Label_NoSimulations.Visible = false;
                    }
                    else
                    {
                        Label_NoSimulations.Visible = true;
                    }

                    Simulations_GridView.DataSource = modTable;
                    Simulations_GridView.DataBind();

                    for (int i=0; i<modTable.Rows.Count; i++)
                    {
                        //AUTOMATED PREDICTION
                        if (modTable.Rows[i]["RiskLetter"].ToString() == "A")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null")).Visible = false;
                        }
                        else if (modTable.Rows[i]["RiskLetter"].ToString() == "B")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null")).Visible = false;
                        }
                        else if (modTable.Rows[i]["RiskLetter"].ToString() == "C")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null")).Visible = false;
                        }
                        else if (modTable.Rows[i]["RiskLetter"].ToString() == "D")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null")).Visible = false;
                        }
                        else if (modTable.Rows[i]["RiskLetter"].ToString() == "E")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null")).Visible = false;
                        }
                        else
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null")).Visible = true;
                        }

                        //USER PREDICTION
                        if (modTable.Rows[i]["RiskLetterOpt"].ToString() == "A")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A_Opt")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null_Opt")).Visible = false;
                        }
                        else if (modTable.Rows[i]["RiskLetterOpt"].ToString() == "B")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B_Opt")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null_Opt")).Visible = false;
                        }
                        else if (modTable.Rows[i]["RiskLetterOpt"].ToString() == "C")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C_Opt")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null_Opt")).Visible = false;
                        }
                        else if (modTable.Rows[i]["RiskLetterOpt"].ToString() == "D")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D_Opt")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null_Opt")).Visible = false;
                        }
                        else if (modTable.Rows[i]["RiskLetterOpt"].ToString() == "E")
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E_Opt")).Visible = true;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null_Opt")).Visible = false;
                        }
                        else
                        {
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_A_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_B_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_C_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_D_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_E_Opt")).Visible = false;
                            ((Image)Simulations_GridView.Rows[i].FindControl("Image_null_Opt")).Visible = true;
                        }

                        //VALIDATED BUTTON
                        if (modTable.Rows[i]["Status"].ToString() == "Complete")
                        {
                            if (modTable.Rows[i]["RiskLetterOpt"].ToString() != "N/A")
                            {
                                ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).Text = "Oui";
                                ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).Enabled = true;
                                ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).BackColor = System.Drawing.Color.Green;
                            }
                            else
                            {
                                ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).Text = "Validez!";
                                ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).Enabled = true;
                                ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).BackColor = System.Drawing.Color.Red; ;
                            }
                        }
                        else
                        {
                            ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).Text = "N/A";
                            ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).Enabled = false;
                            ((Button)Simulations_GridView.Rows[i].FindControl("Validated_Button")).BackColor = System.Drawing.Color.LightGray;
                        }

                        // HIDE VALIDATE COLUMN IF HAVE DAM OPERATOR PERMISSION
                        Simulations_GridView.Columns[12].Visible = true;

                        bool damOperator = new bool();
                        int idUser = Convert.ToInt32(Session["idUser"]);
                        
                        var use = from u in context.Users
                                    where u.id == idUser
                                    select u;

                        if (use.Count() > 0)
                        {
                            damOperator = use.FirstOrDefault().Permission_opt;
                            if (damOperator == true)
                            {
                                Simulations_GridView.Columns[12].Visible = true;
                                //if a dam operator, then show the validate button column on the simulations table
                            }
                            else
                            {
                                Simulations_GridView.Columns[12].Visible = false;
                                //if not a dam operator, then show basic simulations summary table
                            }
                        }
                        else
                        {
                            damOperator = false;
                            Simulations_GridView.Columns[12].Visible = false;
                            // if no user login, then just show basic simulations summary table
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Details_Button_Command(object sender, CommandEventArgs e)
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int rowIndex = Convert.ToInt32(e.CommandArgument);
                    GridViewRow rowtest = Simulations_GridView.Rows[rowIndex];
                    int selectedSim = Convert.ToInt32(rowtest.Cells[0].Text);
                    SelectedSim_TextBox.Text = selectedSim.ToString();

                    var sim = (from s in context.ModelRuns
                              where s.id == selectedSim
                              select s).FirstOrDefault();

                    StartTime_Text.Text = sim.StartTime.Date.ToString("dd MMM yyyy");
                    EndTime_Text.Text = sim.EndTime.Date.ToString("dd MMM yyyy");
                    Status_Text.Text = sim.Status.ToString();
                    DateTime RunStart = Convert.ToDateTime(sim.RunStartTime);
                    RunStartTime_Text.Text = RunStart.ToString("dd MMM yyyy HH:mm");

                    var mXs = from ms in context.ScenariosXMetas
                              where ms.id == sim.idScenariosXMeta
                              select ms;

                    if (mXs.Count() > 0)
                    {
                        var metaEng = (from mm in context.Metascenarios
                                    where mm.id == mXs.FirstOrDefault().idMetascenario
                                    select mm).FirstOrDefault();

                        var meta = (from mmo in context.MetascenariosFrenches
                                    where mmo.id == metaEng.idMetascenariosFrench
                                    select mmo).FirstOrDefault();

                        // if auto risk
                        Risk_Text.Text = meta.Risk.ToString();
                        Description_Text.Text = meta.Description.ToString();

                        if (sim.idScenariosXMetaOpt > 0)
                        {
                            var mXs_opt = from mso in context.ScenariosXMetas
                                          where mso.id == sim.idScenariosXMetaOpt
                                          select mso;

                            if (mXs_opt.Count() > 0)
                            {
                                var meta_optEng = (from mmo in context.Metascenarios
                                                where mmo.id == mXs_opt.FirstOrDefault().idMetascenario
                                                select mmo).FirstOrDefault();

                                var meta_opt = (from mmoo in context.MetascenariosFrenches
                                                where mmoo.id == meta_optEng.idMetascenariosFrench
                                                select mmoo).FirstOrDefault();

                                Risk_Text_Dam.Text = meta_opt.Risk.ToString();
                                Description_Text_Dam.Text = meta_opt.Description.ToString();
                            }
                        }
                        else
                        {
                            Risk_Text_Dam.Text = "en attente de validation par operateur de barrager";
                            Description_Text_Dam.Text = "Pas applicable";
                        }
                    }
                    else
                    {
                        if (sim.Status == "Ongoing")
                        {
                            Risk_Text.Text = "Calculateur... réessayez dans quelques minutes";
                            Description_Text.Text = "Pas applicable";
                            Risk_Text_Dam.Text = "Validation par l'opérateur du barrage sera nécessaire";
                            Description_Text_Dam.Text = "Pas applicable";
                        }
                        else
                        {
                            Risk_Text.Text = "Non disponible";
                            Description_Text.Text = "Non disponible";
                            Risk_Text_Dam.Text = "Non disponible";
                            Description_Text_Dam.Text = "Non disponible";
                        }
                    }
                }
            }

            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            MultiView1.ActiveViewIndex = 1;
        }

        protected void Ok_Button_Click(object sender, EventArgs e)
        {
            ResetTextboxes();
            Load_Simulation_GridView();
            Load_Header_Text();
            MultiView1.ActiveViewIndex = 0;
        }
        protected void ResetTextboxes()
        {
            StartTime_Text.Text = "";
            EndTime_Text.Text = "";
            Status_Text.Text = "";
            RunStartTime_Text.Text = "";
            Risk_Text.Text = "";
            Description_Text.Text = "";
        }

        protected void Modify_Button_Click(object sender, EventArgs e)
        {
            Label27.Visible = true;
            Calculate_Button.Visible = true;
            FlowMinus2_TextBox.Visible = true;
            FlowMinus1_TextBox.Visible = true;
            Flow0_TextBox.Visible = true;
            FlowPlus1_TextBox.Visible = true;
            FlowPlus2_TextBox.Visible = true;
            Flow0_TextBox.Enabled = true;
            FlowPlus2_TextBox.Enabled = true;
            FlowPlus1_TextBox.Enabled = true;

            Modify_Button.Visible = false;
            AcceptSend_Button.Visible = false;

            Label21.Visible = false;
            Risk_Calculated_Text.Visible = false;
            Description_Text_Calc.Visible = false;
            Image_A_Calc.Visible = false;
            Image_B_Calc.Visible = false;
            Image_C_Calc.Visible = false;
            Image_D_Calc.Visible = false;
            Image_E_Calc.Visible = false;
            Image_Null_Calc.Visible = false;
            SaveSend_Button.Visible = false;
        }

        protected void Calculate_Button_Click(object sender, EventArgs e)
        {
            int selectedSim = Convert.ToInt32(SelectedSim_TextBox_Calc.Text);

            List<double> target_down = new List<double>();
            target_down.Add(Convert.ToDouble(FlowMinus2_TextBox.Text));
            target_down.Add(Convert.ToDouble(FlowMinus1_TextBox.Text));
            target_down.Add(Convert.ToDouble(Flow0_TextBox.Text));
            target_down.Add(Convert.ToDouble(FlowPlus1_TextBox.Text));
            target_down.Add(Convert.ToDouble(FlowPlus2_TextBox.Text));

            List<sc_val> scenarios = new List<sc_val>();

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var varSc = from ss in context.Scenarios
                                select ss;

                    foreach (Scenario s in varSc)
                    {
                        sc_val scItem = new sc_val();
                        scItem.num = s.Number;
                        scItem.flow1 = s.Flow1;
                        scItem.flow2 = s.Flow2;
                        scItem.flow3 = s.Flow3;
                        scItem.flow4 = s.Flow4;
                        scItem.flow5 = s.Flow5;

                        scenarios.Add(scItem);
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            sc_val result_down = findclosest(target_down, scenarios);

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var varSc = (from ss in context.Scenarios
                                 where ss.Number == result_down.num
                                 select ss).FirstOrDefault();

                    var varScMeta = (from sm in context.ScenariosXMetas
                                     where sm.idScenarios == varSc.id
                                     select sm).FirstOrDefault();

                    var varMetaEng = (from mm in context.Metascenarios
                                   where mm.id == varScMeta.idMetascenario
                                   select mm).FirstOrDefault();

                    var varMeta = (from mm in context.MetascenariosFrenches
                                   where mm.id == varMetaEng.idMetascenariosFrench
                                   select mm).FirstOrDefault();

                    Risk_Calculated_Text.Text = varMeta.Risk;
                    Description_Text_Calc.Text = varMeta.Description;
                    Calc_ScenarioID.Text = varSc.id.ToString();

                    if (varMeta.Name == "A")
                    {
                        Image_A_Calc.Visible = true;
                        Image_B_Calc.Visible = false;
                        Image_C_Calc.Visible = false;
                        Image_D_Calc.Visible = false;
                        Image_E_Calc.Visible = false;
                        Image_Null_Calc.Visible = false;
                    }
                    else if (varMeta.Name == "B")
                    {
                        Image_A_Calc.Visible = false;
                        Image_B_Calc.Visible = true;
                        Image_C_Calc.Visible = false;
                        Image_D_Calc.Visible = false;
                        Image_E_Calc.Visible = false;
                        Image_Null_Calc.Visible = false;
                    }
                    else if (varMeta.Name == "C")
                    {
                        Image_A_Calc.Visible = false;
                        Image_B_Calc.Visible = false;
                        Image_C_Calc.Visible = true;
                        Image_D_Calc.Visible = false;
                        Image_E_Calc.Visible = false;
                        Image_Null_Calc.Visible = false;
                    }
                    else if (varMeta.Name == "D")
                    {
                        Image_A_Calc.Visible = false;
                        Image_B_Calc.Visible = false;
                        Image_C_Calc.Visible = false;
                        Image_D_Calc.Visible = true;
                        Image_E_Calc.Visible = false;
                        Image_Null_Calc.Visible = false;
                    }
                    else if (varMeta.Name == "E")
                    {
                        Image_A_Calc.Visible = false;
                        Image_B_Calc.Visible = false;
                        Image_C_Calc.Visible = false;
                        Image_D_Calc.Visible = false;
                        Image_E_Calc.Visible = true;
                        Image_Null_Calc.Visible = false;
                    }
                    else
                    {
                        Image_A_Calc.Visible = false;
                        Image_B_Calc.Visible = false;
                        Image_C_Calc.Visible = false;
                        Image_D_Calc.Visible = false;
                        Image_E_Calc.Visible = false;
                        Image_Null_Calc.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            Label21.Visible = true;
            Risk_Calculated_Text.Visible = true;
            Description_Text_Calc.Visible = true;
            SaveSend_Button.Visible = true;

            Calculate_Button.Visible = false;
            Flow0_TextBox.Enabled = false;
            FlowPlus1_TextBox.Enabled = false;
            FlowPlus2_TextBox.Enabled = false;
            Modify_Button.Visible = true;
            AcceptSend_Button.Visible = false;
        }

        public static sc_val findclosest(List<double> target, List<sc_val> scenarios)
        {
            double mindist = 0;
            int ck = 0;
            sc_val closest = new sc_val();

            foreach (sc_val sc in scenarios)
            {
                int number = sc.num;

                List<double> flows = new List<double> { sc.flow1, sc.flow2, sc.flow3, sc.flow4, sc.flow5 };
                double dist = 0;
                for (int i = 0; i < flows.Count(); i++)
                {
                    double difference = Math.Pow((target[i] - flows[i]), 2.0);
                    dist = dist + difference;
                }
                dist = Math.Pow(dist, 0.5);
                if (ck == 0)
                {
                    mindist = dist;
                    ck = 1;
                }
                if (dist <= mindist)
                {
                    mindist = dist;
                    closest = sc;
                }
            }
            return closest;
        }
        public class sc_val
        {
            public int num;
            public double flow1;
            public double flow2;
            public double flow3;
            public double flow4;
            public double flow5;
        }

        protected void Cancel_Button_Click(object sender, EventArgs e)
        {
            Label_AcceptSend_Alert_Confirm.Visible = false;
            AcceptSend_Button_ConfirmYes.Visible = false;
            AcceptSend_Button_ConfirmCancel.Visible = false;
            Label_SaveSend_Alert_Confirm.Visible = false;
            SaveSend_Button_ConfirmYes.Visible = false;
            SaveSend_Button_ConfirmCancel.Visible = false;

            MultiView1.ActiveViewIndex = 0;
        }

        protected void Send_Button_Click(object sender, EventArgs e)
        {
            int selectedSim = Convert.ToInt32(SelectedSim_TextBox_Calc.Text);
            DateTime StartTimeSave = new DateTime();
            int varXstaID = new int();

            try
            {
                //SAVE calculated simulation to database
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var sta = (from s in context.Stations
                               where s.Name == "Nangbeto Dam"
                               select s).FirstOrDefault();

                    var vari = (from v in context.Variables
                                where v.Name == "Outflow"
                                select v).FirstOrDefault();

                    var varXsta = (from vXs in context.VariableXStations
                                   where vXs.idStation == sta.id & vXs.idVariable == vari.id
                                   select vXs).FirstOrDefault();

                    varXstaID = varXsta.id;
                    
                    var sim = (from s in context.ModelRuns
                               where s.id == selectedSim
                               select s).FirstOrDefault();

                    sim.idScenariosXMetaOpt = Convert.ToInt32(Calc_ScenarioID.Text);
                    StartTimeSave = Convert.ToDateTime(sim.RunStartTime);
                    context.SaveChanges();
                }

                //SAVE CALCULATED TARGET TO DATABASE
                List<double> target_down = new List<double>();
                target_down.Add(Convert.ToDouble(FlowMinus2_TextBox.Text));
                target_down.Add(Convert.ToDouble(FlowMinus1_TextBox.Text));
                target_down.Add(Convert.ToDouble(Flow0_TextBox.Text));
                target_down.Add(Convert.ToDouble(FlowPlus1_TextBox.Text));
                target_down.Add(Convert.ToDouble(FlowPlus2_TextBox.Text));

                int count = -2;
                foreach (double ff in target_down)
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        var modValAll = from m in context.ModelValues
                                        orderby m.id descending
                                        select m;

                        ModelValue modValAdd = new ModelValue();

                        if (modValAll.Count() > 0)
                        {
                            modValAdd.id = modValAll.First().id + 1;
                        }
                        else
                        {
                            modValAdd.id = 1;
                        }

                        modValAdd.idVariableXStation = varXstaID;
                        modValAdd.StartTime = StartTimeSave;
                        modValAdd.Value = ff;
                        modValAdd.Day = count;
                        modValAdd.idModelRun = Convert.ToInt32(selectedSim);
                        modValAdd.Optimized = true;

                        context.ModelValues.Add(modValAdd);
                        context.SaveChanges();

                    }
                    count = count + 1;
                }

                //SEND EMAIL

                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int scCalc = Convert.ToInt32(Calc_ScenarioID.Text);

                    var mod = (from m in context.ModelRuns
                               where m.id == selectedSim
                               select m).FirstOrDefault();

                    var scUse = (from s in context.ScenariosXMetas
                                 where s.idScenarios == scCalc
                                 select s).FirstOrDefault();

                    var metaEng = (from mm in context.Metascenarios
                                where mm.id == scUse.idMetascenario
                                select mm).FirstOrDefault();

                    var meta = (from mm in context.MetascenariosFrenches
                                   where mm.id == metaEng.idMetascenariosFrench
                                   select mm).FirstOrDefault();

                    var mXa = from ma in context.MetaXContacts
                              where ma.idMetascenarios == scUse.idMetascenario
                              select ma;

                    if (mXa.Count() > 0)
                    {
                        foreach (MetaXContact mXaQuery in mXa)
                        {
                            var con = (from c in context.ContactLists
                                      where c.id == mXaQuery.idContact
                                      select c).FirstOrDefault();
                            
                            var act = (from a in context.Actions
                                        where a.id == mXaQuery.idAction
                                        select a).FirstOrDefault();

                            string action = act.Type;
                            bool sendSuccess = false;

                            if (action.ToLower().Substring(0, 5) == "email")
                            {
                                string[] temp;
                                string emailUse = con.Email;
                                temp = emailUse.Split(';');

                                foreach (string s in temp)
                                {
                                    string comAddress = s;
                                    string msgHeader = meta.Header;
                                    string msgMain = "La période de prévision de " + mod.StartTime.ToString("dd MMM yyyy") + " au " + mod.EndTime.ToString("dd MMM yyyy") + " a été analysée par les opérateurs du barrage et indique les conditions suivantes:\n" + meta.Risk.ToString() +" risque (niveau " + meta.Name.ToString() + ") - " + meta.Description.ToString(); 
                                    string msgFooter = meta.Footer;

                                    string msg = msgHeader + "\n\n" + msgMain + "\n\n" + msgFooter;

                                    sendSuccess = SendEmail(comAddress, "Systême d'Alert Précoce: " + Risk_Calc_Text.Text + " Risque", msg);
                                    if (!sendSuccess)
                                    {
                                        trace.TraceEvent(TraceEventType.Error, 1, "Failed to send communication through " + action + " to " + comAddress);
                                        trace.Flush();
                                    }
                                }
                            }
                            else
                            {
                                //action type not setup
                                trace.TraceEvent(TraceEventType.Error, 1, "Failed to send communication - either action format is incorrect or communication type is not setup.  Message not sent");
                                trace.Flush();
                                return;
                            }
                            
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            Modify_Button.Enabled = true;

            Label_AcceptSend_Alert_Confirm.Visible = false;
            AcceptSend_Button_ConfirmYes.Visible = false;
            AcceptSend_Button_ConfirmCancel.Visible = false;
            AcceptSend_Button.Enabled = true;
            
            Label_SaveSend_Alert_Confirm.Visible = false;
            SaveSend_Button_ConfirmYes.Visible = false;
            SaveSend_Button_ConfirmCancel.Visible = false;
            SaveSend_Button.Enabled = true;

            Load_Simulation_GridView();
            Load_Header_Text();
            MultiView1.ActiveViewIndex = 0;
        }

        public static bool SendEmail(string addresses, string subject, string msgBody)
        {
            bool success = false;

            var client = new SmtpClient("smtp.gmail.com", 587)
            {
                Credentials = new NetworkCredential("vlist@envmodeling.com", "c0rTad0s"),
                EnableSsl = true
            };

            MailMessage message = new MailMessage();
            message.From = new MailAddress("vlist@envmodeling.com");  //HARDWIRE: for demonstration only - replace with another email ("soporte@gmail.com")
            message.To.Add(addresses);
            message.Subject = subject;
            message.Body = msgBody;
            message.IsBodyHtml = false;
            message.Priority = MailPriority.Normal;

            try
            {
                client.Send(message);
                Console.WriteLine("Sent email");
                Console.ReadLine();

                success = true;
            }
            catch (Exception ex)
            {
                success = false;
                trace.TraceEvent(TraceEventType.Error, 1, "Error sending email to " + addresses);
                trace.Flush();
            }
            
            return success;
        }

        protected void Validated_Button_Command(object sender, CommandEventArgs e)
        {
            int rowIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow rowtest = Simulations_GridView.Rows[rowIndex];
            int selectedSim = Convert.ToInt32(rowtest.Cells[0].Text);

            Populate_Calculator_Page(selectedSim);

            MultiView1.ActiveViewIndex = 2;
        }

        protected void Populate_Calculator_Page(int simUse)
        {
            int selectedSim = simUse;
            SelectedSim_TextBox_Calc.Text = selectedSim.ToString();

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var sim = (from s in context.ModelRuns
                               where s.id == selectedSim
                               select s).FirstOrDefault();

                    StartTime_Text0.Text = sim.StartTime.Date.ToString("dd MMM yyyy");
                    EndTime_Text0.Text = sim.EndTime.Date.ToString("dd MMM yyyy");

                    var sc = (from scXm in context.ScenariosXMetas
                              where scXm.id == sim.idScenariosXMeta
                              select scXm).FirstOrDefault();

                    var metaEng = (from mm in context.Metascenarios
                                where mm.id == sc.idMetascenario
                                select mm).FirstOrDefault();

                    var meta = (from mm in context.MetascenariosFrenches
                                where mm.id == metaEng.idMetascenariosFrench
                                select mm).FirstOrDefault();

                    Risk_Text0.Text = meta.Risk.ToString();
                    Description_Text0.Text = meta.Description.ToString();
                    SelectedSc_TextBox.Text = sim.idScenariosXMeta.ToString();
                    Risk_Calc_Text.Text = Risk_Text0.Text;
                    Description_Calc_Text.Text = Description_Text0.Text;
                    Calc_ScenarioID.Text = sim.idScenariosXMeta.ToString();

                    if (sim.idScenariosXMetaOpt > 0)
                    {
                        var sc_opt = (from scXm in context.ScenariosXMetas
                                      where scXm.id == sim.idScenariosXMetaOpt
                                      select scXm).FirstOrDefault();

                        var meta_optEng = (from mm in context.Metascenarios
                                        where mm.id == sc_opt.idMetascenario
                                        select mm).FirstOrDefault();

                        var meta_opt = (from mm in context.MetascenariosFrenches
                                        where mm.id == meta_optEng.idMetascenariosFrench
                                        select mm).FirstOrDefault();

                        Risk_Calc_Text.Text = meta_opt.Risk.ToString();
                        Description_Calc_Text.Text = meta_opt.Description.ToString(); 
                        Calc_ScenarioID.Text = sim.idScenariosXMetaOpt.ToString();
                    }
                    else
                    {
                        Risk_Calc_Text.Text = "";
                        Description_Calc_Text.Text = "";
                        Calc_ScenarioID.Text = "";
                    }

                    if (meta.Name == "A")
                    {
                        Image_A.Visible = true;
                        Image_B.Visible = false;
                        Image_C.Visible = false;
                        Image_D.Visible = false;
                        Image_E.Visible = false;
                        Image_Null.Visible = false;
                    }
                    else if (meta.Name == "B")
                    {
                        Image_A.Visible = false;
                        Image_B.Visible = true;
                        Image_C.Visible = false;
                        Image_D.Visible = false;
                        Image_E.Visible = false;
                        Image_Null.Visible = false;
                    }
                    else if (meta.Name == "C")
                    {
                        Image_A.Visible = false;
                        Image_B.Visible = false;
                        Image_C.Visible = true;
                        Image_D.Visible = false;
                        Image_E.Visible = false;
                        Image_Null.Visible = false;
                    }
                    else if (meta.Name == "D")
                    {
                        Image_A.Visible = false;
                        Image_B.Visible = false;
                        Image_C.Visible = false;
                        Image_D.Visible = true;
                        Image_E.Visible = false;
                        Image_Null.Visible = false;
                    }
                    else if (meta.Name == "E")
                    {
                        Image_A.Visible = false;
                        Image_B.Visible = false;
                        Image_C.Visible = false;
                        Image_D.Visible = false;
                        Image_E.Visible = true;
                        Image_Null.Visible = false;
                    }
                    else
                    {
                        Image_A.Visible = false;
                        Image_B.Visible = false;
                        Image_C.Visible = false;
                        Image_D.Visible = false;
                        Image_E.Visible = false;
                        Image_Null.Visible = true;
                    }

                    var sta = (from s in context.Stations
                               where s.Name == "Nangbeto Dam"
                               select s).FirstOrDefault();

                    var vari = (from v in context.Variables
                                where v.Name == "Water Level"
                                select v).FirstOrDefault();

                    var varXsta = (from vXs in context.VariableXStations
                                   where vXs.idStation == sta.id & vXs.idVariable == vari.id
                                   select vXs).FirstOrDefault();

                    var mod = (from m in context.ModelValues
                               where m.idModelRun == selectedSim & m.idVariableXStation == varXsta.id
                               select m).FirstOrDefault();

                    WaterLevel_Text.Text = mod.Value.ToString();

                    var varf = (from v in context.Variables
                                where v.Name == "Outflow"
                                select v).FirstOrDefault();

                    var varXStaf = (from vXs in context.VariableXStations
                                    where vXs.idStation == sta.id & vXs.idVariable == varf.id
                                    select vXs).FirstOrDefault();

                    var modf = from m in context.ModelValues
                               where m.idModelRun == selectedSim & m.idVariableXStation == varXStaf.id & m.Optimized == false
                               orderby m.Day ascending
                               select m;

                    foreach (ModelValue mm in modf)
                    {
                        if (mm.Day == -2)
                        {
                            double flow_tempm2 = mm.Value;
                            FlowDayMinus2_Label.Text = String.Format("{0:0.#}", flow_tempm2);
                            FlowMinus2_TextBox.Text = String.Format("{0:0.#}", flow_tempm2);
                        }
                        else if (mm.Day == -1)
                        {
                            double flow_tempm1 = mm.Value;
                            FlowDayMinus1_Label.Text = String.Format("{0:0.#}", flow_tempm1);
                            FlowMinus1_TextBox.Text = String.Format("{0:0.#}", flow_tempm1);
                        }
                        else if (mm.Day == 0)
                        {
                            double flow_temp = mm.Value;
                            FlowDay0_Label.Text = String.Format("{0:0.#}", flow_temp);
                            Flow0_TextBox.Text = String.Format("{0:0.#}", flow_temp);
                        }
                        else if (mm.Day == 1)
                        {
                            double flow_temp1 = mm.Value;
                            FlowDayPlus1_Label.Text = String.Format("{0:0.#}", flow_temp1);
                            FlowPlus1_TextBox.Text = String.Format("{0:0.#}", flow_temp1);
                        }
                        else if (mm.Day == 2)
                        {
                            double flow_temp2 = mm.Value;
                            FlowDayPlus2_Label.Text = String.Format("{0:0.#}", flow_temp2);
                            FlowPlus2_TextBox.Text = String.Format("{0:0.#}", flow_temp2);
                        }
                        else
                        {
                            trace.TraceEvent(TraceEventType.Critical, 1, "Can not find flow");
                        }
                    }

                    var varf_in = (from v in context.Variables
                                where v.Name == "Inflow"
                                select v).FirstOrDefault();

                    var varXStaf_in = (from vXs in context.VariableXStations
                                    where vXs.idStation == sta.id & vXs.idVariable == varf_in.id
                                    select vXs).FirstOrDefault();

                    var modf_in_m2 = from m in context.ModelValues
                                     where m.idModelRun == selectedSim & m.idVariableXStation == varXStaf_in.id & m.Optimized == false & m.Day == -2
                                     select m;

                    if (modf_in_m2.Count() > 0)
                    {
                        double flow_tempm2_in = modf_in_m2.First().Value;
                        FlowDayMinus2_Label_In.Text = String.Format("{0:0.#}", flow_tempm2_in);
                    }
                    else
                    {
                        FlowDayMinus2_Label_In.Text = "Non disponible";
                    }

                    var modf_in_m1 = from m in context.ModelValues
                                     where m.idModelRun == selectedSim & m.idVariableXStation == varXStaf_in.id & m.Optimized == false & m.Day == -1
                                     select m;

                    if (modf_in_m1.Count() > 0)
                    {
                        double flow_tempm1_in = modf_in_m1.First().Value;
                        FlowDayMinus1_Label_In.Text = String.Format("{0:0.#}", flow_tempm1_in);
                    }
                    else
                    {
                        FlowDayMinus1_Label_In.Text = "Non disponible";
                    }

                    var modf_in = from m in context.ModelValues
                                     where m.idModelRun == selectedSim & m.idVariableXStation == varXStaf_in.id & m.Optimized == false & m.Day == 0
                                     select m;

                    if (modf_in.Count() > 0)
                    {
                        double flow_temp_in = modf_in.First().Value;
                        FlowDay0_Label_In.Text = String.Format("{0:0.#}", flow_temp_in);
                    }
                    else
                    {
                        FlowDay0_Label_In.Text = "Non disponible";
                    }

                    var modf_in_p1 = from m in context.ModelValues
                                  where m.idModelRun == selectedSim & m.idVariableXStation == varXStaf_in.id & m.Optimized == false & m.Day == 1
                                  select m;

                    if (modf_in_p1.Count() > 0)
                    {
                        double flow_temp1_in = modf_in_p1.First().Value;
                        FlowDayPlus1_Label_In.Text = String.Format("{0:0.#}", flow_temp1_in);
                    }
                    else
                    {
                        FlowDayPlus1_Label_In.Text = "Non disponible";
                    }

                    var modf_in_p2 = from m in context.ModelValues
                                     where m.idModelRun == selectedSim & m.idVariableXStation == varXStaf_in.id & m.Optimized == false & m.Day == 2
                                     select m;

                    if (modf_in_p2.Count() > 0)
                    {
                        double flow_temp2_in = modf_in_p2.First().Value;
                        FlowDayPlus2_Label_In.Text = String.Format("{0:0.#}", flow_temp2_in);
                    }
                    else
                    {
                        FlowDayPlus2_Label_In.Text = "Non disponible";
                    }

                    if (sim.idScenariosXMetaOpt > 0)
                    {
                        Modify_Button.Visible = false;
                        AcceptSend_Button.Visible = false;
                        Label25.Visible = true;

                        Label27.Visible = true;
                        FlowMinus2_TextBox.Visible = true;
                        FlowMinus1_TextBox.Visible = true;
                        Flow0_TextBox.Visible = true;
                        FlowPlus1_TextBox.Visible = true;
                        FlowPlus2_TextBox.Visible = true;

                        Flow0_TextBox.Enabled = false;
                        FlowPlus1_TextBox.Enabled = false;
                        FlowPlus2_TextBox.Enabled = false;

                        var modfopt = from m in context.ModelValues
                                      where m.idModelRun == selectedSim & m.idVariableXStation == varXStaf.id & m.Optimized == true
                                      orderby m.Day ascending
                                      select m;

                        foreach (ModelValue mm in modfopt)
                        {
                            if (mm.Day == -2)
                            {
                                double flow_tempm2 = mm.Value;
                                FlowMinus2_TextBox.Text = String.Format("{0:0.#}", flow_tempm2);
                            }
                            else if (mm.Day == -1)
                            {
                                double flow_tempm1 = mm.Value;
                                FlowMinus1_TextBox.Text = String.Format("{0:0.#}", flow_tempm1);
                            }
                            else if (mm.Day == 0)
                            {
                                double flow_temp = mm.Value;
                                Flow0_TextBox.Text = String.Format("{0:0.#}", flow_temp);
                            }
                            else if (mm.Day == 1)
                            {
                                double flow_temp1 = mm.Value;
                                FlowPlus1_TextBox.Text = String.Format("{0:0.#}", flow_temp1);
                            }
                            else if (mm.Day == 2)
                            {
                                double flow_temp2 = mm.Value;
                                FlowPlus2_TextBox.Text = String.Format("{0:0.#}", flow_temp2);
                            }
                            else
                            {
                                trace.TraceEvent(TraceEventType.Critical, 1, "Can not find flow");
                            }
                        }

                        var scOpt = (from scXm in context.ScenariosXMetas
                                     where scXm.id == sim.idScenariosXMetaOpt
                                     select scXm).FirstOrDefault();

                        var metaOptEng = (from m in context.Metascenarios
                                       where m.id == scOpt.idMetascenario
                                       select m).FirstOrDefault();

                        var metaOpt = (from mo in context.MetascenariosFrenches
                                       where mo.id == metaOptEng.idMetascenariosFrench
                                       select mo).FirstOrDefault();

                        Risk_Calc_Text.Visible = true;
                        Risk_Calc_Text.Text = metaOpt.Risk;
                        Description_Calc_Text.Visible = true;
                        Description_Calc_Text.Text = metaOpt.Description;

                        if (metaOpt.Name == "A")
                        {
                            Image_A_CalcSum.Visible = true;
                            Image_B_CalcSum.Visible = false;
                            Image_C_CalcSum.Visible = false;
                            Image_D_CalcSum.Visible = false;
                            Image_E_CalcSum.Visible = false;
                            Image_Null_CalcSum.Visible = false;
                        }
                        else if (metaOpt.Name == "B")
                        {
                            Image_A_CalcSum.Visible = false;
                            Image_B_CalcSum.Visible = true;
                            Image_C_CalcSum.Visible = false;
                            Image_D_CalcSum.Visible = false;
                            Image_E_CalcSum.Visible = false;
                            Image_Null_CalcSum.Visible = false;
                        }
                        else if (metaOpt.Name == "C")
                        {
                            Image_A_CalcSum.Visible = false;
                            Image_B_CalcSum.Visible = false;
                            Image_C_CalcSum.Visible = true;
                            Image_D_CalcSum.Visible = false;
                            Image_E_CalcSum.Visible = false;
                            Image_Null_CalcSum.Visible = false;
                        }
                        else if (metaOpt.Name == "D")
                        {
                            Image_A_CalcSum.Visible = false;
                            Image_B_CalcSum.Visible = false;
                            Image_C_CalcSum.Visible = false;
                            Image_D_CalcSum.Visible = true;
                            Image_E_CalcSum.Visible = false;
                            Image_Null_CalcSum.Visible = false;
                        }
                        else if (metaOpt.Name == "E")
                        {
                            Image_A_CalcSum.Visible = false;
                            Image_B_CalcSum.Visible = false;
                            Image_C_CalcSum.Visible = false;
                            Image_D_CalcSum.Visible = false;
                            Image_E_CalcSum.Visible = true;
                            Image_Null_CalcSum.Visible = false;
                        }
                        else
                        {
                            Image_A_CalcSum.Visible = false;
                            Image_B_CalcSum.Visible = false;
                            Image_C_CalcSum.Visible = false;
                            Image_D_CalcSum.Visible = false;
                            Image_E_CalcSum.Visible = false;
                            Image_Null_CalcSum.Visible = true;
                        }
                    }
                    else
                    {
                        Modify_Button.Visible = true;
                        AcceptSend_Button.Visible = true;
                        Label25.Visible = false;
                        Risk_Calc_Text.Visible = false;
                        Description_Calc_Text.Visible = false;
                        Image_Null_CalcSum.Visible = false;
                        Image_A_CalcSum.Visible = false;
                        Image_B_CalcSum.Visible = false;
                        Image_C_CalcSum.Visible = false;
                        Image_D_CalcSum.Visible = false;
                        Image_E_CalcSum.Visible = false;

                        Label27.Visible = false;
                        FlowMinus2_TextBox.Visible = false;
                        FlowMinus1_TextBox.Visible = false;
                        Flow0_TextBox.Visible = false;
                        FlowPlus1_TextBox.Visible = false;
                        FlowPlus2_TextBox.Visible = false;

                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
                //no data available
            }

            Label26.Visible = true;

            Label21.Visible = false;
            Image_Null_Calc.Visible = false;
            Image_A_Calc.Visible = false;
            Image_B_Calc.Visible = false;
            Image_C_Calc.Visible = false;
            Image_D_Calc.Visible = false;
            Image_E_Calc.Visible = false;
            Calc_ScenarioID.Visible = false;
            Risk_Calculated_Text.Visible = false;
            Description_Text_Calc.Visible = false;
            SaveSend_Button.Visible = false;

            Calculate_Button.Visible = false;
        }

        protected void AcceptSend_Button_Click(object sender, EventArgs e)
        {
            Risk_Calc_Text.Text = Risk_Text0.Text;
            Description_Calc_Text.Text = Description_Text0.Text;
            Risk_Calculated_Text.Text = Risk_Calc_Text.Text;
            Description_Text_Calc.Text = Description_Calc_Text.Text;
            Calc_ScenarioID.Text = SelectedSc_TextBox.Text;

            Label_AcceptSend_Alert_Confirm.Visible = true;
            AcceptSend_Button_ConfirmYes.Visible = true;
            AcceptSend_Button_ConfirmCancel.Visible = true;
            AcceptSend_Button.Enabled = false;
            Modify_Button.Enabled = false;
        }
        protected void SaveSend_Button_Click(object sender, EventArgs e)
        {
            Risk_Calc_Text.Text = Risk_Calculated_Text.Text;
            Description_Calc_Text.Text = Description_Text_Calc.Text;

            Label_SaveSend_Alert_Confirm.Visible = true;
            SaveSend_Button_ConfirmYes.Visible = true;
            SaveSend_Button_ConfirmCancel.Visible = true;
            SaveSend_Button.Enabled = false;
            Modify_Button.Enabled = false;
        }
        protected void AcceptSend_Button_Click_Cancel(object sender, EventArgs e)
        {
            Label_AcceptSend_Alert_Confirm.Visible = false;
            AcceptSend_Button_ConfirmYes.Visible = false;
            AcceptSend_Button_ConfirmCancel.Visible = false;
            AcceptSend_Button.Enabled = true;
            Modify_Button.Enabled = true;
        }
        protected void SaveSend_Button_Click_Cancel(object sender, EventArgs e)
        {
            Label_SaveSend_Alert_Confirm.Visible = false;
            SaveSend_Button_ConfirmYes.Visible = false;
            SaveSend_Button_ConfirmCancel.Visible = false;
            SaveSend_Button.Enabled = true;
            Modify_Button.Enabled = true;
        }

        protected void Button_Review_Click(object sender, EventArgs e)
        {
            int simUse = Convert.ToInt32(SelectedSim_TextBox_Header.Text);
            Populate_Calculator_Page(simUse);

            MultiView1.ActiveViewIndex = 2;
        }

        protected void ImageReset()
        {
            Image_A_Header.Visible = false;
            Image_B_Header.Visible = false;
            Image_C_Header.Visible = false;
            Image_D_Header.Visible = false;
            Image_E_Header.Visible = false;
        }
        protected void ImageResetOpt()
        {
            Image_A_Calc_Header.Visible = false;
            Image_B_Calc_Header.Visible = false;
            Image_C_Calc_Header.Visible = false;
            Image_D_Calc_Header.Visible = false;
            Image_E_Calc_Header.Visible = false;
        }

        protected void Add_Data_Button_Click(object sender, EventArgs e)
        {
            Response.Redirect("AddDataDam.aspx");
        }
        
    }
}