﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.IO;

namespace TogoWeb
{
    public partial class Alerts : System.Web.UI.Page
    {
        TraceSource trace;
        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);

            trace.TraceEvent(TraceEventType.Verbose, 1, DateTime.Now.ToString() + ": In Users.aspx");
            trace.Flush();
            if (!IsPostBack)
            {
                Session.Add("idView", 0);

                Load_GridView_Meta();
            }
        }
        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }
        private void Load_GridView_Meta()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var queryAll = from m in context.MetascenariosFrenches
                                   select new
                                   {
                                       ID = m.id,
                                       Name = m.Name,
                                       Risk = m.Risk,
                                       Description = m.Description
                                   };

                    DataTable metaGrid = queryAll.ToDataTable();
                    GridView_Meta.DataSource = metaGrid;
                    GridView_Meta.DataBind();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Button_Alert_Details_Click(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_Meta.Rows[idRow];
            int selectedMeta = Convert.ToInt32(row.Cells[0].Text);

            TextBox_Alert_MetaID.Text = selectedMeta.ToString();
            Label_Alert_MetaRisk.Text = row.Cells[2].Text + " Risque";

            PopulateAlertGridView();

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = (from m in context.MetascenariosFrenches
                                where m.id == selectedMeta
                                select m).FirstOrDefault();

                    TextBox_Message_Header.Text = query.Header;
                    TextBox_Message_Main.Text = "La période de prévision de 26 Feb 2016 au 29 Feb 2016 a été analysée par les opérateurs du barrage et indique les conditions suivantes: " + query.Risk.ToString() + " risque (niveau " + query.Name.ToString() + "). " + query.Description.ToString();
                    TextBox_Message_Footer.Text = query.Footer;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            MultiView1.ActiveViewIndex = 1;
        }

        private void PopulateAlertGridView()
        {
            int selectedMeta = Convert.ToInt32(TextBox_Alert_MetaID.Text);
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = from m in context.MetaXContacts
                                where m.idMetascenarios == selectedMeta
                                select m;

                    DataTable AlertTable = new DataTable();
                    AlertTable.Columns.Add("ActionID", typeof(int));                    
                    AlertTable.Columns.Add("ContactID", typeof(int));
                    AlertTable.Columns.Add("ContactFirstName", typeof(string));
                    AlertTable.Columns.Add("ContactLastName", typeof(string));
                    AlertTable.Columns.Add("ActionType", typeof(string));
                    AlertTable.Columns.Add("ContactEmail", typeof(string));

                    foreach (MetaXContact mXa in query)
                    {
                        int selectedAction = mXa.idAction;
                        int selectedContact = mXa.idContact;

                        var act = (from a in context.Actions
                                  where a.id == selectedAction
                                  select a).FirstOrDefault();

                        var con = (from u in context.ContactLists
                                   where u.id == selectedContact
                                   select u).FirstOrDefault();

                        AlertTable.Rows.Add(act.id, con.id, con.FirstName, con.LastName, act.Type, con.Email);
                    }

                    GridView_Alerts.DataSource = AlertTable;
                    GridView_Alerts.DataBind();

                    for (int i = 0; i < GridView_Alerts.Rows.Count; i++)
                    {
                        GridViewRow row = GridView_Alerts.Rows[i];

                        ((TextBox)row.FindControl("TextBox_Alert_ActionID")).Enabled = true;
                        ((TextBox)row.FindControl("TextBox_Alert_ActionID")).Text = AlertTable.Rows[i]["ActionID"].ToString();
                        ((TextBox)row.FindControl("TextBox_Alert_ActionID")).Enabled = false;
                        ((TextBox)row.FindControl("TextBox_Alert_ContactID")).Enabled = true;
                        ((TextBox)row.FindControl("TextBox_Alert_ContactID")).Text = AlertTable.Rows[i]["ContactID"].ToString();
                        ((TextBox)row.FindControl("TextBox_Alert_ContactID")).Enabled = false;
                    }
                    
                }
                
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void Button_Alert_OK_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 0;
            Reset_AlertAdd(false);
            Label_Error_Action.Visible = false;
            Label_Error_Contact.Visible = false;
            Label_Error_Exists.Visible = false;
            Label_Error_Message_Footer.Visible = false;
            Label_Error_Message_Header.Visible = false;
            TextBox_Message_Footer.Enabled = false;
            TextBox_Message_Header.Enabled = false;
            Button_Message_Edit.Visible = true;
            Button_Message_Save.Visible = false;
            Button_Message_Cancel.Visible = false;
        }

        protected void Button_Alert_Remove_Click(object sender, CommandEventArgs e)
        {
            int selectedMeta = Convert.ToInt32(TextBox_Alert_MetaID.Text);

            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_Alerts.Rows[idRow];

            ((TextBox)row.FindControl("TextBox_Alert_ContactID")).Enabled = true;
            ((TextBox)row.FindControl("TextBox_Alert_ActionID")).Enabled = true;
            int selectedContact = Convert.ToInt32(((TextBox)row.FindControl("TextBox_Alert_ContactID")).Text);
            int selectedAction = Convert.ToInt32(((TextBox)row.FindControl("TextBox_Alert_ActionID")).Text);
            ((TextBox)row.FindControl("TextBox_Alert_ContactID")).Enabled = false;
            ((TextBox)row.FindControl("TextBox_Alert_ActionID")).Enabled = false;

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = (from m in context.MetaXContacts
                                where m.idMetascenarios == selectedMeta & m.idAction == selectedAction & m.idContact == selectedContact
                                select m).FirstOrDefault();

                    context.MetaXContacts.Remove(query);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
            PopulateAlertGridView();
        }

        protected void Button_Alert_Add_Click(object sender, EventArgs e)
        {
            Populate_Drop_AlertAdd_Contact();
            Populate_Drop_AlertAdd_Action();

            Reset_AlertAdd(true);

            Button_Alert_Add.Enabled = true;
            Button_Alert_AddNew.Visible = true;
        }
        protected void Reset_AlertAdd(bool set)
        {
            bool setting = set;
            Button_AlertAdd_Save.Visible = setting;
            Button_AlertAdd_Cancel.Visible = setting;
            DropDownList_AlertAdd_Contact.Visible = setting;
            DropDownList_AlertAdd_Action.Visible = setting;
            Label6.Visible = setting;
            Label7.Visible = setting;
            Button_Alert_AddNew.Visible = setting;
        }
        protected void Populate_Drop_AlertAdd_Contact()
        {
            int selectedMeta = Convert.ToInt32(TextBox_Alert_MetaID.Text);
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = from c in context.ContactLists
                                select c;

                    DataTable contact_Table = new DataTable();
                    contact_Table.Columns.Add("Id", typeof(string));
                    contact_Table.Columns.Add("Name", typeof(string));

                    foreach (ContactList con in query)
                    {
                        string NameString = con.LastName + ", " + con.FirstName;
                        contact_Table.Rows.Add(con.id, NameString);
                    }

                    DropDownList_AlertAdd_Contact.DataSource = contact_Table;
                    DropDownList_AlertAdd_Contact.DataTextField = "Name";
                    DropDownList_AlertAdd_Contact.DataValueField = "Id";
                    DropDownList_AlertAdd_Contact.DataBind();

                    DropDownList_AlertAdd_Contact.Items.Insert(0, ".::Sélectionner contact::.");
                    DropDownList_AlertAdd_Contact.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void Populate_Drop_AlertAdd_Action()
        {
            int selectedMeta = Convert.ToInt32(TextBox_Alert_MetaID.Text);
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = from a in context.Actions
                                select new
                                {
                                    Id = a.id,
                                    Type = a.Type
                                };
                    DataTable actionType_Table = new DataTable();
                    actionType_Table = query.ToDataTable();

                    DropDownList_AlertAdd_Action.DataSource = actionType_Table;
                    DropDownList_AlertAdd_Action.DataTextField = "Type";
                    DropDownList_AlertAdd_Action.DataValueField = "Id";
                    DropDownList_AlertAdd_Action.DataBind();

                    DropDownList_AlertAdd_Action.Items.Insert(0, ".::Sélectionner type d’action::.");
                    DropDownList_AlertAdd_Action.SelectedIndex = 0;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void Button_AlertAdd_Save_Click(object sender, EventArgs e)
        {
            int ck = 0;
            Label_Error_Action.Visible = false;
            Label_Error_Contact.Visible = false;
            Label_Error_Exists.Visible = false;

            if (DropDownList_AlertAdd_Contact.SelectedIndex == 0)
            {
                Label_Error_Contact.Visible = true;
                ck = 1;
            }
            if (DropDownList_AlertAdd_Action.SelectedIndex == 0)
            {
                Label_Error_Action.Visible = true;
                ck = 1;
            }
            if (ck == 1) return;
            
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int selectedMeta = Convert.ToInt32(TextBox_Alert_MetaID.Text);
                    int selectedContact = Convert.ToInt32(DropDownList_AlertAdd_Contact.SelectedValue);
                    int selectedAction = Convert.ToInt32(DropDownList_AlertAdd_Action.SelectedValue);

                    var mXc = from mm in context.MetaXContacts
                          orderby mm.id descending
                          select mm;

                    var mXc_check = from mEx in context.MetaXContacts
                                    where mEx.idMetascenarios == selectedMeta && mEx.idAction == selectedAction && mEx.idContact == selectedContact
                                    select mEx;

                    if (mXc_check.Count() > 0)
                    {
                        Label_Error_Exists.Visible = true;
                        return;
                    }

                    MetaXContact mXc_Add = new MetaXContact();

                    if (mXc.Count() > 0)
                    {
                        mXc_Add.id = mXc.First().id + 1;
                    }
                    else
                    {
                        mXc_Add.id = 1;
                    }
                    mXc_Add.idMetascenarios = selectedMeta;
                    mXc_Add.idAction = selectedAction;
                    mXc_Add.idContact = selectedContact;

                    context.MetaXContacts.Add(mXc_Add);
                    context.SaveChanges();
                }
                PopulateAlertGridView();
                Reset_AlertAdd(false);
                MultiView1.ActiveViewIndex = 1;
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void Button_AlertAdd_Cancel_Click(object sender, EventArgs e)
        {
            Reset_AlertAdd(false);
            Label_Error_Action.Visible = false;
            Label_Error_Contact.Visible = false;
            Label_Error_Exists.Visible = false;
        }

        protected void DropDownList_AlertAdd_Contact_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList_AlertAdd_Contact.SelectedIndex != 0)
            {
                Label_Error_Contact.Visible = false;
            }
        }

        protected void DropDownList_AlertAdd_Action_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList_AlertAdd_Action.SelectedIndex != 0)
            {
                Label_Error_Action.Visible = false;
            }
        }

        protected void Button_Message_Edit_Click(object sender, EventArgs e)
        {
            TextBox_Message_Header.Enabled = true;
            TextBox_Message_Footer.Enabled = true;
            Button_Message_Save.Visible = true;
            Button_Message_Cancel.Visible = true;
        }

        protected void Button_Message_Save_Click(object sender, EventArgs e)
        {
            int selectedMeta = Convert.ToInt32(TextBox_Alert_MetaID.Text);

            Label_Error_Message_Footer.Visible = false;
            Label_Error_Message_Header.Visible = false;

            int ck = 0;
            if (TextBox_Message_Header.Text == "")
            {
                Label_Error_Message_Header.Visible = true;
                ck = 1;
            }
            if (TextBox_Message_Footer.Text == "")
            {
                Label_Error_Message_Footer.Visible = true;
                ck = 1;
            }
            if (ck == 1) return;

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = (from m in context.MetascenariosFrenches
                                 where m.id == selectedMeta
                                 select m).FirstOrDefault();

                    query.Header = TextBox_Message_Header.Text;
                    query.Footer = TextBox_Message_Footer.Text;

                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            TextBox_Message_Footer.Enabled = false;
            TextBox_Message_Header.Enabled = false;
            Button_Message_Save.Visible = false;
            Button_Message_Cancel.Visible = false;

            Label_Error_Message_Footer.Visible = false;
            Label_Error_Message_Header.Visible = false;
        }

        protected void Button_Message_Cancel_Click(object sender, EventArgs e)
        {
            int selectedMeta = Convert.ToInt32(TextBox_Alert_MetaID.Text);
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = (from m in context.MetascenariosFrenches
                                 where m.id == selectedMeta
                                 select m).FirstOrDefault();

                    TextBox_Message_Header.Text = query.Header;
                    TextBox_Message_Main.Text = "La période de prévision de Feb 26, 2016 au Feb 29, 2016 a été analysée par les opérateurs du barrage et indique les conditions suivantes: " + query.Risk.ToString() + " risque (niveau " + query.Name.ToString() + "). " + query.Description.ToString();
                    TextBox_Message_Footer.Text = query.Footer;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            TextBox_Message_Footer.Enabled = false;
            TextBox_Message_Header.Enabled = false;
            Button_Message_Save.Visible = false;
            Button_Message_Cancel.Visible = false;

            Label_Error_Message_Footer.Visible = false;
            Label_Error_Message_Header.Visible = false;
        }

        protected void Button_Alert_AddNew_Click(object sender, EventArgs e)
        {
            Server.Transfer("TogoContact.aspx");
        }

    }
}