﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;

namespace TogoWeb
{
    public partial class AddDataDam : System.Web.UI.Page
    {
        static TraceSource trace;

        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);

            if (!IsPostBack)
            {
                Session.Add("idView", 0);
                Calendar_Add.SelectedDate = DateTime.Today;
            }
        }
        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }
        protected void Reset_Boxes()
        {
            Value_TextBox_Var1.Text = "";
            Value_TextBox_Var2.Text = "";
            Value_TextBox_Var3.Text = "";
            Calendar_Add.SelectedDate = DateTime.Today;
            Hour_TextBox.Text = "6";    //default to 6:00 am
            Minute_TextBox.Text = "0";  //default to 6:00 am
        }
        protected void Reset_Labels()
        {
            Value_Check_Label1.Visible = false;
            Value_Check_Label2.Visible = false;
            Value_Check_Label3.Visible = false;
            Date_Check_Label.Visible = false;
            Time_Check_Label.Visible = false;
            Label_Units1.Visible = false;
            Label_Units2.Visible = false;
            Label_Units3.Visible = false;
            Time_Check_Label_Warning.Visible = false;
            Save_Button_Yes.Visible = false;
            Save_Button_No.Visible = false;
        }

        protected void Button_Save_Click(object sender, EventArgs e)
        {
            //check selection/entries and make sure can convert
            Reset_Labels();

            int ck = 0;
            int ck_all_variables = 0;
            if (Value_TextBox_Var1.Text == "" && Value_TextBox_Var2.Text == "" && Value_TextBox_Var3.Text == "") ck = 1;
            if (Value_TextBox_Var1.Text == "")
            {
                Value_Check_Label1.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var1.Text);
                }
                catch
                {
                    Value_Check_Label1.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var2.Text == "")
            {
                Value_Check_Label2.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var2.Text);
                }
                catch
                {
                    Value_Check_Label2.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var3.Text == "")
            {
                Value_Check_Label3.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var3.Text);
                }
                catch
                {
                    Value_Check_Label3.Visible = true;
                    ck = 1;
                }
            }
            if (Hour_TextBox.Text == "")
            {
                Time_Check_Label.Visible = true;
                ck = 1;
            }
            else
            {
                try
                {
                    double hourCheck = Convert.ToDouble(Hour_TextBox.Text);
                }
                catch
                {
                    Time_Check_Label.Visible = true;
                    ck = 1;
                }
            }
            if (Minute_TextBox.Text == "")
            {
                Time_Check_Label.Visible = true;
                ck = 1;
            }
            else
            {
                try
                {
                    double minCheck = Convert.ToDouble(Minute_TextBox.Text);
                }
                catch
                {
                    Time_Check_Label.Visible = true;
                    ck = 1;
                }
            }

            if (ck == 1)
            {
                return; // if missing info
            }
            else
            {
                if (ck_all_variables == 1)
                {
                    Save_Button_Yes.Visible = true;
                    Save_Button_No.Visible = true;
                    Time_Check_Label_Warning.Visible = true;
                    return;     // if missing one or more variables
                }
                else
                {
                    Save_Values();
                }
            }
        }

        protected void Button_No_Click(object sender, EventArgs e)
        {
            Save_Button_Yes.Visible = false;
            Save_Button_No.Visible = false;
            Time_Check_Label_Warning.Visible = false;
        }

        protected void Button_Yes_Click(object sender, EventArgs e)
        {
            Save_Values();
        }

        protected void Save_Values()
        {
            if (Value_TextBox_Var1.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        var dat = from d in context.DataValues
                                  orderby d.id descending
                                  select d;

                        DataValue datAdd = new DataValue();

                        if (dat.Count() > 0)
                        {
                            datAdd.id = dat.First().id + 1;
                        }
                        else
                        {
                            datAdd.id = 1;
                        }

                        datAdd.Value = Convert.ToDouble(Value_TextBox_Var1.Text);
                        DateTime dtUse = Calendar_Add.SelectedDate;
                        dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                        dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                        datAdd.DateTime = dtUse;

                        var StaQuery = (from s in context.Stations
                                        where s.Name == "Nangbeto Dam"
                                        select s).FirstOrDefault();

                        var VarQuery1 = (from v in context.Variables
                                         where v.Name == "Inflow"
                                         select v).FirstOrDefault();

                        int selectedSta = Convert.ToInt32(StaQuery.id);
                        int selectedVar1 = Convert.ToInt32(VarQuery1.id);

                        var vXs1 = (from x in context.VariableXStations
                                    where x.idVariable == selectedVar1 && x.idStation == selectedSta
                                    select x).FirstOrDefault();

                        datAdd.idVariableXStation = vXs1.id;

                        datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                        context.DataValues.Add(datAdd);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            if (Value_TextBox_Var2.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        var dat = from d in context.DataValues
                                  orderby d.id descending
                                  select d;

                        DataValue datAdd = new DataValue();

                        if (dat.Count() > 0)
                        {
                            datAdd.id = dat.First().id + 1;
                        }
                        else
                        {
                            datAdd.id = 1;
                        }

                        datAdd.Value = Convert.ToDouble(Value_TextBox_Var2.Text);
                        DateTime dtUse = Calendar_Add.SelectedDate;
                        dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                        dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                        datAdd.DateTime = dtUse;

                        var StaQuery = (from s in context.Stations
                                        where s.Name == "Nangbeto Dam"
                                        select s).FirstOrDefault();

                        var VarQuery2 = (from v in context.Variables
                                         where v.Name == "Outflow"
                                         select v).FirstOrDefault();

                        int selectedSta = Convert.ToInt32(StaQuery.id);
                        int selectedVar2 = Convert.ToInt32(VarQuery2.id);

                        var vXs2 = (from x in context.VariableXStations
                                    where x.idVariable == selectedVar2 && x.idStation == selectedSta
                                    select x).FirstOrDefault();

                        datAdd.idVariableXStation = vXs2.id;

                        datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                        context.DataValues.Add(datAdd);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            if (Value_TextBox_Var3.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        var dat = from d in context.DataValues
                                  orderby d.id descending
                                  select d;

                        DataValue datAdd = new DataValue();

                        if (dat.Count() > 0)
                        {
                            datAdd.id = dat.First().id + 1;
                        }
                        else
                        {
                            datAdd.id = 1;
                        }

                        datAdd.Value = Convert.ToDouble(Value_TextBox_Var3.Text);
                        DateTime dtUse = Calendar_Add.SelectedDate;
                        dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                        dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                        datAdd.DateTime = dtUse;

                        var StaQuery = (from s in context.Stations
                                        where s.Name == "Nangbeto Dam"
                                        select s).FirstOrDefault();

                        var VarQuery3 = (from v in context.Variables
                                         where v.Name == "Water Level"
                                         select v).FirstOrDefault();

                        int selectedSta = Convert.ToInt32(StaQuery.id);
                        int selectedVar3 = Convert.ToInt32(VarQuery3.id);

                        var vXs3 = (from x in context.VariableXStations
                                    where x.idVariable == selectedVar3 && x.idStation == selectedSta
                                    select x).FirstOrDefault();

                        datAdd.idVariableXStation = vXs3.id;

                        datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                        context.DataValues.Add(datAdd);
                        context.SaveChanges();
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            Reset_Boxes();
            Reset_Labels();
        }

        protected void Button_Reset_Click(object sender, EventArgs e)
        {
            Reset_Boxes();
            Reset_Labels();
        }


    }
}