﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;

namespace TogoWeb
{
    public partial class AddDataRain : System.Web.UI.Page
    {
        static TraceSource trace;

        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);

            if (!IsPostBack)
            {
                Session.Add("idView", 0);
                //Calendar_Add.SelectedDate = DateTime.Today;
                //TODO when click on date, query database and populate values from database if exist
            }
        }
        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }
        protected void Reset_Boxes()
        {
            Value_TextBox_Var1.Text = "";
            Value_TextBox_Var2.Text = "";
            Value_TextBox_Var3.Text = "";
            Value_TextBox_Var4.Text = "";
            Value_TextBox_Var5.Text = "";
            Value_TextBox_Var6.Text = "";
            Value_TextBox_Var7.Text = "";
            Value_TextBox_Var8.Text = "";
            //Calendar_Add.SelectedDate = DateTime.Today;
            Hour_TextBox.Text = "6";    //default to 6:00 am
            Minute_TextBox.Text = "0";  //default to 6:00 am

            CheckBox_Var1.Checked = false;
            CheckBox_Var2.Checked = false;
            CheckBox_Var3.Checked = false;
            CheckBox_Var4.Checked = false;
            CheckBox_Var5.Checked = false;
            CheckBox_Var6.Checked = false;
            CheckBox_Var7.Checked = false;
            CheckBox_Var8.Checked = false;
            TextBox_ID_Existing_Var1.Text = "";
            TextBox_ID_Existing_Var2.Text = "";
            TextBox_ID_Existing_Var3.Text = "";
            TextBox_ID_Existing_Var4.Text = "";
            TextBox_ID_Existing_Var5.Text = "";
            TextBox_ID_Existing_Var6.Text = "";
            TextBox_ID_Existing_Var7.Text = "";
            TextBox_ID_Existing_Var8.Text = "";
        }

        protected void Reset_Labels()
        {
            Value_Check_Label1.Visible = false;
            Value_Check_Label2.Visible = false;
            Value_Check_Label3.Visible = false;
            Value_Check_Label4.Visible = false;
            Value_Check_Label5.Visible = false;
            Value_Check_Label6.Visible = false;
            Value_Check_Label7.Visible = false;
            Value_Check_Label8.Visible = false;
            Date_Check_Label.Visible = true;
            Time_Check_Label.Visible = false;
            Time_Check_Label_Warning.Visible = false;
            Save_Button_Yes.Visible = false;
            Save_Button_No.Visible = false;
        }

        protected void Button_Save_Click(object sender, EventArgs e)
        {
            //check selection/entries and make sure can convert
            Reset_Labels();
            Date_Check_Label.Visible = false;

            int ck = 0;
            int ck_all_variables = 0;
            if (Value_TextBox_Var1.Text == "" && Value_TextBox_Var2.Text == "" && Value_TextBox_Var3.Text == "" && Value_TextBox_Var4.Text == "" && Value_TextBox_Var5.Text == "" && Value_TextBox_Var6.Text == "" && Value_TextBox_Var7.Text == "" && Value_TextBox_Var8.Text == "") ck = 1;

            if (Value_TextBox_Var1.Text == "")
            {
                //Value_Check_Label1.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var1.Text);
                }
                catch
                {
                    Value_Check_Label1.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var2.Text == "")
            {
                //Value_Check_Label2.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var2.Text);
                }
                catch
                {
                    Value_Check_Label2.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var3.Text == "")
            {
                //Value_Check_Label3.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var3.Text);
                }
                catch
                {
                    Value_Check_Label3.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var4.Text == "")
            {
                //Value_Check_Label4.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var4.Text);
                }
                catch
                {
                    Value_Check_Label4.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var5.Text == "")
            {
                //Value_Check_Label5.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var5.Text);
                }
                catch
                {
                    Value_Check_Label5.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var6.Text == "")
            {
                //Value_Check_Label6.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var6.Text);
                }
                catch
                {
                    Value_Check_Label6.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var7.Text == "")
            {
                //Value_Check_Label7.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var7.Text);
                }
                catch
                {
                    Value_Check_Label7.Visible = true;
                    ck = 1;
                }
            }
            if (Value_TextBox_Var8.Text == "")
            {
                //Value_Check_Label8.Visible = true;
                ck_all_variables = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox_Var8.Text);
                }
                catch
                {
                    Value_Check_Label8.Visible = true;
                    ck = 1;
                }
            }

            if (Hour_TextBox.Text == "")
            {
                Time_Check_Label.Visible = true;
                ck = 1;
            }
            else
            {
                try
                {
                    double hourCheck = Convert.ToDouble(Hour_TextBox.Text);
                }
                catch
                {
                    Time_Check_Label.Visible = true;
                    ck = 1;
                }
            }
            if (Minute_TextBox.Text == "")
            {
                Time_Check_Label.Visible = true;
                ck = 1;
            }
            else
            {
                try
                {
                    double minCheck = Convert.ToDouble(Minute_TextBox.Text);
                }
                catch
                {
                    Time_Check_Label.Visible = true;
                    ck = 1;
                }
            }

            if (ck == 1)
            {
                return; // if missing info
            }
            else
            {
                if (ck_all_variables == 1)
                {
                    Save_Button_Yes.Visible = true;
                    Save_Button_No.Visible = true;
                    Time_Check_Label_Warning.Visible = true;
                    return;     // if missing one or more variables
                }
                else
                {
                    Save_Values();
                }
            }
        }

        protected void Button_No_Click(object sender, EventArgs e)
        {
            Save_Button_Yes.Visible = false;
            Save_Button_No.Visible = false;
            Time_Check_Label_Warning.Visible = false;
        }

        protected void Button_Yes_Click(object sender, EventArgs e)
        {
            Save_Values();
        }

        protected void Save_Values()
        {
            if (Value_TextBox_Var1.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        if (CheckBox_Var1.Checked == true)
                        {
                            var dat = from d in context.DataValues
                                      orderby d.id descending
                                      select d;

                            DataValue datAdd = new DataValue();

                            if (dat.Count() > 0)
                            {
                                datAdd.id = dat.First().id + 1;
                            }
                            else
                            {
                                datAdd.id = 1;
                            }

                            datAdd.Value = Convert.ToDouble(Value_TextBox_Var1.Text);
                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datAdd.DateTime = dtUse;

                            var StaQuery = (from s in context.Stations
                                            where s.Name == "Anie"
                                            select s).FirstOrDefault();

                            var VarQuery = (from v in context.Variables
                                            where v.Name == "Precipitation"
                                            select v).FirstOrDefault();

                            int selectedSta = Convert.ToInt32(StaQuery.id);
                            int selectedVar = Convert.ToInt32(VarQuery.id);

                            var vXs1 = (from x in context.VariableXStations
                                        where x.idVariable == selectedVar && x.idStation == selectedSta
                                        select x).FirstOrDefault();

                            datAdd.idVariableXStation = vXs1.id;

                            datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                            context.DataValues.Add(datAdd);
                            context.SaveChanges();
                        }
                        else
                        {
                            int idUse = Convert.ToInt32(TextBox_ID_Existing_Var1.Text);
                            var datEx = (from d in context.DataValues
                                        where d.id == idUse
                                        select d).FirstOrDefault();

                            datEx.Value = Convert.ToDouble(Value_TextBox_Var1.Text);

                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datEx.DateTime = dtUse;

                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }

            if (Value_TextBox_Var2.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        if (CheckBox_Var2.Checked == true)
                        {
                            var dat = from d in context.DataValues
                                      orderby d.id descending
                                      select d;

                            DataValue datAdd = new DataValue();

                            if (dat.Count() > 0)
                            {
                                datAdd.id = dat.First().id + 1;
                            }
                            else
                            {
                                datAdd.id = 1;
                            }

                            datAdd.Value = Convert.ToDouble(Value_TextBox_Var2.Text);
                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datAdd.DateTime = dtUse;

                            var StaQuery = (from s in context.Stations
                                            where s.Name == "Sokode"
                                            select s).FirstOrDefault();

                            var VarQuery = (from v in context.Variables
                                            where v.Name == "Precipitation"
                                            select v).FirstOrDefault();

                            int selectedSta = Convert.ToInt32(StaQuery.id);
                            int selectedVar = Convert.ToInt32(VarQuery.id);

                            var vXs2 = (from x in context.VariableXStations
                                        where x.idVariable == selectedVar && x.idStation == selectedSta
                                        select x).FirstOrDefault();

                            datAdd.idVariableXStation = vXs2.id;

                            datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                            context.DataValues.Add(datAdd);
                            context.SaveChanges();
                        }
                        else
                        {
                            int idUse = Convert.ToInt32(TextBox_ID_Existing_Var2.Text);
                            var datEx = (from d in context.DataValues
                                         where d.id == idUse
                                         select d).FirstOrDefault();

                            datEx.Value = Convert.ToDouble(Value_TextBox_Var2.Text);

                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datEx.DateTime = dtUse;

                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            if (Value_TextBox_Var3.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        if (CheckBox_Var3.Checked == true)
                        {
                            var dat = from d in context.DataValues
                                      orderby d.id descending
                                      select d;

                            DataValue datAdd = new DataValue();

                            if (dat.Count() > 0)
                            {
                                datAdd.id = dat.First().id + 1;
                            }
                            else
                            {
                                datAdd.id = 1;
                            }

                            datAdd.Value = Convert.ToDouble(Value_TextBox_Var3.Text);
                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datAdd.DateTime = dtUse;

                            var StaQuery = (from s in context.Stations
                                            where s.Name == "Sotoboua"
                                            select s).FirstOrDefault();

                            var VarQuery = (from v in context.Variables
                                            where v.Name == "Precipitation"
                                            select v).FirstOrDefault();

                            int selectedSta = Convert.ToInt32(StaQuery.id);
                            int selectedVar = Convert.ToInt32(VarQuery.id);

                            var vXs3 = (from x in context.VariableXStations
                                        where x.idVariable == selectedVar && x.idStation == selectedSta
                                        select x).FirstOrDefault();

                            datAdd.idVariableXStation = vXs3.id;

                            datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                            context.DataValues.Add(datAdd);
                            context.SaveChanges();
                        }
                        else
                        {
                            int idUse = Convert.ToInt32(TextBox_ID_Existing_Var3.Text);
                            var datEx = (from d in context.DataValues
                                         where d.id == idUse
                                         select d).FirstOrDefault();

                            datEx.Value = Convert.ToDouble(Value_TextBox_Var3.Text);

                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datEx.DateTime = dtUse;

                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            if (Value_TextBox_Var4.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        if (CheckBox_Var4.Checked == true)
                        {
                            var dat = from d in context.DataValues
                                      orderby d.id descending
                                      select d;

                            DataValue datAdd = new DataValue();

                            if (dat.Count() > 0)
                            {
                                datAdd.id = dat.First().id + 1;
                            }
                            else
                            {
                                datAdd.id = 1;
                            }

                            datAdd.Value = Convert.ToDouble(Value_TextBox_Var4.Text);
                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datAdd.DateTime = dtUse;

                            var StaQuery = (from s in context.Stations
                                            where s.Name == "Blitta"
                                            select s).FirstOrDefault();

                            var VarQuery = (from v in context.Variables
                                            where v.Name == "Precipitation"
                                            select v).FirstOrDefault();

                            int selectedSta = Convert.ToInt32(StaQuery.id);
                            int selectedVar = Convert.ToInt32(VarQuery.id);

                            var vXs4 = (from x in context.VariableXStations
                                        where x.idVariable == selectedVar && x.idStation == selectedSta
                                        select x).FirstOrDefault();

                            datAdd.idVariableXStation = vXs4.id;

                            datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                            context.DataValues.Add(datAdd);
                            context.SaveChanges();
                        }
                        else
                        {
                            int idUse = Convert.ToInt32(TextBox_ID_Existing_Var4.Text);
                            var datEx = (from d in context.DataValues
                                         where d.id == idUse
                                         select d).FirstOrDefault();

                            datEx.Value = Convert.ToDouble(Value_TextBox_Var4.Text);

                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datEx.DateTime = dtUse;

                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            if (Value_TextBox_Var5.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        if (CheckBox_Var5.Checked == true)
                        {
                            var dat = from d in context.DataValues
                                      orderby d.id descending
                                      select d;

                            DataValue datAdd = new DataValue();

                            if (dat.Count() > 0)
                            {
                                datAdd.id = dat.First().id + 1;
                            }
                            else
                            {
                                datAdd.id = 1;
                            }

                            datAdd.Value = Convert.ToDouble(Value_TextBox_Var5.Text);
                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datAdd.DateTime = dtUse;

                            var StaQuery = (from s in context.Stations
                                            where s.Name == "Tchamba"
                                            select s).FirstOrDefault();

                            var VarQuery = (from v in context.Variables
                                            where v.Name == "Precipitation"
                                            select v).FirstOrDefault();

                            int selectedSta = Convert.ToInt32(StaQuery.id);
                            int selectedVar = Convert.ToInt32(VarQuery.id);

                            var vXs5 = (from x in context.VariableXStations
                                        where x.idVariable == selectedVar && x.idStation == selectedSta
                                        select x).FirstOrDefault();

                            datAdd.idVariableXStation = vXs5.id;

                            datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                            context.DataValues.Add(datAdd);
                            context.SaveChanges();
                        }
                        else
                        {
                            int idUse = Convert.ToInt32(TextBox_ID_Existing_Var5.Text);
                            var datEx = (from d in context.DataValues
                                         where d.id == idUse
                                         select d).FirstOrDefault();

                            datEx.Value = Convert.ToDouble(Value_TextBox_Var5.Text);

                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datEx.DateTime = dtUse;

                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            if (Value_TextBox_Var6.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        if (CheckBox_Var6.Checked == true)
                        {
                            var dat = from d in context.DataValues
                                      orderby d.id descending
                                      select d;

                            DataValue datAdd = new DataValue();

                            if (dat.Count() > 0)
                            {
                                datAdd.id = dat.First().id + 1;
                            }
                            else
                            {
                                datAdd.id = 1;
                            }

                            datAdd.Value = Convert.ToDouble(Value_TextBox_Var6.Text);
                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datAdd.DateTime = dtUse;

                            var StaQuery = (from s in context.Stations
                                            where s.Name == "Aledjo"
                                            select s).FirstOrDefault();

                            var VarQuery = (from v in context.Variables
                                            where v.Name == "Precipitation"
                                            select v).FirstOrDefault();

                            int selectedSta = Convert.ToInt32(StaQuery.id);
                            int selectedVar = Convert.ToInt32(VarQuery.id);

                            var vXs6 = (from x in context.VariableXStations
                                        where x.idVariable == selectedVar && x.idStation == selectedSta
                                        select x).FirstOrDefault();

                            datAdd.idVariableXStation = vXs6.id;

                            datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                            context.DataValues.Add(datAdd);
                            context.SaveChanges();
                        }
                        else
                        {
                            int idUse = Convert.ToInt32(TextBox_ID_Existing_Var6.Text);
                            var datEx = (from d in context.DataValues
                                         where d.id == idUse
                                         select d).FirstOrDefault();

                            datEx.Value = Convert.ToDouble(Value_TextBox_Var6.Text);

                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datEx.DateTime = dtUse;

                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            if (Value_TextBox_Var7.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        if (CheckBox_Var7.Checked == true)
                        {
                            var dat = from d in context.DataValues
                                      orderby d.id descending
                                      select d;

                            DataValue datAdd = new DataValue();

                            if (dat.Count() > 0)
                            {
                                datAdd.id = dat.First().id + 1;
                            }
                            else
                            {
                                datAdd.id = 1;
                            }

                            datAdd.Value = Convert.ToDouble(Value_TextBox_Var7.Text);
                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datAdd.DateTime = dtUse;

                            var StaQuery = (from s in context.Stations
                                            where s.Name == "Goubi"
                                            select s).FirstOrDefault();

                            var VarQuery = (from v in context.Variables
                                            where v.Name == "Precipitation"
                                            select v).FirstOrDefault();

                            int selectedSta = Convert.ToInt32(StaQuery.id);
                            int selectedVar = Convert.ToInt32(VarQuery.id);

                            var vXs7 = (from x in context.VariableXStations
                                        where x.idVariable == selectedVar && x.idStation == selectedSta
                                        select x).FirstOrDefault();

                            datAdd.idVariableXStation = vXs7.id;

                            datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                            context.DataValues.Add(datAdd);
                            context.SaveChanges();
                        }
                        else
                        {
                            int idUse = Convert.ToInt32(TextBox_ID_Existing_Var7.Text);
                            var datEx = (from d in context.DataValues
                                         where d.id == idUse
                                         select d).FirstOrDefault();

                            datEx.Value = Convert.ToDouble(Value_TextBox_Var7.Text);

                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datEx.DateTime = dtUse;

                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }
            if (Value_TextBox_Var8.Text != "")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        if (CheckBox_Var8.Checked == true)
                        {
                            var dat = from d in context.DataValues
                                      orderby d.id descending
                                      select d;

                            DataValue datAdd = new DataValue();

                            if (dat.Count() > 0)
                            {
                                datAdd.id = dat.First().id + 1;
                            }
                            else
                            {
                                datAdd.id = 1;
                            }

                            datAdd.Value = Convert.ToDouble(Value_TextBox_Var8.Text);
                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datAdd.DateTime = dtUse;

                            var StaQuery = (from s in context.Stations
                                            where s.Name == "Nangbeto"
                                            select s).FirstOrDefault();

                            var VarQuery = (from v in context.Variables
                                            where v.Name == "Precipitation"
                                            select v).FirstOrDefault();

                            int selectedSta = Convert.ToInt32(StaQuery.id);
                            int selectedVar = Convert.ToInt32(VarQuery.id);

                            var vXs8 = (from x in context.VariableXStations
                                        where x.idVariable == selectedVar && x.idStation == selectedSta
                                        select x).FirstOrDefault();

                            datAdd.idVariableXStation = vXs8.id;

                            datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                            context.DataValues.Add(datAdd);
                            context.SaveChanges();
                        }
                        else
                        {
                            int idUse = Convert.ToInt32(TextBox_ID_Existing_Var8.Text);
                            var datEx = (from d in context.DataValues
                                         where d.id == idUse
                                         select d).FirstOrDefault();

                            datEx.Value = Convert.ToDouble(Value_TextBox_Var8.Text);

                            DateTime dtUse = Calendar_Add.SelectedDate;
                            dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                            dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                            datEx.DateTime = dtUse;

                            context.SaveChanges();
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
            }

            Reset_Boxes();
            Reset_Labels();
            Date_Check_Label.Visible = true;
        }
        protected void Button_Reset_Click(object sender, EventArgs e)
        {
            Reset_Boxes();
            Reset_Labels();
        }

        protected void Calendar_Add_SelectionChanged(object sender, EventArgs e)
        {
            Reset_Boxes();
            Reset_Labels();
            Date_Check_Label.Visible = false;
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    DateTime lb = Calendar_Add.SelectedDate;
                    DateTime ub = lb.AddDays(1);

                    //station 1 anie
                    var dat1 = from d in context.DataValues
                              where d.DateTime > lb && d.DateTime < ub && d.idVariableXStation == 1 
                              orderby d.DateTime descending
                              select d;

                    if (dat1.Count() > 0)
                    {
                        DataValue dataValue_use1 = dat1.First();
                        CheckBox_Var1.Checked = false;  //EXISTING
                        TextBox_ID_Existing_Var1.Text = Convert.ToString(dataValue_use1.id);
                        Value_TextBox_Var1.Text = Convert.ToString(dataValue_use1.Value);
                    }
                    else 
                    {
                        CheckBox_Var1.Checked = true;   //NEW
                        TextBox_ID_Existing_Var1.Text = "";
                        Value_TextBox_Var1.Text = "";
                    }
                    //station 2 Sokode
                    var dat2 = from d in context.DataValues
                              where d.DateTime > lb && d.DateTime < ub && d.idVariableXStation == 2
                              orderby d.DateTime descending
                              select d;

                    if (dat2.Count() > 0)
                    {
                        DataValue dataValue_use2 = dat2.First();
                        CheckBox_Var2.Checked = false;  //EXISTING
                        TextBox_ID_Existing_Var2.Text = Convert.ToString(dataValue_use2.id);
                        Value_TextBox_Var2.Text = Convert.ToString(dataValue_use2.Value);
                    }
                    else
                    {
                        CheckBox_Var2.Checked = true;   //NEW
                        TextBox_ID_Existing_Var2.Text = "";
                        Value_TextBox_Var2.Text = "";
                    }
                    //station 3 sotoboua
                    var dat3 = from d in context.DataValues
                               where d.DateTime > lb && d.DateTime < ub && d.idVariableXStation == 3
                               orderby d.DateTime descending
                               select d;

                    if (dat3.Count() > 0)
                    {
                        DataValue dataValue_use3 = dat3.First();
                        CheckBox_Var3.Checked = false;  //EXISTING
                        TextBox_ID_Existing_Var3.Text = Convert.ToString(dataValue_use3.id);
                        Value_TextBox_Var3.Text = Convert.ToString(dataValue_use3.Value);
                    }
                    else
                    {
                        CheckBox_Var3.Checked = true;   //NEW
                        TextBox_ID_Existing_Var3.Text = "";
                        Value_TextBox_Var3.Text = "";
                    }
                    //station 4 Blitta
                    var dat4 = from d in context.DataValues
                               where d.DateTime > lb && d.DateTime < ub && d.idVariableXStation == 4
                               orderby d.DateTime descending
                               select d;

                    if (dat4.Count() > 0)
                    {
                        DataValue dataValue_use4 = dat4.First();
                        CheckBox_Var4.Checked = false;  //EXISTING
                        TextBox_ID_Existing_Var4.Text = Convert.ToString(dataValue_use4.id);
                        Value_TextBox_Var4.Text = Convert.ToString(dataValue_use4.Value);
                    }
                    else
                    {
                        CheckBox_Var4.Checked = true;   //NEW
                        TextBox_ID_Existing_Var4.Text = "";
                        Value_TextBox_Var4.Text = "";
                    }
                    //station 5 Tchamba
                    var dat5 = from d in context.DataValues
                               where d.DateTime > lb && d.DateTime < ub && d.idVariableXStation == 5
                               orderby d.DateTime descending
                               select d;

                    if (dat5.Count() > 0)
                    {
                        DataValue dataValue_use5 = dat5.First();
                        CheckBox_Var5.Checked = false;  //EXISTING
                        TextBox_ID_Existing_Var5.Text = Convert.ToString(dataValue_use5.id);
                        Value_TextBox_Var5.Text = Convert.ToString(dataValue_use5.Value);
                    }
                    else
                    {
                        CheckBox_Var5.Checked = true;   //NEW
                        TextBox_ID_Existing_Var5.Text = "";
                        Value_TextBox_Var5.Text = "";
                    }
                    //station 6 Aledjo
                    var dat6 = from d in context.DataValues
                               where d.DateTime > lb && d.DateTime < ub && d.idVariableXStation == 6
                               orderby d.DateTime descending
                               select d;

                    if (dat6.Count() > 0)
                    {
                        DataValue dataValue_use6 = dat6.First();
                        CheckBox_Var6.Checked = false;  //EXISTING
                        TextBox_ID_Existing_Var6.Text = Convert.ToString(dataValue_use6.id);
                        Value_TextBox_Var6.Text = Convert.ToString(dataValue_use6.Value);
                    }
                    else
                    {
                        CheckBox_Var6.Checked = true;   //NEW
                        TextBox_ID_Existing_Var6.Text = "";
                        Value_TextBox_Var6.Text = "";
                    }
                    //station 7 Goubi
                    var dat7 = from d in context.DataValues
                               where d.DateTime > lb && d.DateTime < ub && d.idVariableXStation == 7
                               orderby d.DateTime descending
                               select d;

                    if (dat7.Count() > 0)
                    {
                        DataValue dataValue_use7 = dat7.First();
                        CheckBox_Var7.Checked = false;  //EXISTING
                        TextBox_ID_Existing_Var7.Text = Convert.ToString(dataValue_use7.id);
                        Value_TextBox_Var7.Text = Convert.ToString(dataValue_use7.Value);
                    }
                    else
                    {
                        CheckBox_Var7.Checked = true;   //NEW
                        TextBox_ID_Existing_Var7.Text = "";
                        Value_TextBox_Var7.Text = "";
                    }
                    //station 8 Goubi
                    var dat8 = from d in context.DataValues
                               where d.DateTime > lb && d.DateTime < ub && d.idVariableXStation == 8
                               orderby d.DateTime descending
                               select d;

                    if (dat8.Count() > 0)
                    {
                        DataValue dataValue_use8 = dat8.First();
                        CheckBox_Var8.Checked = false;  //EXISTING
                        TextBox_ID_Existing_Var8.Text = Convert.ToString(dataValue_use8.id);
                        Value_TextBox_Var8.Text = Convert.ToString(dataValue_use8.Value);
                    }
                    else
                    {
                        CheckBox_Var8.Checked = true;   //NEW
                        TextBox_ID_Existing_Var8.Text = "";
                        Value_TextBox_Var8.Text = "";
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
    }
}