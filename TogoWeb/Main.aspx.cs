﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;


namespace TogoWeb
{
    public partial class Main : System.Web.UI.Page
    {
        TraceSource trace;
        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            if (Session["idUser"] == null)
            {
                //IF NOT LOGGED IN
                //remove add data, can only view data
                if (MainMenu.Items[2].ChildItems.Count > 1)
                {
                    MainMenu.Items[2].ChildItems.RemoveAt(2);
                    MainMenu.Items[2].ChildItems.RemoveAt(1);
                }
                if (MainMenu.Items.Count > 4)
                {
                    MainMenu.Items.RemoveAt(4); // remove admin bar
                }
                if (MainMenu.Items.Count > 3)
                {
                    MainMenu.Items.RemoveAt(3); // remove red cross data
                }

                Button_Login.Visible = true;
                Label_Login.Visible = false;
                Button_LogOut.Visible = false;
            }
            if (Session["idUser"] != null)
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        int user_id = Convert.ToInt32(Session["idUser"]);
                        var user = (from u in context.Users
                                    where u.id == user_id
                                    select u);

                        if (user.Count() > 0)
                        {
                            Button_Login.Visible = false;
                            Label_Login.Visible = true;
                            Label_Login.Text = "User: " + user.First().Username.Trim();
                            Button_LogOut.Visible = true;

                            if (MainMenu.Items[2].ChildItems.Count < 3)
                            {
                                MenuItem menuDataAdd = new MenuItem();
                                menuDataAdd.Value = "AddDataRain";
                                menuDataAdd.Text = "Ajouter Précipitations";
                                MainMenu.Items[2].ChildItems.Add(menuDataAdd);

                                MenuItem menuDataAddDam = new MenuItem();
                                menuDataAddDam.Value = "AddDataDam";
                                menuDataAddDam.Text = "Ajouter au Nangbéto";
                                MainMenu.Items[2].ChildItems.Add(menuDataAddDam);
                            }
                            if (user.First().Permission == false)
                            {
                                if (user.First().Permission_opt == false)
                                {
                                    if (user.First().Permission_RedCross == true)
                                    {
                                        if (MainMenu.Items[2].ChildItems.Count > 1)
                                        {
                                            MainMenu.Items[2].ChildItems.RemoveAt(2);   //remove add data tab
                                            MainMenu.Items[2].ChildItems.RemoveAt(1);   //remove add data tab
                                        }
                                    }
                                }
                            }
                            if (user.First().Permission_RedCross == false)
                            {
                                if (MainMenu.Items.Count > 3)
                                {
                                    MainMenu.Items.RemoveAt(3); // remove red cross tab
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, DateTime.Now.ToString() + ":  " + ex.Message);
                    trace.Flush();
                }
            }
            if (!IsPostBack)
            {
                loginErrorLabel.Visible = false;
                if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
                {
                    UserTextbox.Text = Request.Cookies["UserName"].Value;
                    passwordTextbox.Attributes["value"] = Request.Cookies["Password"].Value;
                    rememberCheckbox.Checked = true;
                }
                if (Session["idUser"] != null)
                {
                    try
                    {
                        using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                        {
                            int user_id = Convert.ToInt32(Session["idUser"]);
                            var user = (from u in context.Users
                                        where u.id == user_id
                                        select u);

                            if (user.Count() > 0)
                            {
                                if (user.First().Permission == false)
                                {
                                    if (user.First().Permission_opt == false)
                                    {
                                        if (user.First().Permission_RedCross == true)
                                        {
                                            if (MainMenu.Items[2].ChildItems.Count > 1)
                                            {
                                                MainMenu.Items[2].ChildItems.RemoveAt(2);   //remove add data tab
                                                MainMenu.Items[2].ChildItems.RemoveAt(1);   //remove add data tab
                                            }
                                        }
                                    }
                                }
                                if (user.First().Permission_RedCross == false)
                                {
                                    if (MainMenu.Items.Count > 3)
                                    {
                                        if (MainMenu.Items[3].Text == "Red Cross Data")
                                        {
                                            MainMenu.Items.RemoveAt(3); // remove red cross tab
                                        }
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        trace.TraceEvent(TraceEventType.Critical, 1, DateTime.Now.ToString() + ":  " + ex.Message);
                        trace.Flush();
                    }
                }
            }
        }
        protected void MainMenu_MenuItemClick(object sender, MenuEventArgs e)
        {
            try
            {
                switch (e.Item.Value)
                {
                    case "Simulations":
                        MultiView1.ActiveViewIndex = 0;
                        MainMenu.Items[0].Selected = true;
                        break;
                    case "DataPage":
                        MultiView1.ActiveViewIndex = 1;
                        MainMenu.Items[1].Selected = true;
                        break;
                    case "Map":
                        MultiView1.ActiveViewIndex = 3;
                        MainMenu.Items[2].Selected = true;
                        break;
                    case "Users":
                        MultiView1.ActiveViewIndex = 4;
                        MainMenu.Items[3].Selected = true;
                        break;
                    case "About":
                        MultiView1.ActiveViewIndex = 6;
                        MainMenu.Items[4].Selected = true;
                        break;
                    case "Alerts":
                        MultiView1.ActiveViewIndex = 5;
                        MainMenu.Items[6].Selected = true;
                        break;
                    case "Contact":
                        MultiView1.ActiveViewIndex = 7;
                        MainMenu.Items[5].Selected = true;
                        break;
                    case "Log":
                        MultiView1.ActiveViewIndex = 8;
                        MainMenu.Items[7].Selected = true;
                        break;
                    case "AddDataRain":
                        MultiView1.ActiveViewIndex = 9;
                        MainMenu.Items[8].Selected = true;
                        break;
                    case "Help":
                        MultiView1.ActiveViewIndex = 10;
                        MainMenu.Items[9].Selected = true;
                        break;
                    case "AddDataDam":
                        MultiView1.ActiveViewIndex = 11;
                        MainMenu.Items[10].Selected = true;
                        break;
                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, DateTime.Now.ToString() + ":  " + ex.Message);
                trace.Flush();
            }
        }

        protected void Button_Login_Click(object sender, EventArgs e)
        {
            MultiView1.ActiveViewIndex = 2;
        }

        protected void loginButton_Click(object sender, EventArgs e)
        {
            string userName = UserTextbox.Text;
            string password = passwordTextbox.Text;
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var user = (from u in context.Users
                                where u.Username == userName &
                                u.Password == password
                                select u);
                    if (user.Count() < 1)
                    {
                        loginErrorLabel.Visible = true;
                        Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(-1);
                        trace.TraceEvent(TraceEventType.Verbose, 1, DateTime.Now.ToString() + ":  Login failed: username: " + userName);
                        trace.Flush();
                        Button_Login.Visible = true;
                        Label_Login.Text = "";
                        Label_Login.Visible = false;
                    }
                    else
                    {
                        if (rememberCheckbox.Checked)
                        {
                            Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                            Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
                        }
                        else
                        {
                            Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                            Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);

                        }
                        Response.Cookies["UserName"].Value = UserTextbox.Text.Trim();
                        Response.Cookies["Password"].Value = passwordTextbox.Text.Trim();
                        Response.Cookies["ValidLogin"].Value = "true";
                        Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(60);
                        Session.Add("UserName", UserTextbox.Text.Trim());
                        Session.Add("idUser", user.First().id);
                        trace.TraceEvent(TraceEventType.Verbose, 1, DateTime.Now.ToString() + " :Login successful: username: " + userName + ", userID: " + user.First().id);
                        trace.Flush();

                        bool temp_user = new bool();
                        temp_user = user.First().Permission;

                        if (MainMenu.Items[2].ChildItems.Count < 2)
                        {
                            MenuItem menuDataAdd = new MenuItem();
                            menuDataAdd.Value = "AddDataRain";
                            menuDataAdd.Text = "Ajouter Précipitations";
                            MainMenu.Items[2].ChildItems.Add(menuDataAdd);

                            MenuItem menuDataAddDam = new MenuItem();
                            menuDataAddDam.Value = "AddDataDam";
                            menuDataAddDam.Text = "Ajouter au Nangbéto";
                            MainMenu.Items[2].ChildItems.Add(menuDataAddDam);
                        }

                        if (MainMenu.Items.Count < 4)
                        {
                            MenuItem menuRedCross = new MenuItem();
                            menuRedCross.Value = "RedCross";
                            menuRedCross.Text = "Données Croix Rouge";
                            MenuItem menuRedCross_Obs = new MenuItem();
                            menuRedCross_Obs.Value = "Log";
                            menuRedCross_Obs.Text = "Impactes observés";
                            MenuItem menuRedCross_Contact = new MenuItem();
                            menuRedCross_Contact.Value = "Contact";
                            menuRedCross_Contact.Text = "Chaine d'alerte";
                            MenuItem menuRedCross_Alerts = new MenuItem();
                            menuRedCross_Alerts.Value = "Alerts";
                            menuRedCross_Alerts.Text = "Niveaux d'alerte";
                            menuRedCross.ChildItems.Add(menuRedCross_Obs);
                            menuRedCross.ChildItems.Add(menuRedCross_Contact);
                            menuRedCross.ChildItems.Add(menuRedCross_Alerts);
                            MainMenu.Items.AddAt(3, menuRedCross);
                        }
                        if (MainMenu.Items.Count < 5)
                        {
                            MenuItem menuAdmin = new MenuItem();
                            menuAdmin.Value = "Admin";
                            menuAdmin.Text = "Admin";
                            MenuItem menuUsers = new MenuItem();
                            menuUsers.Value = "Users";
                            menuUsers.Text = "Users";
                            menuAdmin.ChildItems.Add(menuUsers);
                            MainMenu.Items.AddAt(4, menuAdmin);
                        }
                      
                        Button_Login.Visible = false;
                        Label_Login.Text = "Usager: " + UserTextbox.Text.Trim();
                        Label_Login.Visible = true;
                        Button_LogOut.Visible = true;
                        MultiView1.ActiveViewIndex = 3;

                        Response.Redirect("Main.aspx");
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, DateTime.Now.ToString() + ":  " + ex.Message);
                trace.Flush();
            }
        }

        protected void Button_LogOut_Click(object sender, EventArgs e)
        {
            Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(-1);
            Session["idUser"] = null;
            Response.Redirect("Main.aspx");
            Button_Login.Visible = true;
            Label_Login.Visible = false;
            Button_LogOut.Visible = false;
        }
    }

}