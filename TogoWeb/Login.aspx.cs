﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;

namespace TogoWeb
{
    public partial class Login : System.Web.UI.Page
    {
        TraceSource trace;
        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");
            if (!IsPostBack)
            {
                loginErrorLabel.Visible = false;
                if (Request.Cookies["UserName"] != null && Request.Cookies["Password"] != null)
                {
                    UserTextbox.Text = Request.Cookies["UserName"].Value;
                    passwordTextbox.Attributes["value"] = Request.Cookies["Password"].Value;
                    rememberCheckbox.Checked = true;
                }
            }
        }

        protected void loginButton_Click(object sender, EventArgs e)
        {
            string userName = UserTextbox.Text;
            string password = passwordTextbox.Text;
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var user = (from u in context.Users
                                where u.Username == userName &
                                u.Password == password
                                select u);
                    if (user.Count() < 1)
                    {
                        loginErrorLabel.Visible = false;
                        Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(-1);
                        trace.TraceEvent(TraceEventType.Verbose, 1, DateTime.Now.ToString() + ":  Login failed: username: " + userName);
                        trace.Flush();
                    }
                    else
                    {
                        if (rememberCheckbox.Checked)
                        {
                            Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(30);
                            Response.Cookies["Password"].Expires = DateTime.Now.AddDays(30);
                        }
                        else
                        {
                            Response.Cookies["UserName"].Expires = DateTime.Now.AddDays(-1);
                            Response.Cookies["Password"].Expires = DateTime.Now.AddDays(-1);

                        }
                        Response.Cookies["UserName"].Value = UserTextbox.Text.Trim();
                        Response.Cookies["Password"].Value = passwordTextbox.Text.Trim();
                        Response.Cookies["ValidLogin"].Value = "true";
                        Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(60);

                        Session.Add("UserName", UserTextbox.Text.Trim());
                        Session.Add("idUser", user.First().id);
                        trace.TraceEvent(TraceEventType.Verbose, 1, DateTime.Now.ToString() + " :Login successful: username: " + userName + ", userID: " + user.First().id);
                        trace.Flush();
                        Response.Redirect("Main.aspx");

                        loginButton.Visible = false;
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, DateTime.Now.ToString() + ":  " + ex.Message);
                trace.Flush();
            }
        }
    }
}