﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.Net.Mail;

namespace TogoWeb
{
    public partial class TogoContact : System.Web.UI.Page
    {
        static TraceSource trace;
        
        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);

            if (!IsPostBack)
            {
                Session.Add("idView", 0);
                Load_GridView_ContactList();
            }

        }
        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }

        protected void Load_GridView_ContactList()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = from c in context.ContactLists
                                select new
                                {
                                    id = c.id,
                                    FirstName = c.FirstName,
                                    LastName = c.LastName,
                                    Email = c.Email
                                    //TechSupport = c.TechSupport
                                };

                    DataTable dt = query.ToDataTable();
                    GridView_ContactList.DataSource = dt;
                    GridView_ContactList.DataBind();

                    Label_NoContacts.Visible = false;

                    if (GridView_ContactList.Rows.Count > 0)
                    {
                        for (int i = 0; i < GridView_ContactList.Rows.Count; i++)
                        {
                            GridViewRow row = GridView_ContactList.Rows[i];

                            ((TextBox)row.FindControl("TextBox_GridView_ID")).Text = dt.Rows[i]["id"].ToString();
                            ((TextBox)row.FindControl("TextBox_GridView_FirstName")).Text = dt.Rows[i]["FirstName"].ToString();
                            ((TextBox)row.FindControl("TextBox_GridView_LastName")).Text = dt.Rows[i]["LastName"].ToString();
                            ((TextBox)row.FindControl("TextBox_GridView_Email")).Text = dt.Rows[i]["Email"].ToString();
                            //((CheckBox)row.FindControl("CheckBox_GridView_TechSupport")).Checked = Convert.ToBoolean(dt.Rows[i]["TechSupport"]);
                        }
                    }
                    else
                    {
                        Label_NoContacts.Visible = true;
                    }

                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Button_GridView_Edit_Command(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_ContactList.Rows[idRow];
            ((TextBox)row.FindControl("TextBox_GridView_FirstName")).Enabled = true;
            ((TextBox)row.FindControl("TextBox_GridView_LastName")).Enabled = true;
            ((TextBox)row.FindControl("TextBox_GridView_Email")).Enabled = true;
            //((CheckBox)row.FindControl("CheckBox_GridView_TechSupport")).Enabled = true;
            ((Button)row.FindControl("Button_GridView_Edit")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Save")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Delete")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Cancel")).Visible = true;
        }

        protected void Button_GridView_Save_Command(object sender, CommandEventArgs e)
        {
            
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_ContactList.Rows[idRow];

            //Check if there is text in the textboxes
            int ck = 0;
            Label_FirstName_Error.Visible = true;
            Label_LastName_Error.Visible = true;
            Label_Email_Error.Visible = true;

            if (((TextBox)row.FindControl("TextBox_GridView_FirstName")).Text == "")
            {
                Label_FirstName_Error.Visible = true;
                ck = 1;
            }
            if (((TextBox)row.FindControl("TextBox_GridView_LastName")).Text == "")
            {
                Label_LastName_Error.Visible = true;
                ck = 1;
            }
            if (((TextBox)row.FindControl("TextBox_GridView_Email")).Text == "")
            {
                Label_Email_Error.Visible = true;
                ck = 1;
            }
            if (ck == 1)
            {
                return;
            }
            Label_FirstName_Error.Visible = false;
            Label_LastName_Error.Visible = false;
            Label_Email_Error.Visible = false;

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    int id = Convert.ToInt32(((TextBox)row.FindControl("TextBox_GridView_ID")).Text);

                    var query = (from c in context.ContactLists
                                 where c.id == id
                                 select c).FirstOrDefault();

                    query.FirstName = ((TextBox)row.FindControl("TextBox_GridView_FirstName")).Text;
                    query.LastName = ((TextBox)row.FindControl("TextBox_GridView_LastName")).Text;
                    query.Email = ((TextBox)row.FindControl("TextBox_GridView_Email")).Text;
                    //query.TechSupport = ((CheckBox)row.FindControl("CheckBox_GridView_TechSupport")).Checked;
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            ((TextBox)row.FindControl("TextBox_GridView_FirstName")).Enabled = false;
            ((TextBox)row.FindControl("TextBox_GridView_LastName")).Enabled = false;
            ((TextBox)row.FindControl("TextBox_GridView_Email")).Enabled = false;
            //((CheckBox)row.FindControl("CheckBox_GridView_TechSupport")).Enabled = false;

            ((Button)row.FindControl("Button_GridView_Edit")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Save")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Delete")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Cancel")).Visible = false;

            Load_GridView_ContactList();

        }

        protected void Button_GridView_Cancel_Command(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_ContactList.Rows[idRow];

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    ((TextBox)row.FindControl("TextBox_GridView_ID")).Enabled = true;
                    int id = Convert.ToInt32(((TextBox)row.FindControl("TextBox_GridView_ID")).Text);
                    ((TextBox)row.FindControl("TextBox_GridView_ID")).Enabled = false;

                    var query = (from c in context.ContactLists
                                where c.id == id
                                select c).FirstOrDefault();

                    ((TextBox)row.FindControl("TextBox_GridView_FirstName")).Text = query.FirstName;
                    ((TextBox)row.FindControl("TextBox_GridView_LastName")).Text = query.LastName;
                    ((TextBox)row.FindControl("TextBox_GridView_Email")).Text = query.Email;
                    //((CheckBox)row.FindControl("CheckBox_GridView_TechSupport")).Checked = query.TechSupport;
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            ((TextBox)row.FindControl("TextBox_GridView_FirstName")).Enabled = false;
            ((TextBox)row.FindControl("TextBox_GridView_LastName")).Enabled = false;
            ((TextBox)row.FindControl("TextBox_GridView_Email")).Enabled = false;
            //((CheckBox)row.FindControl("CheckBox_GridView_TechSupport")).Enabled = false;

            ((Button)row.FindControl("Button_GridView_Edit")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Save")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Delete")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Cancel")).Visible = false;
        }

        protected void Button_AddContact_Click(object sender, EventArgs e)
        {
            Button_AddContact.Enabled = false;
            Label_FirstName_Add.Visible = true;
            TextBox_FirstName_Add.Visible = true;
            Label_LastName_Add.Visible = true;
            TextBox_LastName_Add.Visible = true;
            Label_Email_Add.Visible = true;
            TextBox_Email_Add.Visible = true;
            //CheckBox_TechSupport_Add.Visible = true;
            Button_AddContact_Save.Visible = true;
            Button_AddContact_Cancel.Visible = true;
        }

        protected void Button_AddContact_Save_Click(object sender, EventArgs e)
        {
            //Check if there is text in the textboxes
            int ck = 0;
            if (TextBox_FirstName_Add.Text == "")
            {
                Label_FirstName_Error.Visible = true;
                ck = 1;
            }
            if (TextBox_LastName_Add.Text == "")
            {
                Label_LastName_Error.Visible = true;
                ck = 1;
            }
            if (TextBox_Email_Add.Text == "")
            {
                Label_Email_Error.Visible = true;
                ck = 1;
            }
            if (ck == 1)
            {
                return;
            }
            Label_FirstName_Error.Visible = false;
            Label_LastName_Error.Visible = false;
            Label_Email_Error.Visible = false;

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = from c in context.ContactLists
                                orderby c.id descending
                                select c;

                    ContactList cNew = new ContactList();
                    if (query.Count() > 0)
                    {
                        cNew.id = query.First().id + 1;
                    }
                    else
                    {
                        cNew.id = 1;
                    }

                    cNew.FirstName = TextBox_FirstName_Add.Text;
                    cNew.LastName = TextBox_LastName_Add.Text;
                    cNew.Email = TextBox_Email_Add.Text;
                    //cNew.TechSupport = CheckBox_TechSupport_Add.Checked;

                    context.ContactLists.Add(cNew);
                    context.SaveChanges();
                }

            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            Button_AddContact.Enabled = true;
            Label_FirstName_Add.Visible = false;
            TextBox_FirstName_Add.Visible = false;
            TextBox_FirstName_Add.Text = "";
            Label_LastName_Add.Visible = false;
            TextBox_LastName_Add.Visible = false;
            TextBox_LastName_Add.Text = "";
            Label_Email_Add.Visible = false;
            TextBox_Email_Add.Visible = false;
            TextBox_Email_Add.Text = "";
            //CheckBox_TechSupport_Add.Visible = false;
            //CheckBox_TechSupport_Add.Checked = false;
            Button_AddContact_Save.Visible = false;
            Button_AddContact_Cancel.Visible = false;

            Load_GridView_ContactList();
        }

        protected void Button_AddContact_Cancel_Click(object sender, EventArgs e)
        {
            Button_AddContact.Enabled = true;
            Label_FirstName_Add.Visible = false;
            TextBox_FirstName_Add.Visible = false;
            TextBox_FirstName_Add.Text = "";
            Label_LastName_Add.Visible = false;
            TextBox_LastName_Add.Visible = false;
            TextBox_LastName_Add.Text = "";
            Label_Email_Add.Visible = false;
            TextBox_Email_Add.Visible = false;
            TextBox_Email_Add.Text = "";
            //CheckBox_TechSupport_Add.Visible = false;
            //CheckBox_TechSupport_Add.Checked = false;
            Button_AddContact_Save.Visible = false;
            Button_AddContact_Cancel.Visible = false;
        }

        protected void Button_GridView_Delete_Command(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_ContactList.Rows[idRow];
            ((Label)row.FindControl("Label_Delete_Check")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Delete_Yes")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Delete_No")).Visible = true;
        }

        protected void Button_GridView_Delete_No_Command(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_ContactList.Rows[idRow];
            ((Label)row.FindControl("Label_Delete_Check")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Delete_Yes")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Delete_No")).Visible = false;
        }

        protected void Button_GridView_Delete_Yes_Command(object sender, CommandEventArgs e)
        {
            int idRow = Convert.ToInt32(e.CommandArgument);
            GridViewRow row = GridView_ContactList.Rows[idRow];

            int ContactID = Convert.ToInt32(((TextBox)row.FindControl("TextBox_GridView_ID")).Text);

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = (from c in context.ContactLists
                                where c.id == ContactID
                                select c).FirstOrDefault();

                    context.ContactLists.Remove(query);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            ((TextBox)row.FindControl("TextBox_GridView_FirstName")).Enabled = false;
            ((TextBox)row.FindControl("TextBox_GridView_LastName")).Enabled = false;
            ((TextBox)row.FindControl("TextBox_GridView_Email")).Enabled = false;
            //((CheckBox)row.FindControl("CheckBox_GridView_TechSupport")).Enabled = false;

            ((Button)row.FindControl("Button_GridView_Edit")).Visible = true;
            ((Button)row.FindControl("Button_GridView_Save")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Delete")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Cancel")).Visible = false;

            ((Label)row.FindControl("Label_Delete_Check")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Delete_Yes")).Visible = false;
            ((Button)row.FindControl("Button_GridView_Delete_No")).Visible = false;
            Load_GridView_ContactList();
        }
    }
}