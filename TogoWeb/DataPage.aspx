﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DataPage.aspx.cs" Inherits="TogoWeb.DataPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style4 {
            height: 23px;
        }
        .auto-style5 {
            height: 29px;
        }
        .auto-style6 {
            height: 30px;
        }
        .auto-style7 {
            height: 24px;
        }
        .auto-style8 {
            height: 35px;
        }
        </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="View1" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Stations de Données "></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="Stations_GridView" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="Details_Button" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Details_Button_Command" Text="Détails" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Id" HeaderText="ID">
                                                <ItemStyle HorizontalAlign="Center" Width="70px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Name" HeaderText="Nom">
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="xCoordinate" HeaderText="Latitude">
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="yCoordinate" HeaderText="Longitude">
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View2" runat="server">

                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label11" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station de Données:"></asp:Label>
                                        &nbsp;&nbsp;<asp:TextBox ID="TextBox1_Sta" runat="server" Enabled="False" Font-Names="Calibri" Font-Size="Medium"></asp:TextBox>
                                        &nbsp;<asp:TextBox ID="TextBox1_StationID" runat="server" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Font-Names="Calibri" Text="Variable: "></asp:Label>
                                        <asp:DropDownList ID="DropDownList1_Var" runat="server" AutoPostBack="True" Font-Names="Calibri" OnSelectedIndexChanged="DropDown_Var_SelectedIndexChanged" Font-Size="Medium">
                                        </asp:DropDownList>
                                        &nbsp;<asp:Label ID="Label_Variable_Error" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="S'il vous plaît sélectionner la variable"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label14" runat="server" Font-Names="Calibri" Text="Plage de Données: "></asp:Label>
                                        <asp:TextBox ID="TextBox_DataRange_Start" runat="server" Enabled="False" Visible="False" Font-Names="Calibri" Font-Size="Medium"></asp:TextBox>
                                        <asp:Button ID="DataRange_Start_Button" runat="server" OnClick="DataRange_Start_Button_Click" Text="Sélectionnez le début" Width="169px" Font-Names="Calibri" Font-Size="Medium" />
                                        <asp:Calendar ID="Calendar1" runat="server" Height="105px" OnSelectionChanged="Calendar1_SelectionChanged" Visible="False" Width="152px" Font-Names="Calibri" Font-Size="Medium"></asp:Calendar>
                                        &nbsp;&nbsp;<asp:Label ID="Label15" runat="server" Font-Names="Calibri" Text="a" Font-Size="Medium"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_DataRange_End" runat="server" Enabled="False" Visible="False" Font-Names="Calibri" Font-Size="Medium"></asp:TextBox>
                                        &nbsp;<asp:Button ID="DataRange_End_Button" runat="server" OnClick="DataRange_End_Button_Click" Text="Sélectionnez la fin" Width="159px" Font-Names="Calibri" Font-Size="Medium" />
                                        &nbsp;<asp:Calendar ID="Calendar2" runat="server" OnSelectionChanged="Calendar2_SelectionChanged" Visible="False" Font-Names="Calibri" Font-Size="Medium"></asp:Calendar>
                                        &nbsp;<asp:Button ID="DateRange_Reset_Button" runat="server" OnClick="DateRange_Reset_Button_Click" Text="Réinitialiser" Font-Names="Calibri" Font-Size="Medium" />
                                        &nbsp;&nbsp;<asp:Label ID="Label_DateRange_Error" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="S'il vous plaît sélectionnez une plage de dates valides"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        <asp:Button ID="DateRange_Apply_Button" runat="server" OnClick="DateRange_Apply_Button_Click" Text="Appliquer" Font-Names="Calibri" Font-Size="Medium" />
                                        &nbsp;<asp:Button ID="Cancel_Button" runat="server" Font-Names="Calibri" OnClick="Cancel_Button_Click" Text="Annuler" Font-Size="Medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style4">
                                        <asp:Label ID="Label_NoData" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Black" Text="Aucune donnée disponible pour la période sélectionnée"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="AddValue_Button" runat="server" Font-Names="Calibri" OnClick="AddValue_Button_Click" Text="Ajouter Fiche " Visible="False" Font-Size="Medium" />
                                        &nbsp;<asp:Button ID="Export_Button" runat="server" Font-Names="Calibri" OnClick="Export_Button_Click" Text="Export de fiche au dossier CSV" Visible="False" Width="213px" Font-Size="Medium" />
                                        &nbsp;
                                        <asp:Label ID="Label_ExportError" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="Error exporting file" Visible="False"></asp:Label>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label_ValueError" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="S'il vous plaît entrer la valeur" Visible="False"></asp:Label>
                                        <asp:GridView ID="Data_GridView" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None" style="margin-right: 0px" Font-Size="Medium">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox_ID" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TimeDate" HeaderText="Date" >
                                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="Valeur">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox_Value" runat="server" Enabled="False"></asp:TextBox>
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Units" HeaderText="Unité">
                                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="Button_GridView_Edit" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_GridView_Edit_Click" Text="Editer" />
                                                        <asp:Button ID="Button_GridView_Save" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_GridView_Save_Click" Text="Sauvegarder" Visible="False" />
                                                        <asp:Button ID="Button_GridView_Cancel" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_GridView_Cancel_Click" Text="Annuler" Visible="False" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>

                        </asp:View>
                        <asp:View ID="View3" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Ajouter une valeur de données"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station de données:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox1_Sta_Add" runat="server" Enabled="False" Font-Names="Calibri" Font-Size="Medium"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label19" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox1_Var_Add" runat="server" Enabled="False" Font-Names="Calibri" Font-Size="Medium"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Font-Names="Calibri" Text="Valeur: " Visible="False" Font-Size="Medium"></asp:Label>
                                        <asp:TextBox ID="Value_TextBox" runat="server" Font-Names="Calibri" Visible="False" Font-Size="Medium"></asp:TextBox>
                                        <asp:Label ID="Label_Units" runat="server" Font-Names="Calibri" Text="(units)" Visible="False" Font-Size="Medium"></asp:Label>
                                        &nbsp;<asp:Label ID="Value_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Calendar ID="Calendar_Add" runat="server" Font-Names="Calibri" Font-Size="Medium"></asp:Calendar>
                                        &nbsp;<asp:Label ID="Date_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="S'il vous plaît sélectionner la date" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Font-Names="Calibri" Text="Heure: " Visible="False" Font-Size="Medium"></asp:Label>
                                        <asp:TextBox ID="Hour_TextBox" runat="server" Font-Names="Calibri" Visible="False" Font-Size="Medium"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label9" runat="server" Font-Names="Calibri" Text="Minute: " Visible="False" Font-Size="Medium"></asp:Label>
                                        <asp:TextBox ID="Minute_TextBox" runat="server" Font-Names="Calibri" Visible="False" Font-Size="Medium"></asp:TextBox>
                                        <asp:Label ID="Time_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="S'il vous plaît entrer une heure" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Save_Button" runat="server" OnClick="Save_Button_Click" Text="Sauvegarder" Visible="False" Font-Names="Calibri" Font-Size="Medium" />
                                        &nbsp;<asp:Button ID="Cancel_Add_Button" runat="server" Font-Names="Calibri" OnClick="Cancel_Add_Button_Click" Text="Annuler" Font-Size="Medium" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View4" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td class="auto-style8">
                                        <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station de données:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox1_Sta0" runat="server" Enabled="False" Font-Names="Calibri" Font-Size="Medium"></asp:TextBox>
                                        &nbsp;<asp:TextBox ID="TextBox1_StationID0" runat="server" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style5">
                                        <asp:Label ID="Label13" runat="server" Font-Names="Calibri" Text="Variable: " Font-Size="Medium"></asp:Label>
                                        <asp:DropDownList ID="DropDownList1_Var0" runat="server" AutoPostBack="True" Font-Names="Calibri" OnSelectedIndexChanged="DropDown_Var0_SelectedIndexChanged" Font-Size="Medium">
                                        </asp:DropDownList>
                                        &nbsp;<asp:Label ID="Label_Variable_Error0" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="S'il vous plaît sélectionner la variable" Font-Size="Medium"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style5">
                                        <asp:Label ID="Label16" runat="server" Font-Names="Calibri" Text="Plage de Données: " Font-Size="Medium"></asp:Label>
                                        <asp:TextBox ID="TextBox_DataRange_Start0" runat="server" Enabled="False" Visible="False" Font-Names="Calibri" Font-Size="Medium"></asp:TextBox>
                                        &nbsp;<asp:Button ID="DataRange_Start_Button0" runat="server" OnClick="DataRange_Start_Button0_Click" Text="Sélectionnez le début" Width="162px" Font-Names="Calibri" Font-Size="Medium" />
                                        &nbsp;<asp:Calendar ID="Calendar10" runat="server" Height="105px" OnSelectionChanged="Calendar10_SelectionChanged" Visible="False" Width="152px" Font-Names="Calibri" Font-Size="Medium"></asp:Calendar>
                                        &nbsp;<asp:Label ID="Label17" runat="server" Font-Names="Calibri" Text="a"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_DataRange_End0" runat="server" Enabled="False" Visible="False" Font-Names="Calibri" Font-Size="Medium"></asp:TextBox>
                                        &nbsp;<asp:Button ID="DataRange_End_Button0" runat="server" OnClick="DataRange_End_Button0_Click" Text="Sélectionnez la fin" Width="159px" Font-Names="Calibri" Font-Size="Medium" />
                                        &nbsp;<asp:Calendar ID="Calendar20" runat="server" OnSelectionChanged="Calendar20_SelectionChanged" Visible="False" Font-Names="Calibri" Font-Size="Medium"></asp:Calendar>
                                        &nbsp;<asp:Button ID="DateRange_Reset_Button0" runat="server" OnClick="DateRange_Reset_Button0_Click" Text="Réinitialiser" Font-Names="Calibri" Font-Size="Medium" />
                                        &nbsp;
                                        <asp:Label ID="Label_DateRange_Error0" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="S'il vous plaît sélectionnez une plage de dates valides" Font-Size="Medium"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style6">
                                        <asp:Button ID="DateRange_Apply_Button0" runat="server" OnClick="DateRange_Apply_Button0_Click" Text="Appliquer" Font-Names="Calibri" Font-Size="Medium" />
                                        <asp:Button ID="Cancel_Button0" runat="server" Font-Names="Calibri" OnClick="Cancel_Button_Click" Text="Annuler" Font-Size="Medium" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Export_Button0" runat="server" Font-Names="Calibri" OnClick="Export_Button0_Click" Text="Export de fiche au dossier CSV" Visible="False" Width="216px" Font-Size="Medium" />
                                        &nbsp;<asp:Label ID="Label_ExportError0" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" ForeColor="Red" Text="Error exporting file" Visible="False" Font-Size="Medium"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label_NoData0" runat="server" Font-Italic="True" Font-Names="Calibri" ForeColor="Black" Text="Aucune donnée disponible pour la période sélectionnée"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="Data_GridView0" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None" style="margin-right: 0px" Font-Size="Medium">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox_ID0" runat="server"></asp:TextBox>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="TimeDate" HeaderText="Date" >
                                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Value" HeaderText="Valeur" >
                                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Units" HeaderText="Unité">
                                                <ItemStyle HorizontalAlign="Center" Width="90px" />
                                                </asp:BoundField>
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style4">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    <div>
    
    </div>
    </form>
</body>
</html>
