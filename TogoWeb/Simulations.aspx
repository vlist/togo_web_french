﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Simulations.aspx.cs" Inherits="TogoWeb.Simulations" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 108px;
        }
        .auto-style3 {
            height: 23px;
        }
        .auto-style7 {
            width: 267px;
        }
        .auto-style8 {
            height: 30px;
        }
        .auto-style10 {
            height: 26px;
        }
        .auto-style12 {
            height: 30px;
            }
        .auto-style24 {
            height: 26px;
            }
        .auto-style25 {
            height: 24px;
        }
        .auto-style26 {
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="View1" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Panel ID="Panel1" runat="server" BorderColor="Black" BorderStyle="Double" Width="706px">
                                            &nbsp;&nbsp;<br />&nbsp;&nbsp;<asp:Label ID="Label28" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Simulation le plus recente"></asp:Label>
                                            &nbsp;<asp:Button ID="Button_Review" runat="server" BorderColor="#660066" BorderStyle="Double" Font-Bold="True" Font-Size="Medium" OnClick="Button_Review_Click" Text="Validez!" />
                                            &nbsp;<asp:TextBox ID="SelectedSim_TextBox_Header" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                            <br />
                                            &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label29" runat="server" Font-Names="Calibri" Text="Période de simulation:"></asp:Label>
                                            <asp:Label ID="StartTime_TextHeader" runat="server" Font-Names="Calibri" Text="(Start Time)"></asp:Label>
                                            &nbsp;<asp:Label ID="Label30" runat="server" Font-Names="Calibri" Text="a "></asp:Label>
                                            <asp:Label ID="EndTime_TextHeader" runat="server" Font-Names="Calibri" Text="(End Time)"></asp:Label>
                                            <br />
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label31" runat="server" Font-Names="Calibri" Text="Niveau de risque prévu par modèle FUNES automatisé: "></asp:Label>
                                            <asp:Label ID="Risk_Text_Header" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                            &nbsp;<asp:Image ID="Image_Null_Header" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_A_Header" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                            <asp:Image ID="Image_B_Header" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_C_Header" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_D_Header" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_E_Header" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" Width="22px" />
                                            <br />
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label32" runat="server" Font-Names="Calibri" Text="Niveau de risque prévu par operateurs de barrage: "></asp:Label>
                                            <asp:Label ID="Risk_Calc_Text_Header" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                            &nbsp;<asp:Image ID="Image_Null_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_A_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                            <asp:Image ID="Image_B_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" Width="21px" />
                                            <asp:Image ID="Image_C_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_D_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                            <asp:Image ID="Image_E_Calc_Header" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" Width="22px" />
                                            <br />
                                            &nbsp;&nbsp;&nbsp;
                                            <asp:Label ID="Label_NoSimulations_Header" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" Text="There are no past simulations." Visible="False"></asp:Label>
                                            <br />
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>&nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style10">
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Sommaire: simulations éffectuées recemment"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label_NoSimulations" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" Text="There are no past simulations." Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:GridView ID="Simulations_GridView" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:BoundField DataField="Id" HeaderText="ID" />
                                                <asp:BoundField DataField="StartTime" HeaderText="Période Simulation Début" >
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EndTime" HeaderText="Période Simulation Fin">
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Status" HeaderText="État" />
                                                <asp:BoundField DataField="RunStartTime" HeaderText="Date parcouru" >
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RiskLetter" HeaderText="Niveau Risque Prévu Automatisé" Visible="False" >
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RiskDescription" HeaderText="Niveau Risque Prévu Automatisé" >
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image_Null" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" />
                                                        <asp:Image ID="Image_A" runat="server" Height="20px" ImageUrl="~/Images/red_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_B" runat="server" Height="20px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_C" runat="server" Height="20px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_D" runat="server" Height="20px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_E" runat="server" Height="20px" ImageUrl="~/Images/gray_square.jpg" Visible="False" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="RiskLetterOpt" HeaderText="Niveau Risque Prévu Nangbéto" Visible="False">
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RiskDescriptionOpt" HeaderText="Niveau Risque Prévu Nangbéto" >
                                                <ItemStyle HorizontalAlign="Center" />
                                                </asp:BoundField>
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Image ID="Image_Null_Opt" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" />
                                                        <asp:Image ID="Image_A_Opt" runat="server" Height="20px" ImageUrl="~/Images/red_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_B_Opt" runat="server" Height="20px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_C_Opt" runat="server" Height="20px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_D_Opt" runat="server" Height="20px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                                        <asp:Image ID="Image_E_Opt" runat="server" Height="20px" ImageUrl="~/Images/gray_square.jpg" Visible="False" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Détails">
                                                    <ItemTemplate>
                                                        <asp:Button ID="Simulation_Details_Button" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Details_Button_Command" Text="Détails" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Valider?">
                                                    <ItemTemplate>
                                                        <asp:Button ID="Validated_Button" runat="server" BackColor="#00CC66" CommandArgument="<%# Container.DataItemIndex %>" Font-Bold="True" ForeColor="Black" OnCommand="Validated_Button_Command" Text="Yes/No" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="font-weight: 700">&nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Détails simulation"></asp:Label>
                                        &nbsp;<asp:TextBox ID="SelectedSim_TextBox" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Font-Names="Calibri" Text="Période de simulation: "></asp:Label>
                                        <asp:Label ID="StartTime_Text" runat="server" Font-Names="Calibri" Text="(Start Time)"></asp:Label>
                                        &nbsp;<asp:Label ID="Label8" runat="server" Font-Names="Calibri" Text="a "></asp:Label>
                                        <asp:Label ID="EndTime_Text" runat="server" Font-Names="Calibri" Text="(End Time)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Font-Names="Calibri" Text="État: "></asp:Label>
                                        <asp:Label ID="Status_Text" runat="server" Font-Names="Calibri" Text="(Status)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Font-Names="Calibri" Text="Date parcouru: "></asp:Label>
                                        <asp:Label ID="RunStartTime_Text" runat="server" Font-Names="Calibri" Text="(Simulation Start Time)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3">
                                        <asp:Label ID="Label5" runat="server" Font-Names="Calibri" Text="Niveau risque prévu automatisé: "></asp:Label>
                                        <asp:Label ID="Risk_Text" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Font-Names="Calibri" Text="Description: "></asp:Label>
                                        <asp:Label ID="Description_Text" runat="server" Font-Names="Calibri" Text="(Description)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label36" runat="server" Font-Names="Calibri" Text="Risk niveau risque prévu Nangbéto"></asp:Label>
                                        &nbsp;<asp:Label ID="Risk_Text_Dam" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label37" runat="server" Font-Names="Calibri" Text="Description: "></asp:Label>
                                        <asp:Label ID="Description_Text_Dam" runat="server" Font-Names="Calibri" Text="(Description)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style8">
                                        <asp:Button ID="Ok_Button" runat="server" Height="26px" OnClick="Ok_Button_Click" Text="Ok" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;</td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View3" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label9" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Détails simulation"></asp:Label>
                                        &nbsp;<asp:TextBox ID="SelectedSim_TextBox_Calc" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                        &nbsp;<asp:TextBox ID="SelectedSc_TextBox" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label10" runat="server" Font-Names="Calibri" Text="Période de simulation: "></asp:Label>
                                        <asp:Label ID="StartTime_Text0" runat="server" Font-Names="Calibri" Text="(Start Time)"></asp:Label>
                                        &nbsp;<asp:Label ID="Label11" runat="server" Font-Names="Calibri" Text="a "></asp:Label>
                                        <asp:Label ID="EndTime_Text0" runat="server" Font-Names="Calibri" Text="(End Time)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        <asp:Label ID="Label12" runat="server" Font-Names="Calibri" Text="Niveau risque prévu automatisé: "></asp:Label>
                                        &nbsp;</td>
                                    <td>
                                        <asp:Label ID="Label25" runat="server" Font-Names="Calibri" Text="Risk niveau risque prévu Nangbéto:" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Risk_Text0" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
                                        &nbsp;<asp:Image ID="Image_Null" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_A" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                        <asp:Image ID="Image_B" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_C" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_D" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_E" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" Width="22px" />
                                    </td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Risk_Calc_Text" runat="server" Font-Names="Calibri" Text="(Risk)" Visible="False"></asp:Label>
                                        &nbsp;<asp:Image ID="Image_Null_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_A_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                        <asp:Image ID="Image_B_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" Width="21px" />
                                        <asp:Image ID="Image_C_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_D_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_E_CalcSum" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" Width="22px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style7">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Description_Text0" runat="server" Font-Names="Calibri" Text="(Description)"></asp:Label>
                                        &nbsp;</td>
                                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Description_Calc_Text" runat="server" Font-Names="Calibri" Text="(Description)" Visible="False"></asp:Label>
                                        &nbsp;</td>
                                </tr>
                                <tr>
                                    <td class="auto-style3" colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label14" runat="server" Font-Names="Calibri" Text="Niveau du lac [m.s.m]: "></asp:Label>
                                        <asp:Label ID="WaterLevel_Text" runat="server" Font-Names="Calibri" Text="(Water Level)"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Button ID="Add_Data_Button" runat="server" Font-Bold="True" Height="26px" OnClick="Add_Data_Button_Click" Text="Ajouter données Nangbéto" Width="213px" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label15" runat="server" Font-Names="Calibri" Text="Débit moyen journalier au barrage [m3/sec]:"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style24" colspan="2">
                                        <asp:Label ID="Label35" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text=" " Width="95px"></asp:Label>
                                        <asp:Label ID="Label34" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text="Débit apport" Width="160px"></asp:Label>
                                        <asp:Label ID="Label26" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text="Débit aval - Prévision automatisé" Width="160px"></asp:Label>
                                        <asp:Label ID="Label27" runat="server" Font-Italic="False" Font-Names="Calibri" Font-Underline="True" Text="Débit aval - Prévision Nangbéto" Visible="False" Width="160px"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style24" colspan="2"><asp:Label ID="Label16" runat="server" Font-Names="Calibri" Text="Avant hier:" Width="95px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus2_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus2_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:TextBox ID="FlowMinus2_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style12">
                                        <asp:Label ID="Label17" runat="server" Font-Names="Calibri" Text="Hier:" Width="95px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus1_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayMinus1_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:TextBox ID="FlowMinus1_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style10">
                                        <asp:Label ID="Label18" runat="server" Font-Names="Calibri" Text="Aujourd'hui:" Width="95px"></asp:Label>
                                        <asp:Label ID="FlowDay0_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDay0_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:TextBox ID="Flow0_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style26">
                                        <asp:Label ID="Label19" runat="server" Font-Names="Calibri" Text="Demain:" Width="95px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus1_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus1_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:TextBox ID="FlowPlus1_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style26">
                                        <asp:Label ID="Label20" runat="server" Font-Names="Calibri" Text="Aprés demain:" Width="95px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus2_Label_In" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:Label ID="FlowDayPlus2_Label" runat="server" Font-Italic="False" Font-Names="Calibri" Text="Non disponible" Width="160px"></asp:Label>
                                        <asp:TextBox ID="FlowPlus2_TextBox" runat="server" Enabled="False" Visible="False" Width="160px"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style26" colspan="2">&nbsp;</td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;<asp:Button ID="AcceptSend_Button" runat="server" Font-Bold="True" Height="26px" OnClick="AcceptSend_Button_Click" Text="Accepter et envoyer communication" Width="237px" BackColor="#009900" ForeColor="Black" />
                                        &nbsp;<asp:Label ID="Label_AcceptSend_Alert_Confirm" runat="server" Font-Bold="True" Font-Names="Calibri" ForeColor="Red" Text="Êtes-vous sûr d’envoyer cette communication du risque?" Visible="False"></asp:Label>
                                        &nbsp;<asp:Button ID="AcceptSend_Button_ConfirmYes" runat="server" Font-Bold="True" Height="26px" OnClick="Send_Button_Click" Text="Oui" Visible="False" />
                                        &nbsp;<asp:Button ID="AcceptSend_Button_ConfirmCancel" runat="server" Font-Bold="True" Height="26px" OnClick="AcceptSend_Button_Click_Cancel" Text="Annuler" Visible="False" />
                                        &nbsp;<asp:Button ID="Modify_Button" runat="server" Font-Bold="True" Height="26px" OnClick="Modify_Button_Click" Text="Modifier les débits pour calculer le risque" Width="302px" BackColor="Yellow" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style3" colspan="2">&nbsp;<asp:Button ID="Calculate_Button" runat="server" Font-Bold="True" Height="26px" OnClick="Calculate_Button_Click" Text="Calculer" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Label ID="Label21" runat="server" Font-Names="Calibri" Text="Risque aval calculé:" Visible="False"></asp:Label>
                                        &nbsp; 
                                        <asp:TextBox ID="Calc_ScenarioID" runat="server" Enabled="False" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Risk_Calculated_Text" runat="server" Font-Names="Calibri" Text="(Risk)" Visible="False"></asp:Label>
                                        &nbsp;<asp:Image ID="Image_Null_Calc" runat="server" Height="21px" ImageUrl="~/Images/null_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_A_Calc" runat="server" Height="21px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="21px" />
                                        <asp:Image ID="Image_B_Calc" runat="server" Height="21px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_C_Calc" runat="server" Height="21px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_D_Calc" runat="server" Height="21px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                                        <asp:Image ID="Image_E_Calc" runat="server" Height="21px" ImageUrl="~/Images/gray_square.jpg" Visible="False" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" class="auto-style25">
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:Label ID="Description_Text_Calc" runat="server" Font-Names="Calibri" Text="(Description)" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2">
                                        <asp:Button ID="SaveSend_Button" runat="server" Font-Bold="True" Height="26px" OnClick="SaveSend_Button_Click" Text="Valider et envoyer communication" Visible="False" Width="236px" />
                                        &nbsp;<asp:Label ID="Label_SaveSend_Alert_Confirm" runat="server" Font-Bold="True" Font-Names="Calibri" ForeColor="Red" Text="Êtes-vous sûr d’envoyer cette communication du risque?" Visible="False"></asp:Label>
                                        &nbsp;<asp:Button ID="SaveSend_Button_ConfirmYes" runat="server" Font-Bold="True" Height="26px" OnClick="Send_Button_Click" Text="Oui" Visible="False" />
                                        &nbsp;<asp:Button ID="SaveSend_Button_ConfirmCancel" runat="server" Font-Bold="True" Height="26px" OnClick="SaveSend_Button_Click_Cancel" Text="Annuler" Visible="False" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
