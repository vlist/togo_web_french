﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;

namespace TogoWeb
{
    public partial class AddData : System.Web.UI.Page
    {
        static TraceSource trace;

        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);

            if (!IsPostBack)
            {
                Session.Add("idView", 0);

                Populate_DropDown_Station();
                Populate_DropDown_Variable();
                Reset_Boxes();
                Calendar_Add.SelectedDate = DateTime.Today;

                Button_Reset.Visible = true;
                Save_Button.Visible = true;
            }
        }
        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }

        protected void Populate_DropDown_Station()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var staQuery = from s in context.Stations
                                   select new
                                   {
                                       id = s.id,
                                       Name = s.Name
                                   };

                    DataTable staTable = staQuery.ToDataTable();

                    DropDownList_Station.DataSource = staTable;
                    DropDownList_Station.DataTextField = "Name";
                    DropDownList_Station.DataValueField = "id";
                    DropDownList_Station.DataBind();
                    DropDownList_Station.Items.Insert(0, ".::Sélectionnez station::.");
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void Populate_DropDown_Variable()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var vXsQuery = from v in context.VariableXStations
                              where v.idStation == DropDownList_Station.SelectedIndex
                              select v;

                    DataTable varTable = new DataTable();
                    varTable.Columns.Add("ID", typeof(int));
                    varTable.Columns.Add("Name", typeof(string));
                    varTable.Columns.Add("Unit", typeof(string));

                    foreach (VariableXStation vXs in vXsQuery)
                    {
                        int selectedVar = vXs.idVariable;

                        var varQuery = (from v in context.Variables
                                   where v.id == selectedVar
                                   select v).FirstOrDefault();

                        string NameTextUse = varQuery.Name;

                        if (varQuery.Name.Trim() == "Inflow")
                        {
                            NameTextUse = "Apport";
                        }
                        if (varQuery.Name.Trim() == "Outflow")
                        {
                            NameTextUse = "Débit en aval";
                        }
                        if (varQuery.Name.Trim() == "Water Level")
                        {
                            NameTextUse = "Niveau du lac";
                        }
                        varTable.Rows.Add(varQuery.id, NameTextUse, varQuery.Unit);
                    }

                    DropDownList_Variable.DataSource = varTable;
                    DropDownList_Variable.DataTextField = "Name";
                    DropDownList_Variable.DataValueField = "id";
                    DropDownList_Variable.DataBind();
                    DropDownList_Variable.Items.Insert(0, ".::Sélectionnez variable::.");
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void DropDownList_Add_Variable_SelectedIndexChanged(object sender, EventArgs e)
        {
            Label_Units.Visible = false;
            if (DropDownList_Variable.SelectedValue != ".::Sélectionnez variable::.")
            {
                try
                {
                    using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                    {
                        int selectedVar = Convert.ToInt32(DropDownList_Variable.SelectedValue);
                        var varQuery = (from v in context.Variables
                                        where v.id == selectedVar
                                        select v).FirstOrDefault();

                        Label_Units.Text = varQuery.Unit;
                        if (varQuery.Unit.Trim() == "m MSL") Label_Units.Text = "m.s.m";
                    }
                }
                catch (Exception ex)
                {
                    trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                    throw ex;
                }
                Label_Units.Visible = true;
            }
        }
        protected void DropDownList_Add_Station_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList_Station.SelectedValue != ".::Sélectionnez station::.")
            {
                Populate_DropDown_Variable();
                DropDownList_Variable.Enabled = true;
                Label_Check_Sta.Visible = false;
            }
            else
            {
                DropDownList_Station.SelectedValue = ".::Sélectionnez station::.";
                Label_Units.Visible = false;
                Label_Check_Sta.Visible = true;
                Populate_DropDown_Variable();
                DropDownList_Variable.Enabled = false;
            }

        }
        protected void Reset_Boxes()
        {
            Value_TextBox.Text = "";
            Hour_TextBox.Text = "6";    //default to 6:00 am
            Minute_TextBox.Text = "0";  //default to 6:00 am
            Calendar_Add.SelectedDate = DateTime.Today;
        }
        protected void Reset_Labels()
        {
            Label_Check_Sta.Visible = false;
            Label_Check_Var.Visible = false;
            Value_Check_Label.Visible = false;
            Date_Check_Label.Visible = false;
            Time_Check_Label.Visible = false;
            Label_Units.Visible = false;
        }

        protected void Button_Save_Click(object sender, EventArgs e)
        {
            //check selection/entries and make sure can convert
            Reset_Labels();
            int ck = 0;
            if (DropDownList_Station.SelectedValue == ".::Sélectionnez station::.")
            {
                Label_Check_Sta.Visible = true;
                ck = 1;
            }
            if (DropDownList_Variable.SelectedValue == ".::Sélectionnez variable::.")
            {
                Label_Check_Var.Visible = true;
                ck = 1;
            }
            if (Value_TextBox.Text == "")
            {
                Value_Check_Label.Visible = true;
                ck = 1;
            }
            else
            {
                try
                {
                    double valCheck = Convert.ToDouble(Value_TextBox.Text);
                }
                catch
                {
                    Value_Check_Label.Visible = true;
                    ck = 1;
                }
            }
            if (Hour_TextBox.Text == "")
            {
                Time_Check_Label.Visible = true;
                ck = 1;
            }
            else
            {
                try
                {
                    double hourCheck = Convert.ToDouble(Hour_TextBox.Text);
                }
                catch
                {
                    Time_Check_Label.Visible = true;
                    ck = 1;
                }
            }
            if (Minute_TextBox.Text == "")
            {
                Time_Check_Label.Visible = true;
                ck = 1;
            }
            else
            {
                try
                {
                    double minCheck = Convert.ToDouble(Minute_TextBox.Text);
                }
                catch
                {
                    Time_Check_Label.Visible = true;
                    ck = 1;
                }
            }
            if (ck == 1) return;

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var dat = from d in context.DataValues
                              orderby d.id descending
                              select d;

                    DataValue datAdd = new DataValue();

                    if (dat.Count() > 0)
                    {
                        datAdd.id = dat.First().id + 1;
                    }
                    else
                    {
                        datAdd.id = 1;
                    }

                    datAdd.Value = Convert.ToDouble(Value_TextBox.Text);
                    DateTime dtUse = Calendar_Add.SelectedDate;
                    dtUse = dtUse.AddHours(Convert.ToDouble(Hour_TextBox.Text));
                    dtUse = dtUse.AddMinutes(Convert.ToDouble(Minute_TextBox.Text));
                    datAdd.DateTime = dtUse;

                    int selectedVar = Convert.ToInt32(DropDownList_Variable.SelectedValue);
                    int selectedSta = Convert.ToInt32(DropDownList_Station.SelectedValue);

                    var vXs = (from x in context.VariableXStations
                               where x.idVariable == selectedVar && x.idStation == selectedSta
                               select x).FirstOrDefault();

                    datAdd.idVariableXStation = vXs.id;

                    datAdd.idUsers = Convert.ToInt32(Session["idUser"]);

                    context.DataValues.Add(datAdd);
                    context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            Reset_Boxes();
            Reset_Labels();
        }

        protected void Button_Reset_Click(object sender, EventArgs e)
        {
            DropDownList_Station.SelectedIndex = 0;
            DropDownList_Variable.SelectedIndex = 0;
            Reset_Boxes();
            Reset_Labels();
        }

        protected void Button_Add_Data_Click(object sender, EventArgs e)
        {
            Populate_DropDown_Station();
            Populate_DropDown_Variable();
            Reset_Boxes();
            Calendar_Add.SelectedDate = DateTime.Today;

            Button_Reset.Visible = true;
            Save_Button.Visible = true;

        }
    }
}