﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="About.aspx.cs" Inherits="TogoWeb.About" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
 p.MsoNormal
	{margin-top:0in;
	margin-right:0in;
	margin-bottom:8.0pt;
	margin-left:0in;
	line-height:107%;
	font-size:11.0pt;
	font-family:"Calibri",sans-serif;
	}
        .auto-style2 {
            height: 33px;
        }
        .auto-style4 {
            height: 132px;
            width: 800px;
        }
        .auto-style5 {
            width: 800px;
        }
    pre
	{margin-bottom:.0001pt;
	font-size:10.0pt;
	font-family:"Courier New";
	        margin-left: 0in;
            margin-right: 0in;
            margin-top: 0in;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
    </div>
        <table class="auto-style1">
            <tr>
                <td class="auto-style2">
                    <table class="auto-style1">
                        <tr>
                            <td class="auto-style5">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" Text="À propos"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style5">
                                <asp:Image ID="Image1" runat="server" Height="266px" ImageUrl="~/Images/Logos for FUNES website.png" Width="562px" />
                            </td>
                        </tr>
                        <tr>
                            <td class="auto-style4">
                                <p class="MsoNormal">
                                    Ce site foumit un résumé des simulations passées, et en cours du modèle hydrographique FUNES (Fonction Estimée), dans la rivière Mono du Togo en aval du barrage Nangbéto, ainsi que les précipitations mesurées, le taux et les niveaux d&#39;eau couler.<span lang="FR" style="font-family:&quot;inherit&quot;,serif;mso-ansi-language:FR"><o:p>&nbsp;</o:p></span></p>
                                <p class="MsoNormal">
                                    <o:p>L&#39;interface Web a été développé par un projet soutenu par le Fonds mondial pour la prévention </o:p>
                                    <o:p>des catastrophes et de relèvement (GFDRR, de la Banque Mondiale) Code pour la résilience (CfR).</o:p></p>
                                <p class="MsoNormal">
                                    <o:p>L&#39;objectif principal de FUNES est de prévoir les conditions d&#39;inondation dans les communautés riveraines en aval du barrage Nangbéto afin de declencher </o:p>
                                    <o:p>les procédures standards operationel&nbsp;pré- déterminées de réduction de risques de catastrophes - et de verser au même temps le financement nécessaire à éxecuter ces actions de préparation.</o:p></p>
                                <p class="MsoNormal">
                                    <o:p>Le FUNES permet également aux operateurs de Nangbéto d&#39;utiliser les prévisions dans la production d&#39;électricité.</o:p>
                                    <o:p>Les observateurs disposant d&#39;une connexion peuvent télécharger des enregistrements de données pluviometriques et niveau d&#39;eau qui informent la fonction de FUNES.</o:p>
                                    <o:p>Les administrateurs du site FUNES peuvent configurer des profils d&#39;utilisateurs et gérer des alertes correspondant à différents niveaux de risques d&#39;inondation dans le communautés en aval du barrage Nangbéto.</o:p>
                                </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;</td>
            </tr>
        </table>
    </form>
</body>
</html>
