﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.Web.Script.Serialization;
using System.Globalization;

namespace TogoWeb
{
    public partial class Map : System.Web.UI.Page
    {

        public int nPoints;
        public int nLocs;
        public string pointsString;
        public string pointsTextString;
        public string pointsOutputString;
        public string pointsIDString;
        public string pointsTypeString;
        public string pointsBackgroundFlag;
        public string pointsColorsString;
        public string pointsMarkerString;

        public string boundsString;
        public int indRunId;

        TraceSource trace;
        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            Populate_Simulation_Labels();
            
            nPoints = 0;
            nLocs = 0;
            double maxx = -9999.9;
            double minx = 9999.9;
            double maxy = -9999.9;
            double miny = 9999.9;
            string uiSep = CultureInfo.CurrentUICulture.NumberFormat.NumberDecimalSeparator;

            List<double[]> points = new List<double[]>();
            List<string> pointText = new List<string>();
            List<string> pointOutput = new List<string>();
            List<int> pointsType = new List<int>();
            List<int> pointsBackGrnd = new List<int>();
            List<int> pointID = new List<int>();
            List<string> pointColors = new List<string>();
            List<string> pointMarkers = new List<string>();

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var sta = from s in context.Stations
                              select s;
                    List<Station> ptList = sta.ToList();
                    nPoints = ptList.Count();

                    foreach (Station ss in ptList)
                    {
                        points.Add(new double[] { Convert.ToDouble(ss.xCoordinate), Convert.ToDouble(ss.yCoordinate) });
                        // update max/min bounds
                        if (ss.yCoordinate > maxx) maxy = Convert.ToDouble(ss.yCoordinate);
                        if (ss.yCoordinate < minx) miny = Convert.ToDouble(ss.yCoordinate);
                        if (ss.xCoordinate > maxx) maxx = Convert.ToDouble(ss.xCoordinate);
                        if (ss.xCoordinate < minx) minx = Convert.ToDouble(ss.xCoordinate);

                        pointText.Add(ss.Name.Trim());

                        int back = 0;
                        int nType = 0;  //"0" for data station markers
                        string color = "#660066";
                        string markerType = "marker-stroked";

                        //hardwire nangbeto dam as a different color
                        if (ss.Name.Trim() == "Nangbeto Dam")
                        {
                            nType = 1;  // "1" for dam marker
                            color = "#000000";
                            markerType = "dam";
                        }

                        var query = from q in context.VariableXStations
                                    where q.idStation == ss.id
                                    select q;

                        string outText = "Recent Data:";
                        int ck = 0;
                        foreach (VariableXStation vXs in query)
                        {
                            var dat = from d in context.DataValues
                                      where d.idVariableXStation == vXs.id
                                      orderby d.DateTime descending
                                      select d;

                            if (dat.Count() > 0)
                            {

                                var vari = from v in context.Variables
                                           where v.id == vXs.idVariable
                                           select v;

                                DateTime DateTimeUse = Convert.ToDateTime(dat.First().DateTime);
                                String DateUse = DateTimeUse.Date.ToString("dd MMM yyyy HH:mm");
                                outText = outText + "<br>" + vari.First().Name.Trim() + " " + DateUse + " " + dat.First().Value.ToString() + " " + vari.First().Unit;

                                DateTime DateTimeCheck = DateTime.Today;
                                DateTimeCheck = DateTimeCheck.AddDays(-1);
                                if (DateTimeUse < DateTimeCheck)
                                {
                                    if (nType == 0) markerType = "marker";
                                }

                                ck = 1;
                            }
                            if (ck == 0)
                            {
                                outText = "No Recent Data Available";
                            }
                        }

                        pointsBackGrnd.Add(back);
                        pointsType.Add(nType);
                        pointOutput.Add(outText);
                        pointID.Add(ss.id);
                        pointColors.Add(color);
                        pointMarkers.Add(markerType);
                    }

                    int maxPointID = pointID.Last();

                    var loc = from l in context.Locations
                              where l.FlagMap == true
                              select l;
                    List<Location> locList = loc.ToList();
                    nLocs = locList.Count();
                    nPoints = nPoints + nLocs;

                    foreach (Location ll in locList)
                    {
                        points.Add(new double[] { Convert.ToDouble(ll.xCoordinate), Convert.ToDouble(ll.yCoordinate) });
                        // update max/min bounds - should be carried over from station pts
                        if (ll.yCoordinate > maxx) maxy = Convert.ToDouble(ll.yCoordinate);
                        if (ll.yCoordinate < minx) miny = Convert.ToDouble(ll.yCoordinate);
                        if (ll.xCoordinate > maxx) maxx = Convert.ToDouble(ll.xCoordinate);
                        if (ll.xCoordinate < minx) minx = Convert.ToDouble(ll.xCoordinate);

                        pointText.Add(ll.Name.Trim());

                        int back = 0;
                        int nType = 2;  //"2" is same as "0" for markers

                        string color = "#CCCCCC"; //gray
                        string markerType = "marker-stroked";
                        string outText = "Risk level merits no humanitarian interventions at this location";

                        if (TextBox_MetaID.Text == "")
                        {
                            color = "#CCCCCC"; //white
                            outText = "N/A";
                        }
                        else
                        {
                            int meta = Convert.ToInt32(TextBox_MetaID.Text);

                            //TODO if there is a user-prediction then color stations by that, otherwise color by auto-prediction
                            if (TextBox_MetaID_opt.Text != "")
                            {
                                meta = Convert.ToInt32(TextBox_MetaID_opt.Text);
                            }

                            //var ptColors = ["#FF0000","#FF9900","#FFFFOO","#0000FF","#C0C0C0","#FFFFFF" ]; // red, orange, yellow, blue, gray, white
                            var metaXloc = from mXl in context.MetaXLocations
                                           where mXl.idMetascenarios == meta & mXl.idLocations == ll.id
                                           select mXl;
                            
                            var metaOut = (from m in context.MetascenariosFrenches
                                          where m.id == meta
                                          select m).FirstOrDefault();

                            if (meta == 1)
                            {
                                if (metaXloc.Count() > 0)
                                {
                                    color = "#FF0000"; //red
                                    outText = metaOut.Description;// "Risk level merits all actions (radio alert, water purification tablets, and sealable bags) in this community";
                                }
                            }
                            else if (meta == 2)
                            {
                                if (metaXloc.Count() > 0)
                                {
                                    color = "#FF9900";  //orange
                                    outText = metaOut.Description;// "Risk level merits all actions (radio alert, water purification tablets, and sealable bags) in this community";
                                }
                            }
                            else if (meta == 3)
                            {
                                if (metaXloc.Count() > 0)
                                {
                                    color = "#FFFF66";  //yellow
                                    outText = metaOut.Description;// "Risk level merits all actions (radio alert, water purification tablets, and sealable bags) in this community";
                                }
                            }
                            else if (meta == 4)
                            {
                                if (metaXloc.Count() > 0)
                                {
                                    color = "#0000FF";  //blue
                                    outText = metaOut.Description;// "Risk level merits radio alert, few water purification tablets, and few sealable bags in this community";
                                }
                            }
                            else if (meta == 5)
                            {
                                color = "#808080";  //gray
                                outText = metaOut.Description;// "Risk level merits no humanitarian interventions at this location";
                            }
                            else
                            {
                                color = "#C0C0C0"; //white
                                outText = "N/A";
                            }
                        }

                        pointsBackGrnd.Add(back);
                        pointsType.Add(nType);
                        pointOutput.Add(outText);
                        pointID.Add(ll.id + maxPointID);
                        pointColors.Add(color);
                        pointMarkers.Add(markerType);
                    }


                    pointsString = new JavaScriptSerializer().Serialize(points);
                    pointsTextString = new JavaScriptSerializer().Serialize(pointText);
                    pointsOutputString = new JavaScriptSerializer().Serialize(pointOutput);
                    pointsTypeString = new JavaScriptSerializer().Serialize(pointsType);
                    pointsBackgroundFlag = new JavaScriptSerializer().Serialize(pointsBackGrnd);
                    pointsIDString = new JavaScriptSerializer().Serialize(pointID);
                    pointsColorsString = new JavaScriptSerializer().Serialize(pointColors);
                    pointsMarkerString = new JavaScriptSerializer().Serialize(pointMarkers);

                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }            

            minx = minx - 0.15;
            miny = miny - 0.15;
            maxx = maxx + 0.15;
            maxy = maxy + 0.15;

            List<double[]> boundsList = new List<double[]>();
            boundsList.Add(new double[] { minx, miny });
            boundsList.Add(new double[] { maxx, maxy });
            boundsString = new JavaScriptSerializer().Serialize(boundsList);

        }
        protected void Populate_Simulation_Labels()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var mod = (from m in context.ModelRuns
                              orderby m.RunStartTime descending
                              select m).First();

                    Label_StartTime.Text = mod.StartTime.ToString("dd MMM yyyy");
                    Label_EndTime.Text = mod.EndTime.ToString("dd MMM yyyy");

                    TextBox_SimID.Text = mod.id.ToString();
                    TextBox_MetaID.Text = "";
                    TextBox_MetaID_opt.Text = "";

                    var mXs = from ms in context.ScenariosXMetas
                              where ms.id == mod.idScenariosXMeta
                              select ms;

                    ImageReset();

                    if (mXs.Count() > 0)
                    {
                        var meta = (from mm in context.MetascenariosFrenches
                                    where mm.id == mXs.FirstOrDefault().idMetascenario
                                    select mm).FirstOrDefault();

                        TextBox_MetaID.Text = meta.id.ToString();

                        Label_Risk.Text = meta.Risk.ToString();
                        Label4.Visible = true;
                        Label_Description.Text = meta.Description.ToString();
                        if (meta.Name == "A")
                        {
                            Image_Base_A.Visible = true;
                        }
                        if (meta.Name == "B")
                        {
                            Image_Base_B.Visible = true;
                        }
                        if (meta.Name == "C")
                        {
                            Image_Base_C.Visible = true;
                        }
                        if (meta.Name == "D")
                        {
                            Image_Base_D.Visible = true;
                        }
                        if (meta.Name == "E")
                        {
                            Image_Base_E.Visible = true;
                        }

                        if (mod.idScenariosXMetaOpt > 0)
                        {
                            var mXs_opt = from ms in context.ScenariosXMetas
                                          where ms.id == mod.idScenariosXMetaOpt
                                          select ms;

                            var meta_opt = (from mm in context.MetascenariosFrenches
                                            where mm.id == mXs_opt.FirstOrDefault().idMetascenario
                                            select mm).FirstOrDefault();

                            TextBox_MetaID_opt.Text = meta_opt.id.ToString();

                            Label_Risk_Opt.Text = meta_opt.Risk.ToString();
                            Label10.Visible = true;
                            Label_Description_Opt.Text = meta_opt.Description.ToString();
                            if (meta_opt.Name == "A")
                            {
                                Image_Base_A_Opt.Visible = true;
                            }
                            if (meta_opt.Name == "B")
                            {
                                Image_Base_B_Opt.Visible = true;
                            }
                            if (meta_opt.Name == "C")
                            {
                                Image_Base_C_Opt.Visible = true;
                            }
                            if (meta_opt.Name == "D")
                            {
                                Image_Base_D_Opt.Visible = true;
                            }
                            if (meta_opt.Name == "E")
                            {
                                Image_Base_E_Opt.Visible = true;
                            }
                        }
                        else
                        {
                            Label_Risk_Opt.Text = "en attente de validation par operateur de barrager";
                            Label_Description_Opt.Text = "";
                            Label10.Visible = false;
                            ImageResetOpt();
                        }
                    }
                    else
                    {
                        Label_Risk.Text = mod.Status.ToString();
                        Label4.Visible = false;
                        Label_Description.Text = "";
                        Label_Risk_Opt.Text = "N/A";
                        Label_Description_Opt.Text = "";
                        Label10.Visible = false;

                        if (mod.Status.ToString() == "Ongoing")
                        {
                            Label_Risk.Text = "Calculateur... réessayez dans quelques minutes";
                            Label_Risk_Opt.Text = "Validation par l'opérateur du barrage sera nécessaire";
                        }

                        ImageReset();
                        ImageResetOpt();
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
        protected void ImageReset()
        {
            Image_Base_A.Visible = false;
            Image_Base_B.Visible = false;
            Image_Base_C.Visible = false;
            Image_Base_D.Visible = false;
            Image_Base_E.Visible = false;
        }
        protected void ImageResetOpt()
        {
            Image_Base_A_Opt.Visible = false;
            Image_Base_B_Opt.Visible = false;
            Image_Base_C_Opt.Visible = false;
            Image_Base_D_Opt.Visible = false;
            Image_Base_E_Opt.Visible = false;
        }
    }
}