﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Data;
using SEM;
using System.Net.Mail;

namespace TogoWeb
{
    public partial class Log : System.Web.UI.Page
    {
        static TraceSource trace;

        protected void Page_Load(object sender, EventArgs e)
        {
            trace = new TraceSource("TogoWeb");

            CheckLogin();
            int idUser = Convert.ToInt32(Session["idUser"]);

            if (!IsPostBack)
            {
                Session.Add("idView", 0);
                Load_GridView_Log();
            }
        }

        protected void CheckLogin()
        {
            if (Request.Cookies["ValidLogin"] != null)
            {
                Response.Cookies["ValidLogin"].Expires = DateTime.Now.AddMinutes(5);
                return;
            }
        }

        protected void Load_GridView_Log()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var query = from c in context.IncidentLogs
                                orderby c.DateTime descending
                                select c;

                    DataTable LogTable = new DataTable();
                    LogTable.Columns.Add("ID", typeof(int));
                    LogTable.Columns.Add("DateTimeUse", typeof(string));
                    LogTable.Columns.Add("Location", typeof(string));
                    LogTable.Columns.Add("DamageBlank", typeof(string));
                    LogTable.Columns.Add("DamageNoDetail", typeof(bool));
                    LogTable.Columns.Add("DamageCrops", typeof(bool));
                    LogTable.Columns.Add("DamageFood", typeof(bool));
                    LogTable.Columns.Add("DamageNonFood", typeof(bool));
                    LogTable.Columns.Add("DamageLivestock", typeof(bool));
                    LogTable.Columns.Add("DamageHouse", typeof(bool));
                    LogTable.Columns.Add("DamageDisplaced", typeof(bool));
                    LogTable.Columns.Add("DamageDeaths", typeof(bool));
                    LogTable.Columns.Add("DamageOther", typeof(bool));
                    LogTable.Columns.Add("Description", typeof(string));
                    LogTable.Columns.Add("Action", typeof(string));

                    if (query.Count() > 0)
                    {
                        foreach (IncidentLog iLog in query)
                        {
                            var use = (from u in context.Users
                                      where u.id == iLog.idUsers
                                      select u).FirstOrDefault();

                            var loc = (from l in context.Locations
                                       where l.id == iLog.idLocations
                                       select l).FirstOrDefault();

                            string printName = use.LastName + ", " + use.FirstName;
                            LogTable.Rows.Add(iLog.id, iLog.DateTime.ToString("dd MMM yyyy"), loc.Name, "", iLog.DamageNoDetail, iLog.DamageCrops, iLog.DamageFood, iLog.DamageNonFood, iLog.DamageLivestock, iLog.DamageHouse, iLog.PeopleDisplaced, iLog.Deaths, iLog.Other, iLog.Description, iLog.Intervention);
                        }
                        Label_GridView_None.Visible = false;
                    }
                    else
                    {
                        Label_GridView_None.Visible = true;
                    }

                    GridView_Log.DataSource = LogTable;
                    GridView_Log.DataBind();

                    for (int i = 0; i < GridView_Log.Rows.Count; i++)
                    {
                        GridViewRow row = GridView_Log.Rows[i];
                        ((TextBox)row.FindControl("TextBox_GridView_Description")).Text = LogTable.Rows[i]["Description"].ToString();
                        ((TextBox)row.FindControl("TextBox_GridView_Action")).Text = LogTable.Rows[i]["Action"].ToString();
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Button_AddObservation_Click(object sender, EventArgs e)
        {
            Panel_AddObservation.Visible = true;
            Button_AddObservation.Enabled = false;

            TextBox_AddLocation.Visible = false;
            Button_AddLocation_Save.Visible = false;
            Label_Add_Location_Error.Visible = false;

            Label_nFood.Visible = false;
            Label_nNonFood.Visible = false;
            Label_nLivestock.Visible = false;
            Label_nHouse.Visible = false;
            Label_nDisplaced.Visible = false;
            Label_nDeaths.Visible = false;
            TextBox_nFood.Visible = false;
            TextBox_nNonFood.Visible = false;
            TextBox_nLivestock.Visible = false;
            TextBox_nHouse.Visible = false;
            TextBox_nDisplaced.Visible = false;
            TextBox_nDeaths.Visible = false;

            Calendar_Add.SelectedDate = DateTime.Today;
            Populate_DropDown_Location();

            Button_Add_Save.Visible = true;
            Button_Add_Cancel.Visible = true;
        }

        private void Populate_DropDown_Location()
        {
            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var locQuery = from l in context.Locations
                                   select new
                                   {
                                       id = l.id,
                                       Name = l.Name
                                   };

                    DataTable locTable = locQuery.ToDataTable();

                    DropDownList_Location.DataSource = locTable;
                    DropDownList_Location.DataTextField = "Name";
                    DropDownList_Location.DataValueField = "id";
                    DropDownList_Location.DataBind();
                    DropDownList_Location.Items.Insert(0, ".::Ajouter un localité::.");
                    DropDownList_Location.Items.Insert(0, ".::Sélectionnez location::.");
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }

        protected void Button_Add_Save_Click(object sender, EventArgs e)
        {
            int ck = 0;
            Reset_Error_Messages();

            if (DropDownList_Location.SelectedValue == ".::Sélectionnez location::.")
            {
                Label_Select_Location_Error.Visible = true;
                ck = 1;
            }
            if (DropDownList_Location.SelectedValue == ".::Ajouter un localité::.")
            {
                Label_Select_Location_Error.Visible = true;
                ck = 1;
            }
            if (CheckBox_DamageFood.Checked == true)
            {
                if (TextBox_nFood.Text == "")
                {
                    Label_nFood_Error.Visible = true;
                    ck = 1;
                }
                else
                {
                    try
                    {
                        int nTest = Convert.ToInt32(TextBox_nFood.Text);
                    }
                    catch
                    {
                        Label_nFood_Error.Visible = true;
                        ck = 1;
                    }
                }
            }
            else
            {
                TextBox_nFood.Text = "0";
            }

            if (CheckBox_DamageNonFood.Checked == true)
            {
                if (TextBox_nNonFood.Text == "")
                {
                    Label_nNonFood_Error.Visible = true;
                    ck = 1;
                }
                else
                {
                    try
                    {
                        int nTest = Convert.ToInt32(TextBox_nNonFood.Text);
                    }
                    catch
                    {
                        Label_nNonFood_Error.Visible = true;
                        ck = 1;
                    }
                }
            }
            else
            {
                TextBox_nNonFood.Text = "0";
            }

            if (CheckBox_DamageLivestock.Checked == true)
            {
                if (TextBox_nLivestock.Text == "")
                {
                    Label_nLivestock_Error.Visible = true;
                    ck = 1;
                }
                else
                {
                    try
                    {
                        int nTest = Convert.ToInt32(TextBox_nLivestock.Text);
                    }
                    catch
                    {
                        Label_nLivestock_Error.Visible = true;
                        ck = 1;
                    }
                }
            }
            else
            {
                TextBox_nLivestock.Text = "0";
            }

            if (CheckBox_DamageHouse.Checked == true)
            {
                if (TextBox_nHouse.Text == "")
                {
                    Label_nHouse_Error.Visible = true;
                    ck = 1;
                }
                else
                {
                    try
                    {
                        int nTest = Convert.ToInt32(TextBox_nHouse.Text);
                    }
                    catch
                    {
                        Label_nHouse_Error.Visible = true;
                        ck = 1;
                    }
                }
            }
            else
            {
                TextBox_nHouse.Text = "0";
            }

            if (CheckBox_PeopleDisplaced.Checked == true)
            {
                if (TextBox_nDisplaced.Text == "")
                {
                    Label_nDisplaced_Error.Visible = true;
                    ck = 1;
                }
                else
                {
                    try
                    {
                        int nTest = Convert.ToInt32(TextBox_nDisplaced.Text);
                    }
                    catch
                    {
                        Label_nDisplaced_Error.Visible = true;
                        ck = 1;
                    }
                }
            }
            else
            {
                TextBox_nDisplaced.Text = "0";
            }
            
            if (CheckBox_Deaths.Checked == true)
            {
                if (TextBox_nDeaths.Text == "")
                {
                    Label_nDeaths_Error.Visible = true;
                    ck = 1;
                }
                else
                {
                    try
                    {
                        int nTest = Convert.ToInt32(TextBox_nDeaths.Text);
                    }
                    catch
                    {
                        Label_nDeaths_Error.Visible = true;
                        ck = 1;
                    }
                }
            }
            else
            {
                TextBox_nDeaths.Text = "0";
            }

            int textCount = TextBox_Description.Text.Count();
            int textACount = TextBox_Action.Text.Count(); 

            if (TextBox_Description.Text == "")
            {
                Label_Description_Error.Visible = true;
                ck = 1;
            }
            else
            {
                if (TextBox_Description.Text.Count() >  500)
                {
                    Label_Description_Char_Error.Visible = true;
                    ck = 1;
                }
            }
            if (TextBox_Action.Text == "")
            {
                Label_Action_Error.Visible = true;
                ck = 1;
            }
            else
            {
                if (TextBox_Action.Text.Count() > 500)
                {
                    Label_Action_Char_Error.Visible = true;
                    ck = 1;
                }
            }
            if (ck == 1) return;

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    if (TextBox_LogID.Text == "")
                    {
                        var logQuery = from l in context.IncidentLogs
                                       orderby l.id descending
                                       select l;

                        IncidentLog logAdd = new IncidentLog();

                        if (logQuery.Count() > 0)
                        {
                            logAdd.id = logQuery.First().id + 1;
                        }
                        else
                        {
                            logAdd.id = 1;
                        }

                        logAdd.DateTime = Calendar_Add.SelectedDate;
                        logAdd.DateTimeEntry = DateTime.Now;
                        logAdd.idUsers = Convert.ToInt32(Session["idUser"]);
                        logAdd.idLocations = Convert.ToInt32(DropDownList_Location.SelectedValue);
                        logAdd.Description = TextBox_Description.Text;
                        logAdd.Intervention = TextBox_Action.Text;
                        logAdd.DamageNoDetail = CheckBox_DamageNoDetail.Checked;
                        logAdd.DamageCrops = CheckBox_DamageCrops.Checked;
                        logAdd.DamageFood = CheckBox_DamageFood.Checked;
                        logAdd.DamageNonFood = CheckBox_DamageNonFood.Checked;
                        logAdd.DamageLivestock = CheckBox_DamageLivestock.Checked;
                        logAdd.DamageHouse = CheckBox_DamageHouse.Checked;
                        logAdd.PeopleDisplaced = CheckBox_PeopleDisplaced.Checked;
                        logAdd.Deaths = CheckBox_Deaths.Checked;
                        logAdd.Other = CheckBox_Other.Checked;
                        logAdd.nFood = Convert.ToInt32(TextBox_nFood.Text);
                        logAdd.nNonFood = Convert.ToInt32(TextBox_nNonFood.Text);
                        logAdd.nLivestock = Convert.ToInt32(TextBox_nLivestock.Text);
                        logAdd.nHouse = Convert.ToInt32(TextBox_nHouse.Text);
                        logAdd.nDisplaced = Convert.ToInt32(TextBox_nDisplaced.Text);
                        logAdd.nDeaths = Convert.ToInt32(TextBox_nDeaths.Text);

                        context.IncidentLogs.Add(logAdd);
                        context.SaveChanges();
                    }
                    else
                    {
                        int selectedLog = Convert.ToInt32(TextBox_LogID.Text);
                        var logUpdate = (from l in context.IncidentLogs
                                        where l.id == selectedLog
                                        select l).FirstOrDefault();

                        logUpdate.DateTime = Calendar_Add.SelectedDate;
                        logUpdate.idLocations = Convert.ToInt32(DropDownList_Location.SelectedValue);
                        logUpdate.Description = TextBox_Description.Text;
                        logUpdate.Intervention = TextBox_Action.Text;
                        logUpdate.DamageNoDetail = CheckBox_DamageNoDetail.Checked;
                        logUpdate.DamageCrops = CheckBox_DamageCrops.Checked;
                        logUpdate.DamageFood = CheckBox_DamageFood.Checked;
                        logUpdate.DamageNonFood = CheckBox_DamageNonFood.Checked;
                        logUpdate.DamageLivestock = CheckBox_DamageLivestock.Checked;
                        logUpdate.DamageHouse = CheckBox_DamageHouse.Checked;
                        logUpdate.PeopleDisplaced = CheckBox_PeopleDisplaced.Checked;
                        logUpdate.Deaths = CheckBox_Deaths.Checked;
                        logUpdate.Other = CheckBox_Other.Checked;
                        logUpdate.nFood = Convert.ToInt32(TextBox_nFood.Text);
                        logUpdate.nNonFood = Convert.ToInt32(TextBox_nNonFood.Text);
                        logUpdate.nLivestock = Convert.ToInt32(TextBox_nLivestock.Text);
                        logUpdate.nHouse = Convert.ToInt32(TextBox_nHouse.Text);
                        logUpdate.nDisplaced = Convert.ToInt32(TextBox_nDisplaced.Text);
                        logUpdate.nDeaths = Convert.ToInt32(TextBox_nDeaths.Text);

                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            Reset_AddObservation();
            Reset_Error_Messages();
            Button_AddObservation.Enabled = true;
            Button_Add_Save.Visible = false;
            Button_Add_Cancel.Visible = false;

            TextBox_LogID.Text = "";
            Panel_AddObservation.Visible = false;
            Load_GridView_Log();
        }

        protected void Button_Add_Cancel_Click(object sender, EventArgs e)
        {
            Panel_AddObservation.Visible = false;
            Reset_AddObservation();

            Label_nFood.Visible = false;
            Label_nNonFood.Visible = false;
            Label_nLivestock.Visible = false;
            Label_nHouse.Visible = false;
            Label_nDisplaced.Visible = false;
            Label_nDeaths.Visible = false;
            TextBox_nFood.Visible = false;
            TextBox_nNonFood.Visible = false;
            TextBox_nLivestock.Visible = false;
            TextBox_nHouse.Visible = false;
            TextBox_nDisplaced.Visible = false;
            TextBox_nDeaths.Visible = false;

            TextBox_AddLocation.Visible = false;
            Button_AddLocation_Save.Visible = false;
            Button_AddLocation_Cancel.Visible = false;

            Button_Add_Save.Visible = false;
            Button_Add_Cancel.Visible = false;
            Label_Add_Location_Error.Visible = false;

            Button_AddObservation.Enabled = true;
            TextBox_LogID.Text = "";
            Reset_Error_Messages();
        }
        protected void Button_AddLocation_Save_Click(object sender, EventArgs e)
        {
            Label_Add_Location_Error.Visible = false;
            Label_Add_Location_ExistsError.Visible = false;

            int save_locNew = 0;

            if (TextBox_AddLocation.Text == "")
            {
                Label_Add_Location_Error.Visible = true;
                return;
            }

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var locQuery = from l in context.Locations
                                   where l.Name == TextBox_AddLocation.Text
                                   select l;

                    if (locQuery.Count() > 0)
                    {
                        Label_Add_Location_ExistsError.Visible = true;
                        return;
                    }
                    else
                    {
                        Location locNew = new Location();
                        
                        var locAll = from ll in context.Locations
                                     orderby ll.id descending
                                     select ll;

                        if (locAll.Count() > 0)
                        {
                            locNew.id = locAll.First().id + 1;
                        }
                        else
                        {
                            locNew.id = 1;
                        }

                        save_locNew = locNew.id;

                        locNew.Name = TextBox_AddLocation.Text;
                        locNew.FlagMap = false; // do not show on map
                        context.Locations.Add(locNew);
                        context.SaveChanges();
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }

            Button_AddLocation_Save.Visible = false;
            Button_AddLocation_Cancel.Visible = false;
            TextBox_AddLocation.Visible = false;
            TextBox_AddLocation.Text = "";

            Populate_DropDown_Location();
            DropDownList_Location.Enabled = true;
            DropDownList_Location.SelectedIndex = save_locNew;

            Reset_AddObservation();
        }

        protected void Button_AddLocation_Cancel_Click(object sender, EventArgs e)
        {
            Button_AddLocation_Save.Visible = false;
            Button_AddLocation_Cancel.Visible = false;
            TextBox_AddLocation.Visible = false;
            TextBox_AddLocation.Text = "";
            Label_Add_Location_Error.Visible = false;
            Label_Add_Location_ExistsError.Visible = false;

            DropDownList_Location.Enabled = true;
            DropDownList_Location.SelectedValue = ".::Sélectionnez location::.";
            DropDownList_Location.Enabled = true;
        }

        protected void DropDownList_Location_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (DropDownList_Location.SelectedValue == ".::Ajouter un localité::.")
            {
                TextBox_AddLocation.Visible = true;
                Button_AddLocation_Save.Visible = true;
                Button_AddLocation_Cancel.Visible = true;

                DropDownList_Location.Enabled = false;
            }
        }
        private void Reset_AddObservation()
        {
            CheckBox_DamageNoDetail.Checked = false;
            CheckBox_DamageCrops.Checked = false;
            CheckBox_DamageFood.Checked = false;
            CheckBox_DamageNonFood.Checked = false;
            CheckBox_DamageLivestock.Checked = false;
            CheckBox_DamageHouse.Checked = false;
            CheckBox_PeopleDisplaced.Checked = false;
            CheckBox_Deaths.Checked = false;
            CheckBox_Other.Checked = false;
            TextBox_Description.Text = "";
            TextBox_Action.Text = "";
            TextBox_nFood.Text = "";
            TextBox_nNonFood.Text = "";
            TextBox_nLivestock.Text = "";
            TextBox_nHouse.Text = "";
            TextBox_nDisplaced.Text = "";
            TextBox_nDeaths.Text = "";

            Calendar_Add.SelectedDate = DateTime.Today;
        }
        private void Reset_Error_Messages()
        {
            Label_Select_Location_Error.Visible = false;
            Label_Add_Location_Error.Visible = false;
            Label_Add_Location_ExistsError.Visible = false;
            Label_nFood_Error.Visible = false;
            Label_nNonFood_Error.Visible = false;
            Label_nLivestock_Error.Visible = false;
            Label_nHouse_Error.Visible = false;
            Label_nDisplaced_Error.Visible = false;
            Label_nDeaths_Error.Visible = false;
            Label_Description_Error.Visible = false;
            Label_Action_Error.Visible = false;
            Label_Description_Char_Error.Visible = false;
            Label_Action_Char_Error.Visible = false;
        }

        protected void CheckBox_DamageFood_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_DamageFood.Checked == true)
            {
                Label_nFood.Visible = true;
                TextBox_nFood.Text = "";
                TextBox_nFood.Visible = true;
            }
            else
            {
                Label_nFood.Visible = false;
                TextBox_nFood.Visible = false;
                TextBox_nFood.Text = "";
            }
        }

        protected void CheckBox_DamageNonFood_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_DamageNonFood.Checked == true)
            {
                Label_nNonFood.Visible = true;
                TextBox_nNonFood.Text = "";
                TextBox_nNonFood.Visible = true;
            }
            else
            {
                Label_nNonFood.Visible = false;
                TextBox_nNonFood.Visible = false;
                TextBox_nNonFood.Text = "";
            }
        }

        protected void CheckBox_DamageLivestock_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_DamageLivestock.Checked == true)
            {
                Label_nLivestock.Visible = true;
                TextBox_nLivestock.Text = "";
                TextBox_nLivestock.Visible = true;
            }
            else
            {
                Label_nLivestock.Visible = false;
                TextBox_nLivestock.Visible = false;
                TextBox_nLivestock.Text = "";
            }
        }

        protected void CheckBox_DamageHouse_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_DamageHouse.Checked == true)
            {
                Label_nHouse.Visible = true;
                TextBox_nHouse.Text = "";
                TextBox_nHouse.Visible = true;
            }
            else
            {
                Label_nHouse.Visible = false;
                TextBox_nHouse.Visible = false;
                TextBox_nHouse.Text = "";
            }
        }

        protected void CheckBox_PeopleDisplaced_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_PeopleDisplaced.Checked == true)
            {
                Label_nDisplaced.Visible = true;
                TextBox_nDisplaced.Visible = true;
                TextBox_nDisplaced.Text = "";
            }
            else
            {
                Label_nDisplaced.Visible = false;
                TextBox_nDisplaced.Visible = false;
                TextBox_nDisplaced.Text = "";
            }
        }

        protected void CheckBox_Deaths_CheckedChanged(object sender, EventArgs e)
        {
            if (CheckBox_Deaths.Checked == true)
            {
                Label_nDeaths.Visible = true;
                TextBox_nDeaths.Visible = true;
                TextBox_nDeaths.Text = "";
            }
            else
            {
                Label_nDeaths.Visible = false;
                TextBox_nDeaths.Visible = false;
                TextBox_nDeaths.Text = "";
            }
        }

        protected void Button_GridView_Log_Command(object sender, CommandEventArgs e)
        {
            Panel_AddObservation.Visible = true;
            Button_AddObservation.Enabled = false;

            TextBox_AddLocation.Visible = false;
            Button_AddLocation_Save.Visible = false;
            Label_Add_Location_Error.Visible = false;

            Label_nFood.Visible = false;
            Label_nNonFood.Visible = false;
            Label_nLivestock.Visible = false;
            Label_nHouse.Visible = false;
            Label_nDisplaced.Visible = false;
            Label_nDeaths.Visible = false;
            TextBox_nFood.Visible = false;
            TextBox_nNonFood.Visible = false;
            TextBox_nLivestock.Visible = false;
            TextBox_nHouse.Visible = false;
            TextBox_nDisplaced.Visible = false;
            TextBox_nDeaths.Visible = false;

            Calendar_Add.SelectedDate = DateTime.Today;
            Populate_DropDown_Location();

            Button_Add_Save.Visible = true;
            Button_Add_Cancel.Visible = true;

            int rowIndex = Convert.ToInt32(e.CommandArgument);
            GridViewRow rowtest = GridView_Log.Rows[rowIndex];
            int selectedLog = Convert.ToInt32(rowtest.Cells[0].Text);

            try
            {
                using (Togo_Test_dataEntities context = new Togo_Test_dataEntities())
                {
                    var log = (from ll in context.IncidentLogs
                               where ll.id == selectedLog
                               select ll).FirstOrDefault();

                    TextBox_LogID.Text = selectedLog.ToString();

                    CheckBox_DamageNoDetail.Checked = log.DamageNoDetail;
                    CheckBox_DamageCrops.Checked = log.DamageCrops;
                    CheckBox_DamageFood.Checked = log.DamageFood;
                    CheckBox_DamageNonFood.Checked = log.DamageNonFood;
                    CheckBox_DamageLivestock.Checked = log.DamageLivestock;
                    CheckBox_DamageHouse.Checked = log.DamageHouse;
                    TextBox_nFood.Text = log.nFood.ToString();
                    TextBox_nNonFood.Text = log.nNonFood.ToString();
                    TextBox_nLivestock.Text = log.nLivestock.ToString();
                    TextBox_nHouse.Text = log.nHouse.ToString();
                    CheckBox_PeopleDisplaced.Checked = log.PeopleDisplaced;
                    TextBox_nDisplaced.Text = log.nDisplaced.ToString();
                    CheckBox_Deaths.Checked = log.Deaths;
                    TextBox_nDeaths.Text = log.nDeaths.ToString();
                    CheckBox_Other.Checked = log.Other;
                    TextBox_Description.Text = log.Description;
                    TextBox_Action.Text = log.Intervention;
                    Calendar_Add.SelectedDate = log.DateTime;
                    DropDownList_Location.SelectedIndex = log.idLocations;

                    if (CheckBox_DamageFood.Checked == true)
                    {
                        Label_nFood.Visible = true;
                        TextBox_nFood.Visible = true;
                    }
                    if (CheckBox_DamageNonFood.Checked == true)
                    {
                        Label_nNonFood.Visible = true;
                        TextBox_nNonFood.Visible = true;
                    }
                    if (CheckBox_DamageLivestock.Checked == true)
                    {
                        Label_nLivestock.Visible = true;
                        TextBox_nLivestock.Visible = true;
                    }
                    if (CheckBox_DamageHouse.Checked == true)
                    {
                        Label_nHouse.Visible = true;
                        TextBox_nHouse.Visible = true;
                    }
                    if (CheckBox_PeopleDisplaced.Checked == true)
                    {
                        Label_nDisplaced.Visible = true;
                        TextBox_nDisplaced.Visible = true;
                    }
                    if (CheckBox_Deaths.Checked == true)
                    {
                        Label_nDeaths.Visible = true;
                        TextBox_nDeaths.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                trace.TraceEvent(TraceEventType.Critical, 1, ex.Message);
                throw ex;
            }
        }
    }
}