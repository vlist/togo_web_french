﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Log.aspx.cs" Inherits="TogoWeb.Log" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 29px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="X-Large" Text="Sommaire d’observations"></asp:Label>
                &nbsp;<asp:TextBox ID="TextBox_LogID" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Button_AddObservation" runat="server" Font-Names="Calibri" OnClick="Button_AddObservation_Click" Text="Ajouter observations" Font-Size="Medium" />
&nbsp;<asp:Button ID="Button_Add_Save" runat="server" Font-Names="Calibri" OnClick="Button_Add_Save_Click" Text="Sauvegarder" Visible="False" Font-Size="Medium" />
&nbsp;<asp:Button ID="Button_Add_Cancel" runat="server" Font-Names="Calibri" OnClick="Button_Add_Cancel_Click" Text="Annuler" Visible="False" Font-Size="Medium" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel1" runat="server">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel_AddObservation" runat="server" Visible="False" Width="1003px">
                        <table class="auto-style1">
                            <tr>
                                <td class="auto-style2">
                                    <asp:Label ID="Label_Add_Calendar" runat="server" Font-Names="Calibri" Text="Date d’observation:"></asp:Label>
                                    &nbsp;<asp:Calendar ID="Calendar_Add" runat="server" BackColor="White" BorderColor="#999999" CellPadding="4" DayNameFormat="Shortest" Font-Names="Calibri" Font-Size="Medium" ForeColor="Black" Height="180px" Width="200px">
                                        <DayHeaderStyle BackColor="#CCCCCC" Font-Bold="True" Font-Size="7pt" />
                                        <NextPrevStyle VerticalAlign="Bottom" />
                                        <OtherMonthDayStyle ForeColor="Gray" />
                                        <SelectedDayStyle BackColor="#666666" Font-Bold="True" ForeColor="White" />
                                        <SelectorStyle BackColor="#CCCCCC" />
                                        <TitleStyle BackColor="#999999" BorderColor="Black" Font-Bold="True" />
                                        <TodayDayStyle BackColor="#CCCCCC" ForeColor="Black" />
                                        <WeekendDayStyle BackColor="#FFFFCC" />
                                    </asp:Calendar>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2">
                                    <asp:Label ID="Label_Add_Location" runat="server" Font-Names="Calibri" Text="Localité: " Font-Size="Medium"></asp:Label>
                                    <asp:DropDownList ID="DropDownList_Location" runat="server" AutoPostBack="True" Font-Names="Calibri" OnSelectedIndexChanged="DropDownList_Location_SelectedIndexChanged" Font-Size="Medium">
                                    </asp:DropDownList>
                                    &nbsp;<asp:TextBox ID="TextBox_AddLocation" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    &nbsp;<asp:Button ID="Button_AddLocation_Save" runat="server" Font-Names="Calibri" OnClick="Button_AddLocation_Save_Click" Text="Ajouter un localité" Visible="False" Font-Size="Medium" />
                                    &nbsp;<asp:Button ID="Button_AddLocation_Cancel" runat="server" Font-Names="Calibri" OnClick="Button_AddLocation_Cancel_Click" Text="Annuler" Visible="False" Font-Size="Medium" />
                                    &nbsp;<asp:Label ID="Label_Select_Location_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît sélectionnez localité" Visible="False"></asp:Label>
                                    <asp:Label ID="Label_Add_Location_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît entrez le nom de localité" Visible="False"></asp:Label>
                                    <asp:Label ID="Label_Add_Location_ExistsError" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="Cette localité est déjà enregistré" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td class="auto-style2">
                                    <asp:Label ID="Label_Add_Damage" runat="server" Font-Names="Calibri" Text="Info dégâts:" Font-Size="Medium"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:CheckBox ID="CheckBox_DamageNoDetail" runat="server" Font-Names="Calibri" Text="Aucun détail disponible" />
                                    &nbsp;&nbsp;&nbsp;&nbsp; </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;<asp:CheckBox ID="CheckBox_DamageCrops" runat="server" Font-Names="Calibri" Text="Récolte" />
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_DamageFood" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_DamageFood_CheckedChanged" Text="Vivre" />
                                    &nbsp;<asp:Label ID="Label_nFood" runat="server" Font-Names="Calibri" Text="- Nombre de vivre les dommages:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nFood" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    &nbsp;<asp:Label ID="Label_nFood_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît entrez le nombre de vivre dommages" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_DamageNonFood" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_DamageNonFood_CheckedChanged" Text="Non Vivre" />
                                    &nbsp;<asp:Label ID="Label_nNonFood" runat="server" Font-Names="Calibri" Text="- Nombre de non vivre les dommages:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nNonFood" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    <asp:Label ID="Label_nNonFood_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît entrez le nombre de non vivre dommages" Visible="False"></asp:Label>
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_DamageLivestock" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_DamageLivestock_CheckedChanged" Text="Bêtail" />
                                    &nbsp;<asp:Label ID="Label_nLivestock" runat="server" Font-Names="Calibri" Text="- Nombre bêtail des dommages:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nLivestock" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    <asp:Label ID="Label_nLivestock_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît entrez le nombre bêtail avec des dommages" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_DamageHouse" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_DamageHouse_CheckedChanged" Text="Cases" />
                                    &nbsp;<asp:Label ID="Label_nHouse" runat="server" Font-Names="Calibri" Text="- Nombre cases des dommages:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nHouse" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    <asp:Label ID="Label_nHouse_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît entrez le nombre cases avec des dommages" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_PeopleDisplaced" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_PeopleDisplaced_CheckedChanged" Text="Déplacées" />
                                    &nbsp;<asp:Label ID="Label_nDisplaced" runat="server" Font-Names="Calibri" Text="- Nombre de personnes déplacées:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nDisplaced" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    &nbsp;<asp:Label ID="Label_nDisplaced_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît entrez le nombre de personnes déplacées" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_Deaths" runat="server" AutoPostBack="True" Font-Names="Calibri" OnCheckedChanged="CheckBox_Deaths_CheckedChanged" Text="Décès" />
                                    &nbsp;<asp:Label ID="Label_nDeaths" runat="server" Font-Names="Calibri" Text="- Nombre de décès:" Visible="False"></asp:Label>
                                    &nbsp;<asp:TextBox ID="TextBox_nDeaths" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                                    &nbsp;<asp:Label ID="Label_nDeaths_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît entrez le nombre de décès" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    <asp:CheckBox ID="CheckBox_Other" runat="server" Font-Names="Calibri" Text="Autres" />
                                    &nbsp;</td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label_Add_Description" runat="server" Font-Names="Calibri" Text="Déscription: "></asp:Label>
                                    <asp:TextBox ID="TextBox_Description" runat="server" Font-Names="Calibri" TextMode="MultiLine" Width="250px"></asp:TextBox>
                                    &nbsp;<asp:Label ID="Label_Description_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît décrire l'incident et les dommages" Visible="False"></asp:Label>
                                    <asp:Label ID="Label_Description_Char_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="Texte dépasse le nombre maximum de caractères autorisés" Visible="False"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Label ID="Label_Add_Action" runat="server" Font-Names="Calibri" Text="Action: "></asp:Label>
                                    <asp:TextBox ID="TextBox_Action" runat="server" Font-Names="Calibri" TextMode="MultiLine" Width="250px"></asp:TextBox>
                                    <asp:Label ID="Label_Action_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="S'il vous plaît résumer les actions effectuées" Visible="False"></asp:Label>
                                    <asp:Label ID="Label_Action_Char_Error" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="Red" Text="Texte dépasse le nombre maximum de caractères autorisés" Visible="False"></asp:Label>
                                </td>
                            </tr>
                        </table>
                        </asp:Panel>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label_GridView_None" runat="server" Font-Italic="True" Font-Names="Calibri" Text="No Observations Recorded" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GridView_Log" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                        <AlternatingRowStyle BackColor="White" />
                        <Columns>
                            <asp:BoundField DataField="ID" HeaderText="ID" />
                            <asp:BoundField DataField="DateTimeUse" HeaderText="Date" />
                            <asp:BoundField DataField="Location" HeaderText="Localité" />
                            <asp:BoundField HeaderText="Dégâts:" />
                            <asp:CheckBoxField DataField="DamageNoDetail" HeaderText="Aucun detail ">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageCrops" HeaderText="Récolte">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageFood" HeaderText="Vivre">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageNonFood" HeaderText="Non Vivre" >
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageLivestock" HeaderText="Bêtail" >
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageHouse" HeaderText="Cases" >
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageDisplaced" HeaderText="Déplacées" >
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageDeaths" HeaderText="Décès">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:CheckBoxField DataField="DamageOther" HeaderText="Autres">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" />
                            </asp:CheckBoxField>
                            <asp:TemplateField HeaderText="Détails">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox_GridView_Description" runat="server" Enabled="False" Font-Names="Calibri" TextMode="MultiLine" Width="250px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Action">
                                <ItemTemplate>
                                    <asp:TextBox ID="TextBox_GridView_Action" runat="server" Enabled="False" Font-Names="Calibri" TextMode="MultiLine" Width="250px"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Réviser">
                                <ItemTemplate>
                                    <asp:Button ID="Button_GridView_Log_Edit" runat="server" CommandArgument="<%# Container.DataItemIndex %>" Font-Names="Calibri" OnCommand="Button_GridView_Log_Command" Text="Réviser" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                        <EditRowStyle BackColor="#2461BF" />
                        <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                        <RowStyle BackColor="#EFF3FB" />
                        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                        <SortedAscendingCellStyle BackColor="#F5F7FB" />
                        <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                        <SortedDescendingCellStyle BackColor="#E9EBEF" />
                        <SortedDescendingHeaderStyle BackColor="#4870BE" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
