﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Users.aspx.cs" Inherits="TogoWeb.Users" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 25px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                        <asp:View ID="View1" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Gestión des utilisateurs"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button_User_Add" runat="server" OnClick="Button_User_Add_Click" Text="Ajouter usager" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:GridView ID="GridView_Users" runat="server" AutoGenerateColumns="False" CellPadding="4" Font-Names="Calibri" ForeColor="#333333" GridLines="None">
                                            <AlternatingRowStyle BackColor="White" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="ID" Visible="False">
                                                    <ItemTemplate>
                                                        <asp:TextBox ID="TextBox_ID" runat="server"></asp:TextBox>
                                                        <br />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Username" HeaderText="UN" />
                                                <asp:BoundField DataField="FirstName" HeaderText="Prénom" />
                                                <asp:BoundField DataField="LastName" HeaderText="Nom" />
                                                <asp:TemplateField HeaderText="Privilèges administratifs">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox_Permission" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Privilèges Nangbetó">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox_PermissionOpt" runat="server" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Privilèges Croix Rouge">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox_PermissionRedCross" runat="server" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Technical Support">
                                                    <ItemTemplate>
                                                        <asp:CheckBox ID="CheckBox_TechSupport" runat="server" />
                                                    </ItemTemplate>
                                                    <ItemStyle HorizontalAlign="Center" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="Email" HeaderText="Courriel" />
                                                <asp:TemplateField>
                                                    <ItemTemplate>
                                                        <asp:Button ID="Button_GridView_Edit" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_GridView_Edit_Click" Text="Réviser" />
                                                        &nbsp;<asp:Button ID="Button_GridView_Remove" runat="server" CommandArgument="<%# Container.DataItemIndex %>" OnCommand="Button_GridView_Remove_Click" Text="Effacer" />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                            <EditRowStyle BackColor="#2461BF" />
                                            <FooterStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                                            <RowStyle BackColor="#EFF3FB" />
                                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                                            <SortedAscendingCellStyle BackColor="#F5F7FB" />
                                            <SortedAscendingHeaderStyle BackColor="#6D95E1" />
                                            <SortedDescendingCellStyle BackColor="#E9EBEF" />
                                            <SortedDescendingHeaderStyle BackColor="#4870BE" />
                                        </asp:GridView>
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                        <asp:View ID="View2" runat="server">
                            <table class="auto-style1">
                                <tr>
                                    <td>
                                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Enregister/Réviser Usager"></asp:Label>
                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:TextBox ID="TextBox_AddUser_ID" runat="server" Visible="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label3" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="UN: "></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_Username" runat="server"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_Username" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="S'il vous plaît entrez le UN" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label4" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Password:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_Password" runat="server" TextMode="Password"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_Password" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="S'il vous plaît entrez le mot de passe" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label5" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Prénom:"></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_FirstName" runat="server"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_FirstName" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="S'il vous plaît entrez le prénom" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label6" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Nom: "></asp:Label>
                                        &nbsp;<asp:TextBox ID="TextBox_AddUser_LastName" runat="server"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_LastName" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="S'il vous plaît entrez le nom" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label8" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Courriel: "></asp:Label>
                                        <asp:TextBox ID="TextBox_AddUser_Email" runat="server"></asp:TextBox>
                                        &nbsp;<asp:Label ID="Label_Error_AddUser_Email" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="Red" Text="Please S'il vous plaît entrez le courriel" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label9" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" Text="Entrer plus d'un e-mail en séparant les adresses avec un point-virgule (ex: user@email.com;user2@email.com)"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label7" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Privilèges administratifs: "></asp:Label>
                                        &nbsp;<asp:CheckBox ID="CheckBox_AddUser_Permission" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="Label10" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Privilèges Nangbetó: "></asp:Label>
                                        <asp:CheckBox ID="CheckBox_AddUser_PermissionOpt" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="Label11" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Privilèges Croix Rouge:"></asp:Label>
                                        <asp:CheckBox ID="CheckBox_AddUser_PermissionRedCross" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td class="auto-style2">
                                        <asp:Label ID="Label12" runat="server" Font-Bold="False" Font-Names="Calibri" Font-Size="Medium" Text="Technical Support:"></asp:Label>
                                        &nbsp;<asp:CheckBox ID="CheckBox_AddUser_TechnicalSupport" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="Label_Warning_NoAdminPriv" runat="server" Font-Bold="False" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" Text="S'il vous plaît contacter un administrateur pour modifier les privilèges des utilisateurs" Visible="False"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Button ID="Button_User_Save" runat="server" OnClick="Button_User_Save_Click" Text="Sauvegarder" />
                                        &nbsp;<asp:Button ID="Button_User_Cancel" runat="server" OnClick="Button_User_Cancel_Click" Text="Annuler" />
                                    </td>
                                </tr>
                            </table>
                        </asp:View>
                    </asp:MultiView>
                </td>
            </tr>
            </table>
    
    </div>
    </form>
</body>
</html>
