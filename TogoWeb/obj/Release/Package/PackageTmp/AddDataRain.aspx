﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddDataRain.aspx.cs" Inherits="TogoWeb.AddDataRain" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
        .auto-style3 {
            height: 10px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
        <table class="auto-style1">
            <tr>
                <td style="margin-left: 280px">
                    <asp:Label ID="Label1" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Font-Underline="True" Text="Ajouter une valeur de données de précipitations"></asp:Label>
                    &nbsp;<asp:Label ID="Label2" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" Text="Pour ajouter des données à une station différente, s'il vous plaît sélectionner dans le menu &quot;Données hydro / Ajouter&quot;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="margin-left: 280px">
                    &nbsp; <asp:Label ID="Date_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît sélectionner la date"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Calendar ID="Calendar_Add" runat="server" Font-Names="Calibri" OnSelectionChanged="Calendar_Add_SelectionChanged"></asp:Calendar>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Font-Names="Calibri" Text="Heure: "></asp:Label>
                    <asp:TextBox ID="Hour_TextBox" runat="server" Font-Names="Calibri">6</asp:TextBox>
                    &nbsp;<asp:Label ID="Label9" runat="server" Font-Names="Calibri" Text="Minute: "></asp:Label>
                    <asp:TextBox ID="Minute_TextBox" runat="server" Font-Names="Calibri" style="margin-top: 0px">0</asp:TextBox>
                    &nbsp;<asp:Label ID="Time_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrer les heure et minute" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Panel ID="Panel1" runat="server">
                    </asp:Panel>
                </td>
            </tr>
            <tr>
                <td><asp:Label ID="Label45" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" Text="Si les données manquantes, ne pas entrer une valeur" Font-Bold="True"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label3" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station: "></asp:Label>
                    <asp:Label ID="Label4" runat="server" Font-Names="Calibri" Text="Anie" Width="80px"></asp:Label>
                    <asp:Label ID="Label5" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label6" runat="server" Font-Names="Calibri" Text="Precipitation" Width="100px"></asp:Label>
                    <asp:Label ID="Label7" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var1" runat="server" Font-Names="Calibri"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label_Units1" runat="server" Font-Names="Calibri" Text="mm"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label1" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;<asp:CheckBox ID="CheckBox_Var1" runat="server" Visible="False" Font-Names="Calibri" Text="New" />
                    &nbsp;<asp:TextBox ID="TextBox_ID_Existing_Var1" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station: "></asp:Label>
                    <asp:Label ID="Label11" runat="server" Font-Names="Calibri" Text="Sokode" Width="80px"></asp:Label>
                    <asp:Label ID="Label12" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label13" runat="server" Font-Names="Calibri" Text="Precipitation" Width="100px"></asp:Label>
                    <asp:Label ID="Label14" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var2" runat="server" Font-Names="Calibri"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label_Units2" runat="server" Font-Names="Calibri" Text="mm"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label2" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;<asp:CheckBox ID="CheckBox_Var2" runat="server" Visible="False" Font-Names="Calibri" Text="New" />
                    &nbsp;<asp:TextBox ID="TextBox_ID_Existing_Var2" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label15" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station: "></asp:Label>
                    <asp:Label ID="Label16" runat="server" Font-Names="Calibri" Text="Sotoboua" Width="80px"></asp:Label>
                    <asp:Label ID="Label17" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label18" runat="server" Font-Names="Calibri" Text="Precipitation" Width="100px"></asp:Label>
                    <asp:Label ID="Label19" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var3" runat="server" Font-Names="Calibri"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label_Units3" runat="server" Font-Names="Calibri" Text="mm"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label3" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;<asp:CheckBox ID="CheckBox_Var3" runat="server" Visible="False" Font-Names="Calibri" Text="New" />
                    &nbsp;<asp:TextBox ID="TextBox_ID_Existing_Var3" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label20" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station: "></asp:Label>
                    <asp:Label ID="Label21" runat="server" Font-Names="Calibri" Text="Blitta" Width="80px"></asp:Label>
                    <asp:Label ID="Label22" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label23" runat="server" Font-Names="Calibri" Text="Precipitation" Width="100px"></asp:Label>
                    <asp:Label ID="Label24" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var4" runat="server" Font-Names="Calibri"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label_Units4" runat="server" Font-Names="Calibri" Text="mm"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label4" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;<asp:CheckBox ID="CheckBox_Var4" runat="server" Visible="False" Font-Names="Calibri" Text="New" />
                    &nbsp;<asp:TextBox ID="TextBox_ID_Existing_Var4" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label25" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station: "></asp:Label>
                    <asp:Label ID="Label26" runat="server" Font-Names="Calibri" Text="Tchamba" Width="80px"></asp:Label>
                    <asp:Label ID="Label27" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label28" runat="server" Font-Names="Calibri" Text="Precipitation" Width="100px"></asp:Label>
                    <asp:Label ID="Label29" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var5" runat="server" Font-Names="Calibri"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label_Units5" runat="server" Font-Names="Calibri" Text="mm"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label5" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;<asp:CheckBox ID="CheckBox_Var5" runat="server" Visible="False" Font-Names="Calibri" Text="New" />
                    &nbsp;<asp:TextBox ID="TextBox_ID_Existing_Var5" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label30" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station: "></asp:Label>
                    <asp:Label ID="Label31" runat="server" Font-Names="Calibri" Text="Aledjo" Width="80px"></asp:Label>
                    <asp:Label ID="Label32" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label33" runat="server" Font-Names="Calibri" Text="Precipitation" Width="100px"></asp:Label>
                    <asp:Label ID="Label34" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var6" runat="server" Font-Names="Calibri"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label_Units6" runat="server" Font-Names="Calibri" Text="mm"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label6" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;<asp:CheckBox ID="CheckBox_Var6" runat="server" Visible="False" Font-Names="Calibri" Text="New" />
                    &nbsp;<asp:TextBox ID="TextBox_ID_Existing_Var6" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label35" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station: "></asp:Label>
                    <asp:Label ID="Label36" runat="server" Font-Names="Calibri" Text="Goubi" Width="80px"></asp:Label>
                    <asp:Label ID="Label37" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label38" runat="server" Font-Names="Calibri" Text="Precipitation" Width="100px"></asp:Label>
                    <asp:Label ID="Label39" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var7" runat="server" Font-Names="Calibri"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label_Units7" runat="server" Font-Names="Calibri" Text="mm"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label7" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;<asp:CheckBox ID="CheckBox_Var7" runat="server" Visible="False" Font-Names="Calibri" Text="New" />
                    &nbsp;<asp:TextBox ID="TextBox_ID_Existing_Var7" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Label ID="Label40" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station: "></asp:Label>
                    <asp:Label ID="Label41" runat="server" Font-Names="Calibri" Text="Nangbeto" Width="80px"></asp:Label>
                    <asp:Label ID="Label42" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label43" runat="server" Font-Names="Calibri" Text="Precipitation" Width="100px"></asp:Label>
                    <asp:Label ID="Label44" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var8" runat="server" Font-Names="Calibri"></asp:TextBox>
                    &nbsp;<asp:Label ID="Label_Units8" runat="server" Font-Names="Calibri" Text="mm"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label8" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;<asp:CheckBox ID="CheckBox_Var8" runat="server" Visible="False" Font-Names="Calibri" Text="New" />
                    &nbsp;<asp:TextBox ID="TextBox_ID_Existing_Var8" runat="server" Font-Names="Calibri" Visible="False"></asp:TextBox>
                    </td>
            </tr>
            <tr>
                <td class="auto-style2">&nbsp;</td>
            </tr>
            <tr>
                <td class="auto-style2">
                    <asp:Button ID="Save_Button" runat="server" OnClick="Button_Save_Click" Text="Sauvegarder" />
                    &nbsp;<asp:Button ID="Button_Reset" runat="server" OnClick="Button_Reset_Click" Text="Annuler" />
                    &nbsp;<asp:Label ID="Time_Check_Label_Warning" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Medium" ForeColor="#FF3300" Text="Les valeurs sont manquantes pour une ou plusieurs variables. Voulez-vous continuer?" Visible="False" Font-Bold="True"></asp:Label>
                    &nbsp;<asp:Button ID="Save_Button_Yes" runat="server" OnClick="Button_Yes_Click" Text="Oui" Visible="False" />
                    &nbsp;<asp:Button ID="Save_Button_No" runat="server" OnClick="Button_No_Click" Text="No" Visible="False" />
                </td>
            </tr>
            <tr>
                <td class="auto-style2"></td>
            </tr>
        </table>
    
    </div>
    </form>
</body>
</html>
