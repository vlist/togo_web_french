﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" EnableEventValidation="false" Inherits="TogoWeb.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="stylesheet" type="text/css" href="Styles/chosen.css" />
    <link rel="stylesheet" type="text/css" href="Styles/bootstrap.css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="container">
    <div class="row">
        <div class="col-sm-6 col-md-4 col-md-offset-4">
            &nbsp;<h1 class="text-center login-title">Code for Resilience (CfR) Togo</h1>
            <div class="account-wall">
                <form class="form-signin">
                <asp:Textbox runat="server" ID="UserTextbox" CssClass="form-control" type="text" placeholder="Usager" required autofocus></asp:Textbox>
                <asp:TextBox runat="server" ID="passwordTextbox" CssClass = "form-control" type="password" placeholder="Password" required></asp:TextBox>
                <asp:Label runat="server" ID="loginErrorLabel" CssClass="form-group has-error" 
                    for="passwordTextbox" ForeColor="Red" Visible="False">Username or Password is not valid</asp:Label>
                <asp:Button runat="server" ID="loginButton" 
                    CssClass="btn btn-lg btn-primary btn-block" type="submit" formmethod="get" 
                    Text="Entrer" onclick="loginButton_Click" />
                <asp:CheckBox runat="server" ID="rememberCheckbox" 
                    CssClass="checkbox-inline" Text="Rappeller-moi" />
                <span class="clearfix"></span>
                </form>
            </div>
        </div>
    </div>
</div>
    </form>
</body>
</html>
