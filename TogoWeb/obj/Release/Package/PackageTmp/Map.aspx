﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Map.aspx.cs" Inherits="TogoWeb.Map" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="http://cdn.leafletjs.com/leaflet-0.7.3/leaflet.css" />
     <script src="Scripts/leaflet.js"></script>
     <script src="Scripts/leaflet.ajax.min.js"></script>
     <script src="Scripts/jquery-1.10.2.js"></script>
    <script src="Scripts/Leaflet.MakiMarkers.js"></script>
    <style type="text/css">
        #map {
            height: 700px;
        }
    </style>
    <style type="text/css">
        .auto-style1 {
            width: 100%;
        }
        .auto-style2 {
            height: 23px;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="header">
            <table class="auto-style1">
                <tr>
                    <td class="auto-style2">
                        <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Names="Calibri" Text="Simulation la plus récente:"></asp:Label>
                        <asp:TextBox ID="TextBox_SimID" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="TextBox_MetaID" runat="server" Visible="False"></asp:TextBox>
                        <asp:TextBox ID="TextBox_MetaID_opt" runat="server" Visible="False"></asp:TextBox>
                    </td>
                    <td class="auto-style2" rowspan="4">
                        <asp:Image ID="Image1" runat="server" Height="137px" ImageUrl="~/Images/Logos for FUNES website (combined).png" Width="621px" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label7" runat="server" Font-Names="Calibri" Text="Période de simulation:"></asp:Label>
&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label_StartTime" runat="server" Font-Names="Calibri" Text="(Start Time)"></asp:Label>
&nbsp;<asp:Label ID="Label8" runat="server" Font-Names="Calibri" Text="to"></asp:Label>
&nbsp;<asp:Label ID="Label_EndTime" runat="server" Font-Names="Calibri" Text="(End Time)"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label3" runat="server" Font-Names="Calibri" Text="Niveau de risque prévu par le modèle FUNES Automatisé:"></asp:Label>
&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label_Risk" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
&nbsp;<asp:Label ID="Label4" runat="server" Font-Names="Calibri" Text="-"></asp:Label>
&nbsp;<asp:Label ID="Label_Description" runat="server" Font-Names="Calibri" Text="(Description)"></asp:Label>
                    &nbsp;<asp:Image ID="Image_Base_A" runat="server" Height="20px" ImageUrl="~/Images/red_square.jpg" Visible="False" Width="20px" />
                        <asp:Image ID="Image_Base_B" runat="server" Height="20px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                        <asp:Image ID="Image_Base_C" runat="server" Height="20px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                        <asp:Image ID="Image_Base_D" runat="server" Height="20px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                        <asp:Image ID="Image_Base_E" runat="server" Height="20px" ImageUrl="~/Images/gray_square.jpg" Visible="False" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="Label9" runat="server" Font-Names="Calibri" Text="Niveau de risque prévu par Operateurs de Barrage:"></asp:Label>
&nbsp;<br />
&nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label_Risk_Opt" runat="server" Font-Names="Calibri" Text="(Risk)"></asp:Label>
&nbsp;<asp:Label ID="Label10" runat="server" Font-Names="Calibri" Text="-"></asp:Label>
&nbsp;<asp:Label ID="Label_Description_Opt" runat="server" Font-Names="Calibri" Text="(Description)"></asp:Label>
                    &nbsp;<asp:Image ID="Image_Base_A_Opt" runat="server" Height="20px" ImageUrl="~/Images/red_square.jpg" Visible="False" />
                        <asp:Image ID="Image_Base_B_Opt" runat="server" Height="20px" ImageUrl="~/Images/orange_square.jpg" Visible="False" />
                        <asp:Image ID="Image_Base_C_Opt" runat="server" Height="20px" ImageUrl="~/Images/yellow_square.jpg" Visible="False" />
                        <asp:Image ID="Image_Base_D_Opt" runat="server" Height="20px" ImageUrl="~/Images/blue_square.jpg" Visible="False" />
                        <asp:Image ID="Image_Base_E_Opt" runat="server" Height="20px" ImageUrl="~/Images/gray_square.jpg" Visible="False" />
                    </td>
                </tr>
            </table>
        </div>
        
    </div>
        <asp:ScriptManager ID="smMain" runat="server" EnablePageMethods="true" />
        <div id="heading">
            <%--<asp:Label ID="headingLabel" runat="server" Text="This is the heading" Font-Bold="True" Font-Names="Calibri"></asp:Label>&nbsp; </div>--%>
        <div id="map" height:"60%"> 
        </div>

    </form>
    <script type="text/javascript">
         var bounds = <% =this.boundsString %>;
         var map = L.map('map');
         map.fitBounds(bounds);
         var osmUrl = 'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
         var osmAttrib = 'Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
         var osm = new L.TileLayer(osmUrl, { attribution: osmAttrib });
         map.addLayer(osm);

         //add points:
         //var ptColors = ["#660066","#000000","#FF0000","#FF9900","#FFFFOO","#0000FF","#C0C0C0" ]; //purple, black, red, orange, yellow, blue, gray
         var ptCount = <% =this.nPoints %>;
         if (ptCount > 0)
         {
             var ptArr = <% =this.pointsString %>;
             var ptText = <% =this.pointsTextString %>;
             var ptOutput = <% =this.pointsOutputString %>;
             var ptID = <% =this.pointsIDString %>;
             var ptType = <% =this.pointsTypeString %>;
             var ptBack = <% =this.pointsBackgroundFlag %>;
             var ptColor = <% =this.pointsColorsString %>;
             var ptMarker = <% =this.pointsMarkerString %>;

             for (i=0; i<ptCount; i++){
                 //var icon = L.MakiMarkers.icon({icon: "marker", color: ptColors[ptType[i]], size: "m"});
                 ////if (ptType[i] > 0) var icon = L.MakiMarkers.icon({icon: "dam", color: ptColors[ptType[i]], size: "l"});
                 //if (ptType[i] == 1) var icon = L.MakiMarkers.icon({icon: "dam", color: ptColors[ptType[i]], size: "l"});
                 //if (ptType[i] == 2) var icon = L.MakiMarkers.icon({icon: "square", color: ptColors[ptType[i]], size: "m"});

                 //var icon = L.MakiMarkers.icon({icon: "marker-stroked", color: ptColor[i], size: "m"});
                 //if (ptType[i] == 1) var icon = L.MakiMarkers.icon({icon: "dam", color: ptColor[i], size: "l"});
                 //if (ptType[i] == 2) var icon = L.MakiMarkers.icon({icon: "marker-stroked", color: ptColor[i], size: "m"});

                 var icon = L.MakiMarkers.icon({icon: ptMarker[i], color: ptColor[i], size: "m"});
                 if (ptType[i] == 1) var icon = L.MakiMarkers.icon({icon: ptMarker[i], color: ptColor[i], size: "l"});
                 if (ptType[i] == 2) var icon = L.MakiMarkers.icon({icon: ptMarker[i], color: ptColor[i], size: "m"});

                 var newPt = L.marker(ptArr[i], {icon: icon}).addTo(map).bindPopup(ptText[i]+"<br>"+ptOutput[i]);
                 //if (ptOutput[i] != "") newPt.on('click',onPopupClick)
                 //newPt.on('click',onPopupClick)
                 if (ptBack[i] != 1) newPt.on('mouseover',showPopup)
                 newPt._myId = ptID[i];
             }
         }

         function showPopup(e)
         {
             this.openPopup();
         }

         <%--function onPopupClick(e)
         {
             var id = e.target._myId;
             var runID = <% =this.indRunId %>;
             var loc = window.location.pathname;
             var dir = loc.substring(0, loc.lastIndexOf('/'));
             window.open(dir+"MapPopup.aspx?locid=" + id+"&runid="+runID);
         }--%>

         
     </script>
</body>
</html>
