﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AddDataDam.aspx.cs" Inherits="TogoWeb.AddDataDam" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <style type="text/css">

        .auto-style1 {
            width: 100%;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <table class="auto-style1">
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Font-Underline="True" Text="Ajouter une valeur de données Barrage Nangbéto"></asp:Label>
                    &nbsp;<asp:Label ID="Label28" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" Text="Pour ajouter des données à une station différente, s'il vous plaît sélectionner dans le menu &quot;Données hydro / Ajouter&quot;"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Station de données: "></asp:Label>
                    &nbsp;<asp:Label ID="Label27" runat="server" Font-Names="Calibri" Text="Barrage Nangbéto"></asp:Label>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label19" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label24" runat="server" Font-Names="Calibri" Text="Débit apport" Width="90px"></asp:Label>
                    &nbsp;
                    <asp:Label ID="Label4" runat="server" Font-Names="Calibri" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var1" runat="server" Font-Names="Calibri"></asp:TextBox>
                    <asp:Label ID="Label_Units1" runat="server" Font-Names="Calibri" Text="m3/s" Visible="False"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label1" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label20" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label25" runat="server" Font-Names="Calibri" Text="Débit aval" Width="90px"></asp:Label>
                    &nbsp;
                    <asp:Label ID="Label21" runat="server" Font-Names="Calibri" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var2" runat="server" Font-Names="Calibri"></asp:TextBox>
                    <asp:Label ID="Label_Units2" runat="server" Font-Names="Calibri" Text="m3/s" Visible="False"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label2" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label22" runat="server" Font-Bold="True" Font-Names="Calibri" Font-Size="Large" Text="Variable:"></asp:Label>
                    &nbsp;<asp:Label ID="Label26" runat="server" Font-Names="Calibri" Text="Niveau du lac" Width="90px"></asp:Label>
                    &nbsp;
                    <asp:Label ID="Label23" runat="server" Font-Names="Calibri" Text="Valeur: "></asp:Label>
                    <asp:TextBox ID="Value_TextBox_Var3" runat="server" Font-Names="Calibri"></asp:TextBox>
                    <asp:Label ID="Label_Units3" runat="server" Font-Names="Calibri" Text="m.s.m" Visible="False"></asp:Label>
                    &nbsp;<asp:Label ID="Value_Check_Label3" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrez la valeur" Visible="False"></asp:Label>
                    &nbsp;</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Calendar ID="Calendar_Add" runat="server" Font-Names="Calibri"></asp:Calendar>
                    <asp:Label ID="Date_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît sélectionner la date" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label8" runat="server" Font-Names="Calibri" Text="Heure: "></asp:Label>
                    <asp:TextBox ID="Hour_TextBox" runat="server" Font-Names="Calibri">6</asp:TextBox>
                    <asp:Label ID="Label9" runat="server" Font-Names="Calibri" Text="Minute: "></asp:Label>
                    <asp:TextBox ID="Minute_TextBox" runat="server" Font-Names="Calibri" style="margin-top: 0px">0</asp:TextBox>
                    <asp:Label ID="Time_Check_Label" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="S'il vous plaît entrer les heure et minute" Visible="False"></asp:Label>
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>
                    <asp:Button ID="Save_Button" runat="server" OnClick="Button_Save_Click" Text="Sauvegarder" />
                    &nbsp;<asp:Button ID="Button_Reset" runat="server" OnClick="Button_Reset_Click" Text="Annuler" />
                    &nbsp;<asp:Label ID="Time_Check_Label_Warning" runat="server" Font-Italic="True" Font-Names="Calibri" Font-Size="Small" ForeColor="#FF3300" Text="Les valeurs sont manquantes pour une ou plusieurs variables. Voulez-vous continuer?" Visible="False"></asp:Label>
                    &nbsp;<asp:Button ID="Save_Button_Yes" runat="server" OnClick="Button_Yes_Click" Text="Oui" Visible="False" />
                    &nbsp;<asp:Button ID="Save_Button_No" runat="server" OnClick="Button_No_Click" Text="No" Visible="False" />
                </td>
            </tr>
        </table>
    <div>
    
    </div>
    </form>
</body>
</html>
